#!/bin/bash
path=`pwd`
export PYTHONPATH=$PATH:$path
export DJANGO_SETTINGS_MODULE=config.settings
echo "-----Starting ussd ---"
ussd/./ussd_restart.sh
echo "-----Starting app ---"
twistd --pidfile=web.pid -y nock_site.py
