import logging
from twisted.internet import reactor
from apps.consumers.models import Consumer
from apps.consumers.choices import IPRS_STATUS
from ussd import conf

class regedit_id():
    def __init__(self):
        pass
    def processAction(self, resources):
        req=resources['parameters']['request'].lower()
        if len(req) <7 or not req.isdigit():
            resources['parameters']['response'] = 'Invalid input\n Enter National id.'

            resources['hasResponse'] = True
            return resources
        resources['parameters']['national_id']=req
        resources['currentMenu']='regedit_first_name'
        return resources
    def start(self, resources):
        if not resources['hasResponse']:
           resources['parameters']['response']='Reply with your national Id'
        resources['hasResponse'] = False
        return resources


class regedit_first_name():
    def __init__(self):
        pass
    def processAction(self, resources):
        req = resources['parameters']['request']
        if not req:
           return resources
        resources['parameters']['first_name']=req
        resources['currentMenu']='regedit_last_name'
        return resources
    def start(self, resources):
        if not resources['hasResponse']:
            resources['parameters']['response']='Enter your First Name'
        resources['hasResponse']=False
        return resources

class regedit_last_name():
    def __init__(self):
        pass
    def processAction(self, resources):
        req = resources['parameters']['request']
        if not req:
           return resources
        resources['parameters']['last_name']=req
        resources['currentMenu']='wait'
        resources['hasResponse']=True
        resources['parameters']['response']='Please wait while we verify your details'
        reactor.callInThread(self.user_edit, resources)
        return resources
    def start(self, resources):
        if not resources['hasResponse']:
            resources['parameters']['response']='Enter your Last Name'
        resources['hasResponse']=False
        return resources
    def user_edit(self, resources):
        try:
            consumer = Consumer.objects.get(tenant__id=resources['user']['tenant_id'])
            consumer.user.first_name = resources['parameters']['first_name']
            consumer.user.last_name = resources['parameters']['last_name']
            consumer.user.identity_number = resources['parameters']['national_id']
            consumer.user.save()
            consumer.iprs_status = IPRS_STATUS.Default
            consumer.iprs_change_counter +=1
            consumer.save()
            print(f"edited: {resources['user']['tenant_id']}")
        except Exception as e:
            logging.exception(e)

