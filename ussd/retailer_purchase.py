'''
These menus will handle both new cylinder purchase and refill
'''
import logging
from datetime import datetime
import requests
import json
from twisted.internet import reactor

from apps.tenants.models import Tenant
from apps.consumers.models import Consumer
from apps.products.models import (Brand, Cylinder, Product, Listing)
from apps.inventory.models import CurrentStock

from ussd import conf
from ussd.menus import pin
from ussd import utils


class purchase_service():
    def __init__(self):
        pass

    def processAction(self, resources):
        req = resources['parameters']['request'].lower()
        if req == '1':
            resources['parameters']['service'] = conf.SERVICE_PURCHASE
            resources['nextMenu'] = 'purchase_cylinder'
            resources['parameters']['purchase_type'] = conf.PRODUCT_TYPE_SUPAGAS
            resources['currentMenu'] = 'purchase_business_code'
            resources['isRetailer'] = True
        elif req == '2':
            resources['parameters']['service'] = conf.SERVICE_REFILL

            resources['currentMenu'] = 'purchase_business_code'
            resources['nextMenu'] = 'purchase_product_type'
            resources['isRetailer'] = True
        return resources

    def start(self, resources):
        resources['parameters']['orders'] = {}  # initialize cart

        resources['parameters']['response'] = f'Select:\n1.New Cylinder\n2.Gas Refill'
        return resources


class purchase_product_type():
    def __init__(self):
        pass

    def processAction(self, resources):
        req = resources['parameters']['request'].lower()
        if not 'orders' in resources['parameters']:
            resources['parameters']['orders'] = {}  # initialize cart

        if req == '1':
            resources['currentMenu'] = 'purchase_quantity'
            resources['parameters']['purchase_type'] = conf.PRODUCT_TYPE_GASYETU
            resources['parameters']['cylinder_size'] = conf.GAS_YETU_CYLINDER

            # uncomment this line to choose cylinder size
            #resources['nextMenu'] = 'purchase_cylinder'
        elif req == '2':
            resources['currentMenu'] = 'purchase_cylinder'
            resources['parameters']['purchase_type'] = conf.PRODUCT_TYPE_SUPAGAS
        return resources

    def start(self, resources):
        resources['parameters']['response'] = f'Select:\n1.Gas Yetu\n2.Supa Gas'
        if resources['parameters']['service'] == conf.SERVICE_PURCHASE:
            resources['currentMenu'] = 'purchase_cylinder'
            return purchase_cylinder().start(resources)
        return resources


class purchase_cylinder():
    def __init__(self):
        pass

    def processAction(self, resources):
        req = resources['parameters']['request']
        if not req:
            return resources
        if req == '0':
            return resources
        cylinder_size = None
        if req == '1':
            cylinder_size = '3KG'
        elif req == '2':
            cylinder_size = '6KG'
        elif req == '3':
            cylinder_size = '13KG'
        elif req == '4':
            cylinder_size = '50KG'
        else:
            return resources
        resources['parameters']['cylinder_size'] = cylinder_size
        resources['currentMenu'] = 'purchase_quantity'
        return resources

    def start(self, resources):
        if not resources['hasResponse']:
            resources['parameters']['response'] = conf.MSG_PURCHASE_SUPA_GAS
        resources['hasResponse'] = False

        return resources


class purchase_quantity():
    def __init__(self):
        pass

    def processAction(self, resources):
        quantity = resources['parameters']['request']
        if not quantity:
            return resources
        if not quantity.isdigit():
            return resources
        if quantity == '0':
            if len(resources['parameters']['orders']) > 0:
                resources['currentMenu'] = 'purchase_pay'
            else:
                resources['currentMenu'] = 'purchase_product_type'
            return resources
        quantity = int(quantity)
        if quantity < 1 or quantity > conf.MAXIMUM_QUANTITY:
            resources['parameters'][
                'response'] = f'quantity must be greater or equal to 1 and less than {conf.MAXIMUM_QUANTITY}'
            resources['parameters']['response'] = f"{resources['parameters']['response']}\n0.back"
            resources['hasResponse'] = True
            return resources
        #resources['currentMenu'] = 'purchase_pay'
        return self.validate_stock(resources, quantity)

    def validate_stock(self, resources, quantity):
        service = conf.SERVICE_PURCHASE
        cylinder_size = resources['parameters']['cylinder_size']
        if resources['parameters']['service'] == conf.SERVICE_REFILL:
            service = conf.SERVICE_REFILL
        is_gas_yetu = True
        stock_quantity = 0
        if resources['parameters']['purchase_type'] == conf.PRODUCT_TYPE_SUPAGAS:
            is_gas_yetu = False
        tenant = Tenant.objects.get(pk=resources['parameters']['business_id'])
        cylinder = Cylinder.objects.filter(title__iexact=cylinder_size).first()
        listing = Listing.objects.filter(product__gas_yetu=is_gas_yetu,
                                         tenant=tenant,
                                         service=service,
                                         product__cylinder=cylinder).first()
        if listing:
            current_stock = CurrentStock.objects.filter(
                product=listing.product).first()
            if current_stock:
                stock_quantity = current_stock.no_of
        if not listing or stock_quantity < 1:  # no stock
            resources['parameters']['response'] =\
                f"{resources['parameters']['business_title']} does not have requested quantity stock for {cylinder_size}"
            resources['parameters']['response'] = f"{resources['parameters']['response']}\n0.back"
            resources['hasResponse'] = True
            if is_gas_yetu:
                resources['action'] = 'end'
            # resources['currentMenu']='purchase_product_type'
            return resources
        if listing.id in resources['parameters']['orders']:
            resources['parameters']['orders'][listing.id] += quantity
        else:
            resources['parameters']['orders'][listing.id] = quantity

        #resources['parameters']['listing_id'] = listing.id
        #resources['parameters']['list_price'] = listing.list_price
        resources['currentMenu'] = 'purchase_pay'
        return resources

    def start(self, resources):
        if not resources['hasResponse']:
            resources['parameters']['response'] = conf.MSG_PURCHASE_QUANTITY
        resources['hasResponse'] = False
        return resources


class purchase_pay():
    def __init__(self):
        pass

    def processAction(self, resources):
        req = resources['parameters']['request']
        if not req:
            return resources
        if req == '2':
            resources['currentMenu'] = 'purchase_product_type'
        elif req == '1':
            # get prices
            resources['parameters']['response'] = self.prepare_order(resources)
            resources['hasResponse'] = True
            resources['nextMenu'] = 'purchase_confirm'
            resources['currentMenu'] = 'pin'

        return resources

    def start(self, resources):
         # TODO check if wallet balance and display it
        resources['parameters']['response'] = conf.MSG_PURCHASE_PAY
        return resources

    def prepare_order(self, resources):
        text = ''
        total_price = 0
        for listing_id, quantity in resources['parameters']['orders'].items():
            listing = Listing.objects.select_related(
                "product__cylinder").get(pk=listing_id)
            price = quantity * listing.list_price
            total_price += price
            text = f'{text}Cylider size: {listing.product.cylinder.title}\nUnits: {quantity:,}\nPrice: Ksh{price:,}\n'
        text = f'{text}\nTotal Price: Ksh{total_price:,}\nEnter pin to confirm'
        return text


class purchase_confirm():
    def __init__(self):
        pass

    def processAction(self, resources):
        return resources

    def start(self, resources):
        if not 'payment_mode' in resources['parameters']:
           resources['currentMenu'] = 'payment_mode'
           resources['nextMenu'] = 'purchase_gas_offer_finish'
           return payment_mode().start(resources)
        request = {
            "buyer": f"{resources['user']['tenant_id']}",
            "seller": f"{resources['parameters']['business_id']}",
            "seller_code": resources['parameters']['business_code'],
            "mode_of_payment": conf.PAYMENT_MODE,
            "password": resources["password"]
        }
        order_details = []
        for listing_id, quantity in resources['parameters']['orders'].items():
            order_details.append({
                "listing_id": listing_id,
                "quantity": quantity})
        request['order_items'] = order_details
        headers = {'Authorization': f'JWT {resources["token"]}'}
        r = requests.post(f'{conf.API_BASE_URL}/orders/',
                          headers=headers, json=request)
        if r.status_code == requests.codes.ok or r.status_code == requests.codes.created:
            resp = r.json()
            if 'voucher_number' in resp:
                resources['parameters']['response'] = \
                    conf.MSG_PURCHASE_SUPA_GAS_CYLINDER_SUCCESS.format(voucher=resp['voucher_number'],
                                                                       business=resources['parameters']['business_title'])
                if resources['parameters']['purchase_type'] == conf.PURCHASE_REFILL:
                    resources['parameters']['response'] = \
                        conf.MSG_PURCHASE_SUPA_GAS_REFILL_SUCCESS.format(voucher=resp['voucher_number'],
                                                                         business=resources['parameters']['business_title'])
            else:  # ??
                print('Order failed:', request)
        else:
            print(r.text)
            resources['parameters']['response'] = conf.ERROR
            resp = r.json()
            if 'error' in resp:
                if resp['error'] == 'Low Wallet Balance':
                    resources['parameters']['response'] = 'Low Wallet Balance. Please topup and try again'

        resources['action'] = 'end'
        return resources
