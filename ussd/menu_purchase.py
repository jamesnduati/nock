'''
These menus will handle both new cylinder purchase and refill
'''
import logging
from datetime import datetime
import requests
import json
from twisted.internet import reactor

from apps.tenants.models import Tenant
from apps.consumers.models import Consumer
from apps.products.models import (Brand,Cylinder,Product,Listing)

from ussd import conf
from ussd.menus import (pin, payment_mode)
from ussd import utils


class purchase_product_type():
    def __init__(self):
        pass
    def processAction(self, resources):
        req=resources['parameters']['request'].lower()
        if req =='1':
            if resources['user']['is_beneficiary']:
               resources['currentMenu'] = 'purchase_business_code'
               resources['nextMenu'] = 'purchase_gas_offer'
        elif req == '2':
            if resources['user']['is_beneficiary']:
                return resources
            resources['currentMenu'] = 'purchase_business_code'
            resources['nextMenu'] = 'purchase_supa_gas'
        resources['hasResponse'] = False
        return resources

    def start(self, resources):# TODO Check for Gas yetu eligibility
        resources['parameters']['response'] = f'Select:\n2.Supa Gas'
        if resources['user']['is_beneficiary']:
            if resources['parameters']['purchase_type']==conf.PURCHASE_CYLINDER:
                if resources['user']['has_benefited']:
                    resources['parameters']['response'] = 'Purchase discount already redeemed'
                    #resources['currentMenu'] = 'wait'
                    resources['action'] = 'end'
                    #resources['hasResponse'] = True
                else:
                    resources['parameters']['response'] = f'Select:\n1.Gas Yetu'
            elif  resources['parameters']['purchase_type']==conf.PURCHASE_REFILL:
                resources['parameters']['response'] = f'Select:\n1.Gas Yetu'
        else:#consumers
            resources['parameters']['response'] = f'Select:\n2.Supa Gas'
        return resources

class purchase_gas_offer():
    def __init__(self):
        pass
    def processAction(self, resources):
        return resources
    def start(self, resources):
        service = conf.SERVICE_PURCHASE
        if resources['parameters']['purchase_type'] == conf.PURCHASE_REFILL:
            service = conf.SERVICE_REFILL
        tenant=Tenant.objects.get(pk=resources['parameters']['business_id'])
        listing =Listing.objects.filter(product__gas_yetu=True,tenant=tenant, service=service).first()
        if not listing:#no stock
            resources['action'] = 'end'
            resources['parameters']['response'] = f"{resources['parameters']['business_title']} does not have any stock. Select another vendor"
            return resources
        resources['parameters']['listing_id']=listing.id
        # TODO check for wallet balance
        # if has balance
        ###############################
        #balance = int(float(account.amount)/100)
        resources['parameters']['requireTopUp'] = False
        '''
        if int(listing.list_price) > balance:
            resources['parameters']['requireTopUp'] = True
            resources['parameters']['amount'] = int(listing.list_price)-balance # topup balance
        '''
        resources['nextMenu'] = 'purchase_gas_offer_finish'
        resources['parameters']['response'] = conf.MSG_PURCHASE_GAS_OFFER_CYLINDER.format(amount=int(listing.list_price))
        if resources['parameters']['purchase_type'] == conf.PURCHASE_REFILL:
            resources['parameters']['response'] = conf.MSG_PURCHASE_GAS_OFFER_REFILL.format(amount=int(listing.list_price))
        if resources['parameters']['requireTopUp']:
            resources['parameters']['response'] = f"{resources['parameters']['response']}\nTop up of Ksh{resources['parameters']['amount']} will be initiated"
        resources['hasResponse'] = True
        resources['currentMenu'] = 'pin'
        return pin().start(resources)
        # if no balance show top up menu
        #################################
        # TODO topup menu on Gas yetu

        #return resources

class purchase_gas_offer_finish():
    def __init__(self):
        pass

    def processAction(self, resources):
        return resources

    def start(self, resources):
        resources['hasResponse'] = False
        if not 'payment_mode' in resources['parameters']:
           resources['currentMenu'] = 'payment_mode'
           resources['nextMenu'] = 'purchase_gas_offer_finish'
           return payment_mode().start(resources)
        resources = self.make_order(resources)
        resources['action'] = 'end'
        return resources

    def make_order(self,resources):
        resources['parameters']['response']=conf.ERROR
        request = {
                  "buyer": str(resources["user"]["tenant_id"]),
                  "seller":str(resources['parameters']['business_id']),
                  "seller_code": resources['parameters']['business_code'],
                  "password":resources['password'],
                  "mode_of_payment": resources['parameters']['payment_mode'],
                  "order_items": [
                  {
                    "listing_id": resources["parameters"]["listing_id"],
                    "quantity": 1
                  }
                  ]
                  }
        headers = {'Authorization':f'JWT {resources["token"]}'}
        r = requests.post(f'{conf.API_BASE_URL}/orders/',headers=headers, json=request)
        if r.status_code==requests.codes.ok or r.status_code == requests.codes.created:
            resp = r.json()
            print(resp)
            #if resources['parameters']['requireTopUp']:
            #    reactor.callLater(2,utils.initiate_mpesa_stk, resources)
            if 'voucher_number' in resp:

                resources['parameters']['response'] = \
                conf.MSG_PURCHASE_GAS_OFFER_CYLINDER_SUCCESS.format(voucher=resp['voucher_number'],
                business=resources['parameters']['business_title'])
                if resources['parameters']['purchase_type'] == conf.PURCHASE_REFILL:
                   resources['parameters']['response'] = \
                   conf.MSG_PURCHASE_GAS_OFFER_REFILL_SUCCESS.format(voucher=resp['voucher_number'],
                   business=resources['parameters']['business_title'])
                #return True
            else:# ??
                print('Order failed:',request)
                #return False
        else:
            print(r.text)
            resp=r.json()
            if 'error' in resp:
               if 'low wallet balance' in resp['error'].lower():
                  resources['parameters']['response']='Low wallet balance. Please topup and try again'
            elif 'msg' in resp:
                resources['parameters']['response'] = resp['msg']

            #return False
        return resources

class purchase_business_code():
    def __init__(self):
        pass
    def processAction(self, resources):
        req = resources['parameters']['request']
        if not req:
           return resources
        if not req.isdigit():
            resources['parameters']['response'] = 'Business code must be a number\n00.Home'
            return resources
        is_retailer = False
        if 'isRetailer' in resources:
            is_retailer =resources['isRetailer']
        resp = utils.get_business_from_code(req, is_retailer)
        if resp['business']:
            resources['parameters'].update(resp)
        else:
            resources['hasResponse']=True
            resources['parameters']['response'] = f'{req} Business code does not exists\n00.Home'
            return resources
        resources['parameters']['business_code'] = req
        resources['currentMenu'] = resources['nextMenu']
        return resources
    def start(self, resources):
        if not resources['hasResponse']:
           resources['parameters']['response'] = 'Enter Business code'
        resources['hasResponse']=False
        return resources


class purchase_supa_gas():# FIXME adding service type to product
    def __init__(self):
        pass

    def processAction(self, resources):
        if not 'orders' in resources['parameters']:
            resources['parameters']['orders'] = {}#initialize cart
        req = resources['parameters']['request']
        if not req:
           return resources
        if req=='0':
            return resources
        cylinder_size = None
        if req == '1':
            cylinder_size = '3KG'
        elif req == '2':
            cylinder_size = '6KG'
        elif req == '3':
            cylinder_size = '13KG'
        elif req == '4':
            cylinder_size = '50KG'
        else:
            return resources
        service = conf.SERVICE_PURCHASE
        if resources['parameters']['purchase_type'] == conf.PURCHASE_REFILL:
            service = conf.SERVICE_REFILL
        tenant=Tenant.objects.get(pk=resources['parameters']['business_id'])
        cylinder=Cylinder.objects.filter(title__iexact=cylinder_size).first()
        listing = Listing.objects.filter(product__gas_yetu = False,
                               tenant = tenant,
                               service = service,
                               product__cylinder = cylinder).first()
        if not listing:#no stock
            resources['parameters']['response'] =\
                    f"{resources['parameters']['business_title']} does not have any stock for {cylinder_size}"
            resources['parameters']['response'] = f"{resources['parameters']['response']}\n0.back"
            resources['hasResponse']=True
            return resources

        resources['parameters']['listing_id']= listing.id
        resources['parameters']['list_price']= listing.list_price
        resources['currentMenu'] = 'purchase_quantity'
        return resources
    def start(self, resources):
        if not resources['hasResponse']:
           resources['parameters']['response'] = conf.MSG_PURCHASE_SUPA_GAS
        resources['hasResponse']=False
        return resources

class purchase_quantity():
    def __init__(self):
        pass
    def processAction(self, resources):
        quantity = resources['parameters']['request']
        if not quantity:
           return resources
        if not quantity.isdigit():
            return resources
        if quantity == '0':
            if len(resources['parameters']['orders']):
                resources['currentMenu']='purchase_pay'
            else:
                resources['currentMenu']='purchase_supa_gas'
        quantity = int(quantity)
        if quantity <1 or quantity >conf.MAXIMUM_QUANTITY:
            resources['parameters']['response'] = f'quantity must be greater or equal to 1 and less than {conf.MAXIMUM_QUANTITY}'
            resources['parameters']['response'] = f"{resources['parameters']['response']}\n0.back"
            resources['hasResponse'] = True
            return resources
        if resources['parameters']['listing_id'] in resources['parameters']['orders']:
            resources['parameters']['orders'][resources['parameters']['listing_id']] += quantity
        else:
            resources['parameters']['orders'][resources['parameters']['listing_id']] = quantity
        resources['currentMenu']='purchase_pay'
        return resources
    def start(self, resources):
        if not resources['hasResponse']:
            resources['parameters']['response'] = conf.MSG_PURCHASE_QUANTITY
        resources['hasResponse']=False
        return resources

class purchase_pay():
    def __init__(self):
        pass
    def processAction(self, resources):
        req = resources['parameters']['request']
        if not req:
           return resources
        if req == '2':
            resources['currentMenu'] = 'purchase_supa_gas'
        elif req == '1':
            # get prices
            resources['parameters']['response'] = self.prepare_order(resources)
            resources['hasResponse'] = True
            resources['nextMenu'] = 'purchase_confirm'
            resources['currentMenu'] = 'pin'

        return resources
    def start(self, resources):
         # TODO check if wallet balance and display it
        resources['parameters']['response']= conf.MSG_PURCHASE_PAY
        return resources

    def prepare_order(self, resources):
        text=''
        total_price=0
        for listing_id, quantity in resources['parameters']['orders'].items():
            listing = Listing.objects.select_related("product__cylinder").get(pk = listing_id)
            price = quantity * listing.list_price
            total_price += price
            text = f'{text}Cylider size: {listing.product.cylinder.title}\nUnits: {quantity:,}\nPrice: Ksh{price:,}\n'
        text = f'{text}\nTotal Price: Ksh{total_price:,}\nEnter pin to confirm'
        return text

class purchase_confirm():
    def __init__(self):
        pass

    def processAction(self, resources):
        return resources

    def start(self, resources):
        resources['hasResponse'] = False
        if not 'payment_mode' in resources['parameters']:
           resources['currentMenu'] = 'payment_mode'
           resources['nextMenu'] = 'purchase_confirm'
           resources['hasResponse'] = False
           return payment_mode().start(resources)

        request = {
                  "buyer": str(resources['user']['tenant_id']),
                  "seller":str(resources['parameters']['business_id']) ,
                  "seller_code": resources['parameters']['business_code'],
                  "password": resources['password'],
                  "mode_of_payment":resources['parameters']['payment_mode']
                  }
        order_details = []
        for listing_id, quantity in resources['parameters']['orders'].items():
            order_details.append({
                    "listing_id": listing_id,
                    "quantity": quantity})
        request['order_items']=order_details
        headers = {'Authorization':f'JWT {resources["token"]}'}
        r = requests.post(f'{conf.API_BASE_URL}/orders/',headers = headers, json = request)
        if r.status_code==requests.codes.ok or r.status_code == requests.codes.created:
            resp = r.json()
            if 'voucher_number' in resp:
                resources['parameters']['response'] = \
                conf.MSG_PURCHASE_SUPA_GAS_CYLINDER_SUCCESS.format(voucher=resp['voucher_number'],
                business=resources['parameters']['business_title'])
                if resources['parameters']['purchase_type'] == conf.PURCHASE_REFILL:
                   resources['parameters']['response'] = \
                   conf.MSG_PURCHASE_SUPA_GAS_REFILL_SUCCESS.format(voucher=resp['voucher_number'],
                   business=resources['parameters']['business_title'])
            else:# ??
                print('Order failed:',request)
        else:
            print(r.text)
            resp = r.json()
            resources['parameters']['response'] = conf.ERROR
            if 'error' in resp:
               if 'low wallet balance' in resp['error'].lower():
                     resources['parameters']['response'] = 'Low wallet balance. Please topup and try again'
            elif 'msg' in resp:
                resources['parameters']['response'] = resp['msg']

        resources['action'] = 'end'
        return resources
