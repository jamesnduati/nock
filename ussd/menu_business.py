from apps.distribution.models import (Distributor, Retailer)
from ussd import conf


class business_home():
    def __init__(self):
        pass

    def processAction(self, resources):
        req = resources['parameters']['request'].lower()
        if req == '1':
            resources['currentMenu'] = 'business_distributor'
        elif req == '2':
            resources['currentMenu'] = 'business_retailer'
        return resources

    def start(self, resources):
        resources['parameters']['response'] = conf.MSG_BUSINESS_MENU
        return resources


class business_distributor():
    def __init__(self):
        pass

    def processAction(self, resources):
        req = resources['parameters']['request'].strip()
        if not req:
            return resources
        if Distributor.objects.filter(enabled=True, agent_number=req).exists():
            resources['currentMenu'] = 'business_distributor_detail'
            resources['parameters']['agent_number'] = req
        else:
            resources['parameters']['response'] = f'{req} distributor not found'
            resources['hasResponse'] = True
            resources['currentMenu'] = 'wait'

        return resources

    def start(self, resources):
        distributors = Distributor.objects.filter(enabled=True,
                                                  region__id=resources['user']['region_id']).order_by('-agent_number')
        if distributors:
            text = 'Select Distributor Number:\n'
            for distributor in distributors:
                text = f'{text}{distributor.agent_number}.{distributor.trading_name}\n'
            resources['parameters']['response'] = text
        else:
            resources['parameters'][
                'response'] = f"No distributors found at {resources['user']['region_title']}"
            resources['action'] = 'end'

        return resources


class business_distributor_detail():
    def __init__(self):
        pass

    def processAction(self, resources):
        return resources

    def start(self, resources):
        distributor = Distributor.objects.get(
            agent_number=resources['parameters']['agent_number'])
        text = f'Code:{distributor.agent_number}\n{distributor.trading_name}\n'
        text = f'{text}{distributor.physical_address}'
        resources['parameters']['response'] = text
        resources['action'] = 'end'
        return resources


class business_retailer():
    def __init__(self):
        pass

    def processAction(self, resources):
        req = resources['parameters']['request'].strip()
        if not req:
            return resources
        if Retailer.objects.filter(agent_number=req, enabled=True).exists():
            resources['currentMenu'] = 'business_retailer_detail'
            resources['parameters']['agent_number'] = req
        else:
            resources['parameters']['response'] = f'{req} retailer not found'
            resources['hasResponse'] = True
            resources['currentMenu'] = 'wait'

        return resources

    def start(self, resources):
        retailers = Retailer.objects.filter(
            enabled=True, sub_region__id=resources['user']['sub_region_id']).order_by('-agent_number')
        if retailers:
            text = 'Select Retailer Number:\n'
            for retailer in retailers:
                text = f'{text}{retailer.agent_number}.{retailer.trading_name}\n'
            resources['parameters']['response'] = text
        else:
            resources['parameters'][
                'response'] = f"No retailers found at {resources['user']['sub_region_title']}"
            resources['action'] = 'end'

        return resources


class business_retailer_detail():
    def __init__(self):
        pass

    def processAction(self, resources):
        return resources

    def start(self, resources):
        retailer = Retailer.objects.get(
            agent_number=resources['parameters']['agent_number'])
        text = f'Code:{retailer.agent_number}\n{retailer.trading_name}'
        text = f'{text}\n{retailer.physical_address}'
        resources['parameters']['response'] = text
        resources['action'] = 'end'
        return resources
