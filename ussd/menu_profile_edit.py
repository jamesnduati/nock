class profile_edit_id():
    def __init__(self):
        pass
    def processAction(self, resources):
        req=resources['parameters']['request'].lower()
        if len(req) <6 or not req.isdigit():
            resources['parameters']['response'] = 'Invalid input\n Enter National id.'

            resources['hasResponse'] = True
            return resources
        resources['parameters']['national_id']=req
        resources['currentMenu']='registration_region'
        return resources
    def start(self, resources):
        if not resources['hasResponse']:
           resources['parameters']['response']='Reply with your national Id'
        resources['hasResponse'] = False
        return resources

class profile_edit_first_name():
    def __init__(self):
        pass
    def processAction(self, resources):
        req = resources['parameters']['request']
        if not req:
           return resources
        resources['parameters']['first_name']=req
        resources['currentMenu']='registration_last_name'
        return resources
    def start(self, resources):
        if not resources['hasResponse']:
            resources['parameters']['response']='Enter your First Name'
        resources['hasResponse']=False
        return resources

class profile_edit_last_name():
    def __init__(self):
        pass
    def processAction(self, resources):
        req = resources['parameters']['request']
        if not req:
           return resources
        resources['parameters']['last_name']=req
        resources['currentMenu']='registration_id'
        return resources
    def start(self, resources):
        if not resources['hasResponse']:
            resources['parameters']['response']='Enter your Last Name'
        resources['hasResponse']=False
        return resources
