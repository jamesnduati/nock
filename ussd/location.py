'''
- preload locations to redis
- location getters 
'''
import logging
import redis

from apps.core.models import (Region,SubRegion,Location,Village)
#from apps.products.models import (Cylinder,Brand)
from ussd import conf

class UserLocation(object):# Location would be appropriate name here 
    __monostate = None
    def __init__(self):        
        if not UserLocation.__monostate:
            UserLocation.__monostate = self.__dict__             
            self.redis_connection_pool = redis.ConnectionPool(host=conf.REDIS_HOST, port=conf.RESIS_PORT, db=conf.REDIS_DB)
            self.redis = redis.StrictRedis(connection_pool=self.redis_connection_pool)
        else:
            self.__dict__ = UserLocation.__monostate

    def set_locations(self):
        try:          
            keys = self.redis.keys("loc:*")
            if keys:
                for key in keys: self.redis.delete(key)
            regions = Region.objects.all().order_by('code')
            if regions:
                 self.redis.hmset(f'{conf.LOCATION_CACHE_PREFIX}',{loc.code:loc.title for loc in regions})
            for region in regions:
                sub_regions = SubRegion.objects.filter(region=region).order_by('title')
                if sub_regions:
                    self.redis.hmset(f'{conf.LOCATION_CACHE_PREFIX}:{region.code}',{loc.code:loc.title for loc in sub_regions})
                for sub_region in sub_regions:
                    locations=Location.objects.filter(subregion=sub_region).order_by('title')
                    if locations:
                        self.redis.hmset(f'{conf.LOCATION_CACHE_PREFIX}:{region.code}:{sub_region.code}',
                                         {loc.code:loc.title for loc in locations})
                    for location in locations:
                        villages=Village.objects.filter(location=location).order_by('title')
                        if villages:
                            self.redis.hmset(f'{conf.LOCATION_CACHE_PREFIX}:{region.code}:{sub_region.code}:{location.code}',
                                         {village.code:village.title for village in villages})


        except Exception as e:
           logging.exception(e)
    def set_brands(self):
        try:
            pass
        except Exception as e:
            logging.exception(e)
