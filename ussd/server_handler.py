import os
import logging
from datetime import datetime
from time import sleep, time

from twisted.internet import defer, reactor, threads
from twisted.web import resource, server
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")
import django

django.setup()

import memcache
from apps.accounts.models import User
from apps.consumers.models import (Consumer, Beneficiary)
from apps.employees.models import Employee
from apps.tenants.choices import RETAILER

from ussd import conf
from ussd.menus import menus
from ussd.retailer_menus import retailer_menus


class ConsumerService(resource.Resource):
    def __init__(self):
        resource.Resource.__init__(self)
        self.home = 'home'
        self.menu = {}
        self.mc = memcache.Client(['127.0.0.1:11211'], debug=0)

    def get_user(self, params):
        """
           To reduce number of db queries, get as much of the details here
        """
        phone = params['parameters']['phone']
        if Consumer.objects.filter(user__username=phone, user__is_active=True).exists():

            user = User.objects.select_related(
                'consumer__tenant', 'region', 'sub_region').filter(username=phone).first()
            ret = {'first_name': user.first_name, 'is_beneficiary': False,
                   'has_benefited': False, 'pending_beneficiary': True,
                   'tenant_id': user.consumer.tenant_id,
                   'sub_region_id': user.sub_region_id,
                   'sub_region_title': user.sub_region.title,
                   'region_id': user.region_id,
                   'region_title': user.region.title,
                   'iprs_verified': user.consumer.iprs_verified,
                   'iprs_status': user.consumer.iprs_status
                   }
            beneficiary = Beneficiary.objects.filter(user=user).first()
            if beneficiary:
                ret['is_beneficiary'] = True
                ret['pending_beneficiary'] = False
                if beneficiary.has_benefited:
                    ret['has_benefited'] = True
            else:
                if not user.consumer.survey_requested or user.consumer.survey_conducted:
                    ret['pending_beneficiary'] = False
            return ret
        return None

    def get_session_state(self, params):
        '''
           sessionid is a unique identifier of each users ussd journey and state
        '''
        session_state = self.mc.get("{}{}".format(conf.SESSION_KEY,
                                                  params['sessionid']))
        if not session_state:
            session_state = {}
            session_state['parameters'] = params
            session_state['currentMenu'] = 'home'
            session_state['lastSeen'] = datetime.now()
            session_state['action'] = 'con'
            session_state['hasResponse'] = False
            session_state['sessionTime'] = 300
            session_state['sessionid'] = params['sessionid']
            session_state['user'] = self.get_user(session_state)
            self.mc.set("{}{}".format(conf.SESSION_KEY, params['sessionid']),
                        session_state, session_state['sessionTime'])

            return session_state
        else:

            session_state['parameters']['request'] = params['request']
            session_state['parameters']['response'] = None
            return session_state

    def update_session_state(self, params):
        try:
            if 'action' in params:
                if params['action'].lower() == 'end':
                    params['currentMenu'] = 'home'
                    params['sessionTime'] = 1  # expire session
            params['lastSeen'] = datetime.now()
            self.mc.set("{}{}".format(conf.SESSION_KEY,
                                      params['parameters']['sessionid']),
                        params, params['sessionTime'])
        except Exception as e:
            logging.exception(e)
        return params

    def call_next_menu(self, params):
        menu = params['currentMenu']
        if menu == 'root':
            menu = 'home'
        if not menu in menus:
            menu = menus['generic']
        else:
            menu = menus[menu]
        # return defer.maybeDeferred(menu().start, params)
        return menu().start(params)

    def get_response(self, response, request):

        action = response['action'].upper()
        resp = response['parameters']['response']
        resp = f'{action} {resp}'
        print(f'RESP: {resp}')
        request.write(resp.encode('utf8'))
        request.finish()

    def process_request(self, params):

        print("params:: ", params)
        if params['parameters']["request"] == "#" or params['parameters']["request"] == "00":
            params["currentMenu"] = "home"

        menu = params['currentMenu']
        if not menu in menus:
            menu = menus['generic']
        else:
            menu = menus[menu]
        d = threads.deferToThread(menu().processAction, params)
        d.addCallback(self.call_next_menu)
        d.addCallback(self.update_session_state)
        return d

    def render_GET(self, request):
        return self.render_POST(request)

    def render_POST(self, request):
        params = {}
        for k, v in list(request.args.items()):
            params[k.lower().decode('utf8')] = v[0].decode('utf8')
        params['phone'] = params['msisdn']
        params['sessionid'] = params['sessionid']
        req = params['ussdstring'].split('*')
        params['request'] = req[len(req) - 1]
        try:
            d = self.process_request(self.get_session_state(params))
            d.addCallback(self.get_response, request)
        except Exception as e:
            print('error could not process ussd request: ' + str(e))
            logging.exception(e)
            return 'error'
        else:
            return server.NOT_DONE_YET

    def getChild(self, path, request):
        return self

    def _expired(self, uid):
        del self.sessions[uid]
        print(uid, ": session expired")


class RetailerService(resource.Resource):
    def __init__(self):
        resource.Resource.__init__(self)
        self.home = 'home'
        self.mc = memcache.Client(['127.0.0.1:11211'], debug=0)

    def get_user(self, params):
        """
           To reduce number of db queries, get as much of the details here
        """

        phone = params['parameters']['phone']
        if Employee.objects.filter(tenant__tenant_type=RETAILER,
                                   user__phone=phone, user__is_active=True).exists():
            user = Employee.objects.select_related('tenant__retailer').get(tenant__tenant_type=RETAILER,
                                                                           user__phone=phone)
            ret = {'first_name': user.user.first_name,
                   'tenant_id': user.tenant.id,
                   'sub_region_id': user.tenant.retailer.sub_region.id,
                   'sub_region_title': user.tenant.retailer.sub_region.title,
                   'region_id': user.tenant.retailer.sub_region.region.id,
                   }

            return ret
        return None

    def get_session_state(self, params):
        '''
           sessionid is a unique identifier of each users ussd journey and state
           TODO: move session storage to memcache
        '''
        session_state = self.mc.get("{}{}".format(
            conf.RETAILER_SESSION_KEY, params['sessionid']))
        if not session_state:
            session_state = {}
            session_state['parameters'] = params
            session_state['currentMenu'] = self.home
            session_state['lastSeen'] = datetime.now()
            session_state['action'] = 'con'
            session_state['hasResponse'] = False
            session_state['sessionTime'] = 300
            session_state['sessionid'] = ''
            session_state['user'] = self.get_user(session_state)
            self.mc.set("{}{}".format(conf.RETAILER_SESSION_KEY, params['sessionid']),
                        session_state, session_state['sessionTime'])

            return session_state
        else:

            session_state['parameters']['request'] = params['request']
            session_state['parameters']['response'] = None
            return session_state

    def update_session_state(self, params):
        try:
            if 'action' in params:
                if params['action'].lower() == 'end':
                    params['currentMenu'] = self.home
                    params['sessionTime'] = 1  # expire session
            params['lastSeen'] = datetime.now()
            self.mc.set("{}{}".format(conf.RETAILER_SESSION_KEY,
                                      params['parameters']['sessionid']),
                        params, params['sessionTime'])
        except Exception as e:
            logging.exception(e)
        return params

    def call_next_menu(self, params):
        menu = params['currentMenu']
        if menu == 'root':
            menu = self.home

        menu = retailer_menus[menu]
        return menu().start(params)

    def get_response(self, response, request):

        action = response['action'].upper()
        resp = response['parameters']['response']
        resp = f'{action} {resp}'
        print(f'RESP: {resp}')
        request.write(resp.encode('utf8'))
        request.finish()

    def process_request(self, params):
        if params['parameters']["request"] == "#":
            params["currentMenu"] = self.home
        menu = params['currentMenu']
        if not menu in retailer_menus:
            menu = self.home
        menu = retailer_menus[menu]
        d = threads.deferToThread(menu().processAction, params)
        d.addCallback(self.call_next_menu)
        d.addCallback(self.update_session_state)
        return d

    def render_GET(self, request):
        return self.render_POST(request)

    def render_POST(self, request):
        params = {}
        for k, v in list(request.args.items()):
            params[k.lower().decode('utf8')] = v[0].decode('utf8')
        params['phone'] = params['msisdn']
        params['sessionid'] = params['phone']
        req = params['ussdstring'].split('*')
        params['request'] = req[len(req) - 1]

        try:
            d = self.process_request(self.get_session_state(params))
            d.addCallback(self.get_response, request)
        except Exception as e:
            logging.exception(e)
            return 'error'
        else:
            return server.NOT_DONE_YET

###  SESSION HANDLER ###


class SessionFactory(server.Session):
    sessionTimeout = 240
