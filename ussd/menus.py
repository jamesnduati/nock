#!/usr/bin/env python
import logging
import json
import logging
import redis
import requests


from ussd import conf
from ussd.menu_registration import registration_home
from apps.orders.models import Order
from apps.orders.choices import ORDER_STATUS

def Errback(f, resources):
    print(resources, f.getTraceback())
    f.trap()
    resources['response'] = conf.ERROR
    return resources

class home():
    def __init__(self):
        pass
    def processAction(self, resources):
        if not resources['user']:
           resources['currentMenu'] = 'registration_home'
           return registration_home().processAction(resources)
        resources['action']='con'
        req=resources['parameters']['request'].lower()
        if req == '1':
            resources['currentMenu'] = 'purchase_product_type'
            resources['parameters']['purchase_type']=conf.PURCHASE_CYLINDER
        elif req == '2':
            resources['currentMenu'] = 'purchase_product_type'
            resources['parameters']['purchase_type']=conf.PURCHASE_REFILL
        elif req == '3':
            resources['currentMenu'] = 'business_home'
        elif req == '4':
            resources['currentMenu'] = 'wait'
            resources['hasResponse'] = True
            resources['parameters']['response'] = 'Comming Soon'
        elif req == '5':
            resources['currentMenu'] = 'profile_home'

        return resources
    def start(self, resources):
        if not resources['user']:
            resources['currentMenu'] = 'registration_home'
            return registration_home().start(resources)
        if resources['user']['pending_beneficiary']:
            resources['action']='end'
            resources['parameters']['response'] ='Your request for Gas Yetu is still pending'
            return resources
        resources['action']='con'
        resources['parameters']['response'] ='Reply with:\n1.Buy New Cylinder'\
                '\n2.Cylinder Refill\n3.Locate Distributors - Retailers\n4.Other Products'\
                '\n5.My Profile'
        return resources
    def prefetch_orders(self, resources):
        '''
          fetch for atleast  one new order
        '''
        try:
            resources['parameters']['hasNewOrder'] = False
            order=Order.objects.filter(buyer__id=resources['parameters']['tenant_id'],order_status=ORDER_STATUS.New).first()
            if order:
                resources['parameters']['voucher']=order.voucher_number
        except Exception as e:
            logging.exception(e)
        return resources

class pin():
    '''
    Pin confirmation menu
    '''
    def __init__(self):
        pass

    def processAction(self, resources):
        req = resources['parameters']['request'].strip()
        if not req:
           return resources

        if not req.isdigit() or len(req) > conf.PIN_LENGTH or len(req)< conf.PIN_LENGTH:
            resources['parameters']['response']='Pin must be 4 digits'
            resources['hasResponse']=True
            return resources
        resources=self.authenticate(resources,req)
        if 'token' in resources:
            resources['currentMenu']=resources['nextMenu']
        else:
            resources['parameters']['response']='Wrong Pin suplied, enter pin:'
            resources['hasResponse']=True
        return resources

    def authenticate(self, resources, pin):
        try:
            request={
               "password": pin,
               "username": resources['parameters']['phone']
               }
            r=requests.post(f"{conf.API_BASE_URL}/account/login/",json=request)
            if r.status_code == requests.codes.ok:
                resp=r.json()
                if 'token' in resp:
                    resources['token'] = resp['token']
                    resources['password'] = pin # ???
        except Exception as e:
            logging.exception(e)
        return resources
    def start(self, resources):
        if not resources['hasResponse']:
            resources['parameters']['response'] = 'Enter the pin to confirm.'
            resources['hasResponse'] = False

        return resources

class payment_mode():
    def __init__(self):
        pass
    def processAction(self, resources):
        req = resources['parameters']['request'].strip()
        if not req or not req.isdigit():
           return resources
        if req == '1':
            resources['parameters']['payment_mode']='Wallet'
        elif req == '2':
            resources['parameters']['payment_mode']='Cash'
        if 'nextMenu' in resources:
            resources['currentMenu'] = resources['nextMenu']
        return resources

    def start(self, resources):
        if not resources['hasResponse']:
            resources['parameters']['response'] = 'Select Payment Mode:\n1.Wallet\n2.Cash'
            resources['hasResponse'] = False

        return resources


class wait():
    def __init__(self):
        pass
    def processAction(self, resources):
        return resources
    def start(self, resources):
        resources['currentMenu']='home'
        resources['action']='end'
        if not resources['hasResponse']:
            resources['parameters']['response']='Please wait while we process your transaction'
            resources['hasResponse']=False
        return resources

menus = {'home':home,
         'wait':wait,'pin':pin,'payment_mode':payment_mode,
        }

from ussd import menu_registration
for _cls in [i for i in dir(menu_registration)]:
    if _cls.startswith('registration_'):
        menus[_cls]=eval(f'menu_registration.{_cls}')

from ussd import menu_purchase
for _cls in [i for i in dir(menu_purchase)]:
    if _cls.startswith('purchase_'):
        menus[_cls]=eval(f'menu_purchase.{_cls}')
from ussd import menu_profile
for _cls in [i for i in dir(menu_profile)]:
    if _cls.startswith('profile_'):
        menus[_cls]=eval(f'menu_profile.{_cls}')

from ussd import menu_business
for _cls in [i for i in dir(menu_business)]:
    if _cls.startswith('business_'):
        menus[_cls]=eval(f'menu_business.{_cls}')

from ussd import menu_edit_profile
for _cls in [i for i in dir(menu_edit_profile)]:
    if _cls.startswith('regedit_'):
        menus[_cls]=eval(f'menu_edit_profile.{_cls}')

