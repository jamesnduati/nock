"""
Production specific settings.
"""

import os

import dj_database_url
import raven

from .base import *

APPLICATION_DIR = os.path.dirname(globals()['__file__'])

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': os.getenv('DB_NAME'),
        'USER': os.getenv('DB_USER'),
        'PASSWORD': os.getenv('DB_PASSWORD'),
        'HOST': os.getenv('DB_HOST'),
        'PORT': os.getenv('PORT'),
    }
}

EMAIL_BACKEND = "anymail.backends.mailgun.EmailBackend"
DEFAULT_FROM_EMAIL = "no-reply@gasyetu.co.ke"

# ANYMAIL
ANYMAIL = {
    "MAILGUN_API_KEY": os.getenv('MAILGUN_API_KEY'),
    "MAILGUN_SENDER_DOMAIN": 'mg.embupay.co.ke',
}

RAVEN_CONFIG = {
    'dsn': 'https://5da4fdbd15e644a88e3429de7d6b8187:c2ebb6c0d17a44fead2ad4b97061d353@sentry.io/1207553',
    # If you are using git, you can also automatically configure the
    # release based on the git info.
    'release': raven.fetch_git_sha(BASE_DIR),
    'environment': 'PROD'
}

INSTALLED_APPS = INSTALLED_APPS + ['raven.contrib.django.raven_compat', ]

ALLOWED_HOSTS = ['*']
