"""
Settings package initialization.
"""

import os
from django_envie import load_vars

# load and set environment variables from '.env.yml' or '.env.py' files with django_envie
load_vars()

# Ensure development settings are not used in testing and production:
if not os.getenv('CI') and not os.getenv('PROD'):
    from .development import *

if os.getenv('PROD') is not None:
    print('prod')
    from .production import *
