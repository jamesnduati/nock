"""Base url configuration for DRF.
The `urlpatterns` list routes URLs to the API views.
Example:
Using Class-based views
    1. Add an import:  from some_app.api import HomeAPIView
    2. Add a URL to urlpatterns:
        url(r'^api/home/$', HomeApiView.as_view(), name='home_api')
"""
from django.conf.urls import include, url

urlpatterns = [
    url(r'^', include('apps.orders.urls.api')),
    url(r'^account/', include('apps.accounts.urls.api')),
    url(r'^', include('apps.core.api_urls')),
    url(r'^surveys/', include('apps.surveys.urls.api')),
    url(r'^inventory/', include('apps.inventory.urls.api')),
    url(r'^products/', include('apps.products.urls.api')),
    url(r'^distribution/', include('apps.distribution.urls.api')),
    url(r'^finance/', include('apps.finance.urls.api')),
    url(r'^', include('apps.consumers.urls.api')),
]
