from django import forms
from django.conf import settings
from django.forms import BaseFormSet, ModelForm, SelectDateWidget
from django.forms.widgets import Select

from .models import Brand, Cylinder, Listing, Product


class CylinderForm(ModelForm):
    class Meta:
        model = Cylinder
        fields = (
            'title',
        )


class BrandForm(ModelForm):
    class Meta:
        model = Brand
        fields = (
            'title',
        )


class ProductForm(ModelForm):
    class Meta:
        model = Product
        fields = (
            'brand', 'cylinder'
        )

    def __init__(self, *args, **kwargs):
        disabled = kwargs.pop('disabled', None)
        super().__init__(*args, **kwargs)
        if disabled:
            self.fields['cylinder'].widget.attrs['disabled'] = True
            self.fields['brand'].widget.attrs['disabled'] = True


class ProductListingForm(ModelForm):
    class Meta:
        model = Listing
        fields = (
            'service', 'product', 'list_price', 'for_consumer'
        )

    def __init__(self, *args, **kwargs):
        super(ProductListingForm, self).__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        if instance and instance.pk:
            self.fields['service'].disabled = True
            self.fields['product'].disabled = True


class SerialForm(forms.Form):
    serial_number = forms.CharField(
        max_length=32, label="Enter Serial Number", required=False)


class BaseSerialsFormSet(BaseFormSet):
    def __init__(self, *args, **kwargs):
        super(BaseSerialsFormSet, self).__init__(*args, **kwargs)
