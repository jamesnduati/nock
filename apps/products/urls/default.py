from django.conf.urls import url

from ..views.default import (BrandCreateView, BrandDeleteView, BrandListView,
                             BrandUpdateView, CylinderCreateView,
                             CylinderDeleteView, CylinderListView,
                             CylinderUpdateView, ProductCreateView,
                             ProductDeleteView, ProductListingCreateView,
                             ProductListingDeactivateView,
                             ProductListingListView, ProductListingUpdateView,
                             ProductListView, ProductSerialListView,
                             ProductUpdateView, edit_serials)

urlpatterns = [
    url(r'^cylinders/$', CylinderListView.as_view(), name='cylinders'),
    url(r'^cylinders/(?P<pk>[0-9]+)/$',
        CylinderUpdateView.as_view(),
        name='cylinder-update'),
    url(r'^cylinders/(?P<pk>[0-9]+)/delete$',
        CylinderDeleteView.as_view(),
        name='cylinder-delete'),
    url(r'^cylinders/new/$', CylinderCreateView.as_view(), name="cylinder-create"),


    url(r'^brands/$', BrandListView.as_view(), name='brands'),
    url(r'^brands/(?P<pk>[0-9]+)/$',
        BrandUpdateView.as_view(),
        name='brand-update'),
    url(r'^brands/(?P<pk>[0-9]+)/delete$',
        BrandDeleteView.as_view(),
        name='brand-delete'),
    url(r'^brands/new/$', BrandCreateView.as_view(), name="brand-create"),

    url(r'^products/$', ProductListView.as_view(), name='products'),
    url(r'^products/(?P<pk>[0-9]+)/$',
        ProductUpdateView.as_view(),
        name='product-update'),
    url(r'^products/(?P<pk>[0-9]+)/delete$',
        ProductDeleteView.as_view(),
        name='product-delete'),
    url(r'^products/new/$', ProductCreateView.as_view(), name="product-create"),

    url(r'^product_serials/$', ProductSerialListView.as_view(), name='product-serials'),
    url(r'^product_serials/(?P<pk>[0-9a-f-]+)/edit/$', edit_serials, name='product-serials_edit'),

    url(r'^listings/$', ProductListingListView.as_view(), name='listings'),
    url(r'^listings/(?P<pk>[0-9]+)/$',
        ProductListingUpdateView.as_view(),
        name='listing-update'),
    url(r'^listings/(?P<pk>[0-9]+)/deactivate$',
        ProductListingDeactivateView.as_view(),
        name='listing-deactivate'),
    url(r'^listings/new/$', ProductListingCreateView.as_view(), name="listing-create"),
]
