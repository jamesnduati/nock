from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter
from ..views import api

product_router = DefaultRouter()

product_router.register(r'products', api.ProductViewSet)

product_router.register(r'listings', api.ListingViewSet)

product_router.register(r'brands', api.BrandViewSet)

product_router.register(r'cylinders', api.CylindersViewSet)

urlpatterns = [
    url(r'^', include(product_router.urls))
]
