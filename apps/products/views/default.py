from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import IntegrityError, transaction
from django.forms.models import inlineformset_factory
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse, reverse_lazy
from django.views.generic import (CreateView, DeleteView, ListView, UpdateView)
from rolepermissions.decorators import has_permission_decorator
from rolepermissions.mixins import HasRoleMixin

from apps.orders.models import ProductItem

from ..forms import BrandForm, CylinderForm, ProductForm, ProductListingForm
from ..models import Brand, Cylinder, Listing, Product


class CylinderListView(HasRoleMixin, LoginRequiredMixin, ListView):
    model = Cylinder
    template_name = 'products/cylinder/cylinder_list.html'
    context_object_name = 'cylinders'
    allowed_roles = ['system_admin', 'inventory_manager']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Cylinders'
        return context


class CylinderCreateView(HasRoleMixin, CreateView):
    model = Cylinder
    template_name = 'products/cylinder/cylinder.html'
    form_class = CylinderForm
    success_url = reverse_lazy('cylinders')
    allowed_roles = ['system_admin', 'inventory_manager']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Create New Cylinder'
        return context


class CylinderUpdateView(HasRoleMixin, UpdateView):
    model = Cylinder
    template_name = 'products/cylinder/cylinder.html'
    form_class = CylinderForm
    success_url = reverse_lazy('cylinders')
    allowed_roles = ['system_admin', 'inventory_manager']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Update Cylinder'
        return context


class CylinderDeleteView(HasRoleMixin, DeleteView):
    model = Cylinder
    template_name = 'products/cylinder/cylinder_delete.html'
    success_url = reverse_lazy('cylinders')
    allowed_roles = ['system_admin', 'inventory_manager']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Confirm Delete'
        return context


class BrandListView(HasRoleMixin, LoginRequiredMixin, ListView):
    model = Brand
    template_name = 'products/brand/brand_list.html'
    context_object_name = 'brands'
    allowed_roles = ['system_admin', 'inventory_manager']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Brands'
        return context


class BrandCreateView(HasRoleMixin, CreateView):
    model = Brand
    template_name = 'products/brand/brand.html'
    form_class = BrandForm
    success_url = reverse_lazy('brands')
    allowed_roles = ['system_admin', 'inventory_manager']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Create New Brand'
        return context


class BrandUpdateView(HasRoleMixin, UpdateView):
    model = Brand
    template_name = 'products/brand/brand.html'
    form_class = BrandForm
    success_url = reverse_lazy('brands')
    allowed_roles = ['system_admin', 'inventory_manager']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Update Brand'
        return context


class BrandDeleteView(HasRoleMixin, DeleteView):
    model = Brand
    template_name = 'products/brand/brand_delete.html'
    success_url = reverse_lazy('brands')
    allowed_roles = ['system_admin', 'inventory_manager']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Confirm Delete'
        return context


class ProductListView(HasRoleMixin, LoginRequiredMixin, ListView):
    model = Product
    template_name = 'products/product/product_list.html'
    context_object_name = 'products'
    allowed_roles = ['system_admin', 'inventory_manager']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Products'
        return context


class ProductCreateView(HasRoleMixin, CreateView):
    model = Product
    template_name = 'products/product/product.html'
    form_class = ProductForm
    success_url = reverse_lazy('products')
    allowed_roles = ['system_admin', 'inventory_manager']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Create New Product'
        return context


class ProductUpdateView(HasRoleMixin, UpdateView):
    model = Product
    template_name = 'products/product/product.html'
    form_class = ProductForm
    success_url = reverse_lazy('products')
    allowed_roles = ['system_admin', 'inventory_manager']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Update Product'
        return context


class ProductDeleteView(HasRoleMixin, DeleteView):
    model = Product
    template_name = 'products/product/product_delete.html'
    success_url = reverse_lazy('products')
    allowed_roles = ['system_admin', 'inventory_manager']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Confirm Delete'
        return context


class ProductListingCreateView(HasRoleMixin, CreateView):
    model = Listing
    template_name = 'products/listings/listing.html'
    form_class = ProductListingForm
    success_url = reverse_lazy('listings')
    allowed_roles = ['system_admin', 'distributor_admin',
                     'retailer_admin', 'inventory_manager']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Create New Product Listing'
        return context

    def form_valid(self, form):
        user = self.request.user
        tenant = user.employee.tenant
        listing = form.instance
        listing.tenant = tenant
        try:
            listing.save()
        except IntegrityError:
            messages.add_message(
                self.request, messages.ERROR, "Sorry, The Product Listing already exists!")
            return self.form_invalid(form)
        return super(ProductListingCreateView, self).form_valid(form)


class ProductListingDeactivateView(HasRoleMixin, DeleteView):
    model = Listing
    template_name = 'products/listings/listing_deactivate.html'
    success_url = reverse_lazy('listings')
    allowed_roles = ['system_admin', 'distributor_admin',
                     'retailer_admin', 'inventory_manager']

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        self.object.is_active = False
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Deactivate Product Listing'
        return context


class ProductListingUpdateView(HasRoleMixin, UpdateView):
    model = Listing
    template_name = 'products/listings/listing.html'
    form_class = ProductListingForm
    success_url = reverse_lazy('listings')
    allowed_roles = ['system_admin', 'distributor_admin',
                     'retailer_admin', 'inventory_manager']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Update Product Listing'
        return context


class ProductListingListView(HasRoleMixin, LoginRequiredMixin, ListView):
    model = Listing
    template_name = 'products/listings/listing_list.html'
    context_object_name = 'listings'
    allowed_roles = ['system_admin', 'distributor_admin',
                     'retailer_admin', 'inventory_manager']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Product Listings'
        return context

    def get_queryset(self):
        tenant = self.request.user.employee.tenant
        return Listing.objects.filter(tenant=tenant)


class ProductSerialListView(HasRoleMixin, LoginRequiredMixin, ListView):
    model = Product
    template_name = "products/product_serials/product_serials_list.html"
    context_object_name = 'products'
    allowed_roles = ['system_admin', 'distributor_admin',
                     'retailer_admin', 'inventory_manager']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Product Serials'
        return context

    def get_queryset(self):
        return Product.objects.filter()


@login_required
@has_permission_decorator('edit_serials')
def edit_serials(request, pk):
    product = get_object_or_404(Product, pk=pk)
    ProductSerialFormSet = inlineformset_factory(
        Product, ProductItem, fields=('product', 'serial_no'))
    if request.method == 'POST':
        product_form = ProductForm(
            request.POST, instance=product, disabled=False)
        if product_form.is_valid():
            product_serial_formset = ProductSerialFormSet(
                request.POST, instance=product)
            if product_serial_formset.is_valid():
                try:
                    with transaction.atomic():
                        product_serial_formset.save()
                        return redirect(reverse('product-serials'))
                except IntegrityError as e:
                    messages.error(
                        request, f'There was an error saving.{e}')
                    return redirect(reverse('product-serials_edit'))
    else:
        product_form = ProductForm(instance=product, disabled=False)
        product_serial_formset = ProductSerialFormSet(instance=product)
    context = {
        'product_form': product_form,
        'product_serial_formset': product_serial_formset,
        'display': 'Edit Product Serial',
    }

    return render(request, 'products/product_serials/product_serial_edit.html', context)
