from django.apps import apps
from django.contrib import admin

from .models import DeliveryNote, Order, OrderItem, ProductItem, ProductMovement, ProductItemUser

admin.site.register(DeliveryNote)


class OrderItemInline(admin.TabularInline):
    model = OrderItem
    fields = ('listing', 'quantity')
    readonly_fields = ('cost',)


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    inlines = [
        OrderItemInline,
    ]
    list_display = ('order_number','voucher_number', 'order_date', 'buyer',
                    'seller', 'order_status', 'total_cost')


@admin.register(OrderItem)
class OrderItemAdmin(admin.ModelAdmin):
    list_display = ('listing', 'listing', 'quantity', 'cost')


@admin.register(ProductItem)
class ProductItemAdmin(admin.ModelAdmin):
    pass


@admin.register(ProductMovement)
class ProductMovementAdmin(admin.ModelAdmin):
    list_display = ('order', 'source', 'destination', 'product_item')

@admin.register(ProductItemUser)
class ProductItemUserAdmin(admin.ModelAdmin):
    list_display = ('product_item', 'tenant')

