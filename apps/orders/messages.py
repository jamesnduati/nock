from django.utils.translation import ugettext as _

successful_gas_yetu_purchase = _(
    "You have successfully purchased Gas Yetu Cylinder. Your Voucher Number is %s. Thank you for Buying Gas Yetu")
successful_gas_yetu_refill = _(
    "You have successfully refilled Gas Yetu Cylinder. Your Voucher Number is %s. Thank you for Buying Gas Yetu.")
successful_purchase_fulfill = _(
    "Your order of New cylinder purchase with serial numbers %s has been fulfilled successfully."
    " Thank you for Buying Gas Yetu.")
successful_refill_fulfill = _(
    "Your order of cylinder refill with serial numbers %s has been fulfilled successfully."
    " Thank you for Buying Gas Yetu.")
