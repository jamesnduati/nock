import os
from django.db import transaction
from django.utils import timezone
from django.utils.translation import ugettext as _
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

from apps.accounts.choices import BENEFICIARY, CONSUMER, EMPLOYEE
from apps.accounts.utils import Wallet, send_sms
from apps.distribution.models import Distributor, Retailer
from apps.inventory.models import CurrentStock
from apps.products.models import Listing, Product
from apps.tenants.choices import DISTRIBUTOR, RETAILER, TENANT_TYPE

from .choices import MODE_OF_PAYMENT, ORDER_STATUS
from .models import DeliveryNote, Order, OrderItem, ProductItem, ProductMovement
from .utils import get_voucher_number, process_order_express_input


class ProductMovementSerializer(serializers.ModelSerializer):
    """Product Movement Model Serializer. """
    class Meta:
        model = ProductMovement
        fields = ('product_item',)
        depth = 2


class OrderListingSerializer(serializers.ModelSerializer):
    """OrderItem Model Serializer. """

    listing_id = serializers.IntegerField()
    quantity = serializers.IntegerField()
    product_name = serializers.ReadOnlyField(
        source='listing.product.title')

    class Meta:
        model = OrderItem
        fields = ('listing_id', 'product_name', 'quantity', 'cost')
        read_only_fields = ('id',)


class OrderItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = ('listing', 'quantity')
        read_only_fields = ('id', 'cost')


class CylinderSerialSerializer(serializers.Serializer):
    serial_no = serializers.CharField()


class ReturnedCylinderSerializer(serializers.Serializer):
    serial_no = serializers.CharField()
    listing_id = serializers.IntegerField()


class OrderFulfillmentSerializer(serializers.Serializer):
    assigned_cylinder_serials = CylinderSerialSerializer(
        many=True, required=True)
    returned_cylinder_serials = ReturnedCylinderSerializer(
        many=True, required=False)
    password = serializers.CharField()

    def validate_assigned_cylinder_serials(self, assigned_serials):
        for serial in assigned_serials:
            serial = list(serial.items())[0][1]
            try:
                product_item = ProductItem.objects.get(serial_no=serial)
            except ProductItem.DoesNotExist:
                raise serializers.ValidationError(
                    "Product with given serial number %s does not exist" % serial)
        return assigned_serials

    def validate_returned_cylinder_serials(self, returned_serials):
        for serial in returned_serials:
            serial = list(serial.items())[0][1]
            try:
                product_item = ProductItem.objects.get(serial_no=serial)
            except ProductItem.DoesNotExist:
                raise serializers.ValidationError(
                    "Product with given serial number %s does not exist" % serial)

        return returned_serials

    # def validate_mode_of_payment(self, mode_of_payment):
    #     if mode_of_payment not in ['Cash', 'Wallet']:
    #         raise serializers.ValidationError({
    #             'error': "Mode of payment has to be either Cash or Wallet"
    #         })
    #     return mode_of_payment


class OrderReturnedSerializer(serializers.Serializer):
    returned_cylinder_serials = ReturnedCylinderSerializer(
        many=True, required=True)

    def validate_returned_cylinder_serials(self, returned_serials):
        for serial in returned_serials:
            serial = list(serial.items())[0][1]
            try:
                product_item = ProductItem.objects.get(serial_no=serial)
            except ProductItem.DoesNotExist:
                raise serializers.ValidationError(
                    "Product with given serial number %s does not exist" % serial)
        return returned_serials


class OrderSerializer(serializers.ModelSerializer):
    """Order Model Serializer. """
    order_items = OrderListingSerializer(many=True, required=True)
    returned_cylinder_serials = CylinderSerialSerializer(
        many=True, required=False)
    buyer_name = serializers.ReadOnlyField(
        source='get_buyer_name')
    service = serializers.ReadOnlyField(
        source='get_service')
    seller_name = serializers.ReadOnlyField(source='seller.title')
    password = serializers.CharField(write_only=True)
    productmovement = ProductMovementSerializer(
        source='productmovement_set', many=True, required=False)

    class Meta:
        model = Order
        fields = ('buyer', 'buyer_name', 'seller_name', 'seller', 'service', 'order_number', 'mode_of_payment', 'voucher_number', 'order_date',
                  'order_status', 'seller_code', 'total_cost', 'order_items', 'password',
                  'returned_cylinder_serials', 'returned_cylinders_serials', 'issued_cylinders_serials', 'productmovement')
        read_only_fields = ('total_cost', 'order_number', 'returned_cylinders_serials', 'issued_cylinders_serials'
                            'voucher_number', 'password', 'productmovement')
        extra_kwargs = {
            'mode_of_payment': {'required': True},
            'voucher_number': {'required': False},
            'status': {'required': False}
        }

    def validate_password(self, password):
        return password

    def validate(self, data):
        buyer = self.context['request'].user.profile.tenant
        if buyer.tenant_type == TENANT_TYPE.Consumer:
            try:
                order = Order.objects.filter(buyer=buyer, order_status=ORDER_STATUS.New).latest()
                raise ValidationError({
                    'msg': f'Sorry, cannot process order as an order of voucher number {order.voucher_number} is pending fulfillment.Please await fulfillment first.'
                })
            except Order.DoesNotExist:
                pass
            if self.context['request'].user.consumer.iprs_verified == False:
                raise ValidationError({'msg':'You have not passed IPRS verification, '\
                                       'Edit your detaildetails to math your ID'})

        return data

    def validate_order_items(self, order_items):

        if len(order_items) == 0:
            raise ValidationError('Missing order_details')
        try:
            order_tenant = Listing.objects.get(
                pk=order_items[0]['listing_id']).tenant
        except Listing.DoesNotExist:
            raise ValidationError('Listing does not exist')

        for order_item in order_items:
            try:
                Listing.objects.get(pk=order_item['listing_id'])
            except Listing.DoesNotExist:
                raise ValidationError('Listing does not exist')
            else:
                listing = Listing.objects.get(pk=order_item['listing_id'])
                stock, _ = CurrentStock.objects.get_or_create(
                    tenant=order_tenant, product=listing.product)
                available = stock.no_of

                if int(order_item['quantity']) > available:
                    raise ValidationError(
                        'The requested listing {} of quantity {} is not available at the moment'.format(
                            order_item['listing_id'], order_item['quantity']
                        ))

                if listing.tenant != order_tenant:
                    raise ValidationError(
                        'All the posted listings should be belong to a singe Distributor/Retailer for the order to be '
                        'fulfilled'
                    )

        return order_items

    def validate_seller_code(self, seller_code):
        if Distributor.objects.filter(enabled=True, agent_number=seller_code).exists() or Retailer.objects.filter(
                enabled=True, agent_number=seller_code).exists():
            return seller_code
        else:
            raise serializers.ValidationError(
                _("The seller code is invalid"))

    @transaction.atomic
    def create(self, validated_data):
        self.seller_code = 0
        order_items = validated_data['order_items']
        user = self.context['request'].user
        if user.profile_type in [CONSUMER, BENEFICIARY]:
            self.seller = Listing.objects.get(
                pk=order_items[0]['listing_id']).tenant
        elif user.profile_type == EMPLOYEE:
            employee = user.employee
            if employee.tenant.tenant_type == DISTRIBUTOR:
                distributor = Distributor.objects.get(
                    distributor_tenant=employee.tenant)
                distributor_number = distributor.distributor_number
                gas_importer = distributor.gas_importer
                self.seller = gas_importer.gas_importer_tenant
                self.seller_code = gas_importer.agent_number
                self.buyer_phone = gas_importer.contact_phone
                self.agent_email = distributor.jp_agent_email

            elif employee.tenant.tenant_type == RETAILER:
                retailer = Retailer.objects.get(
                    retailer_tenant=employee.tenant)
                distributor = retailer.distributor
                self.seller = distributor.distributor_tenant
                self.seller_code = distributor.agent_number
                self.buyer_phone = distributor.contact_phone
                self.agent_email = retailer.jp_agent_email

        buyer = self.context['request'].user.profile.tenant

        order_date = timezone.now()
        voucher_number = get_voucher_number()
        order = Order.objects.create(
            buyer=buyer,
            seller=self.seller,
            order_status=ORDER_STATUS.New,
            order_date=order_date,
            seller_code=validated_data.get('seller_code', self.seller_code),
            voucher_number=voucher_number,
            mode_of_payment=validated_data['mode_of_payment']
        )
        order_oracle_items = []
        for order_item in order_items:
            data={}
            listing_id = order_item['listing_id']
            quantity = order_item['quantity']

            listing = Listing.objects.get(pk=listing_id)

            OrderItem.objects.create(
                order=order,
                listing=listing,
                quantity=quantity
            )
            data['INVENTORY_ITEM'] = listing.product.title
            data['SHIP_FROM_ORG'] = str(os.getenv('NOC_SHIPPING_ORG'))
            data['ORDERED_QUANTITY'] = quantity
            order_oracle_items.append(data)

        """
        Oracle integration
        """
        if user.profile_type == EMPLOYEE:
            employee = user.employee
            if employee.tenant.tenant_type == DISTRIBUTOR:
                r = process_order_express_input(order_oracle_items, distributor_number, order_number)
                if r['OutputParameters']['X_RETURN_STATUS']!='S':
                    raise serializers.ValidationError({
                        'error': r['OutputParameters']['X_RETURN_MESSAGE']
                    })
        """
        Wallet Payment
        """
        if user.profile_type in [CONSUMER, BENEFICIARY]:
            if validated_data['mode_of_payment'] == MODE_OF_PAYMENT.Wallet:
                wallet = Wallet(user=user, password=validated_data['password'])
                result = wallet.makeWalletPayment(order=order)
                if result['code'] != 0:
                    raise serializers.ValidationError({
                        'error': result['data']
                    })
                if user.profile_type == BENEFICIARY:
                    beneficiary = user.profile
                    beneficiary.has_benefited = True
                    beneficiary.save()

        elif user.profile_type == EMPLOYEE:
            employee = user.employee
            if employee.tenant.tenant_type in [DISTRIBUTOR, RETAILER]:
                """
                Agency Wallet Payment
                """
                voucher_number = order.voucher_number
                merchant_code = self.seller_code
                total_cost = order.total_cost

                wallet = Wallet(password=validated_data['password'])
                result = wallet.makeAgencyPayment(
                    agent_code=self.context['request'].tenant_agent_code,
                    agent_key=self.context['request'].tenant_agent_key,
                    order=order,
                    customer_phone=self.context['request'].tenant_phone
                )
                if result['code'] != 0:
                    raise serializers.ValidationError({
                        'error': result['data']
                    })

        listing = Listing.objects.get(pk=order_items[0]['listing_id'])
        if listing.service == 'purchase':
            message = _(
                "You have successfully purchased Gas Yetu Cylinder. Your Voucher Number is %s. Thank you for Buying Gas Yetu" % (voucher_number))
        else:
            message = _(
                "You have successfully refilled Gas Yetu Cylinder. Your Voucher Number is %s. Thank you for Buying Gas Yetu." % (voucher_number))
        send_sms(
            phone_number=self.context['request'].user.phone, sms_body=message)

        return order

    def update(self, instance, validated_data):
        instance.buyer = validated_data.get('buyer', instance.buyer)
        instance.seller = validated_data.get('seller', instance.seller)
        instance.order_status = validated_data.get(
            'order_status', instance.order_status)
        instance.save()
        return instance


class DeliveryNoteSerializer(serializers.ModelSerializer):
    """DeliveryNote Model Serializer. """
    class Meta:
        model = DeliveryNote
        fields = ('delivery_note_status', 'comments', 'delivered_by',
                  'received_by', 'received_date', 'delivered_date',)
        read_only_fields = ('id', 'received_date', 'delivered_date',)


class SerialNumberSerializer(serializers.ModelSerializer):
    """Serial numbers Model Serializer. """
    product_id = serializers.ReadOnlyField(source="product_item.product.id")
    serial = serializers.ReadOnlyField(source='product_item.serial_no')

    class Meta:
        model = ProductMovement
        fields = ('product_id', 'serial',)
        read_only_fields = ('product_id', 'serial',)
