import xlsxwriter
import tempfile
from io import BytesIO
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect, HttpResponse
from django.template.loader import render_to_string
from weasyprint import HTML
from django.shortcuts import render, reverse
from django.urls import reverse_lazy
from django.views.generic import (CreateView, DeleteView, DetailView, ListView,
                                  RedirectView, UpdateView, View)

from apps.tenants.models import Tenant
from apps.orders.models import Order, OrderItem
from apps.products.models import Listing, Product
from apps.orders.choices import ORDER_STATUS
from datetime import date
from config.settings.base import STATIC_ROOT, STATIC_URL, BASE_DIR
static_path = BASE_DIR + STATIC_URL


@login_required
def get_orders(request):
    orders = Order.objects.all()
    report_data = []
    if request.method == "GET":
        for order in orders:
            data = {}
            data['order_date'] = order.order_date
            data['voucher_number'] = order.voucher_number
            data['buyer'] = order.buyer.title
            data['seller'] = order.seller.title
            data['status'] = order.get_order_status_display
            data['mode_of_payment'] = order.get_mode_of_payment_display
            data['cost'] = order.total_cost
            report_data.append(data)
        return render(request, 'reports/orders.html', {'orders': report_data})
    else:
        max_value = request.POST['max-date']
        min_value = request.POST['min-date']
        for order in orders:
            data = {}
            if order.order_date.strftime('%d-%m-%Y') >= min_value and order.order_date.strftime('%d-%m-%Y') <= max_value:
                data['order_date'] = order.order_date
                data['voucher_number'] = order.voucher_number
                data['buyer'] = order.buyer.title
                data['seller'] = order.seller.title
                data['status'] = order.get_order_status_display
                data['mode_of_payment'] = order.get_mode_of_payment_display
                data['cost'] = order.total_cost
                report_data.append(data)
        return render(request, 'reports/orders.html', {'orders': report_data})


@login_required
def get_order_details(request):
    voucher_number = request.GET['voucher_number']
    order = Order.objects.get(voucher_number=voucher_number)
    order_items = OrderItem.objects.filter(order_id=order.order_number)
    product_details = []
    for order_item in order_items:
        data = {}
        listing = Listing.objects.get(id=order_item.listing_id)
        data['price'] = listing.list_price
        product = Product.objects.get(id=listing.product_id)
        data['product'] = product
        data['issued_serial_number'] = order.issued_cylinders_serials
        data['returned_serial_number'] = order.returned_cylinders_serials
        product_details.append(data)
    return render(request, 'reports/order_details.html', {'order': order, "product_details": product_details})


@login_required
def generate_pdf(request):
    min_value = request.GET['min_date']
    max_value = request.GET['max_date']
    tenant = request.tenant
    report_data = []
    orders = Order.objects.all()
    for order in orders:
        data = {}
        if order.order_date.strftime('%d-%m-%Y') >= min_value and order.order_date.strftime('%d-%m-%Y') <= max_value:
            data['order_date'] = order.order_date
            data['voucher_number'] = order.voucher_number
            data['buyer'] = order.buyer.title
            data['seller'] = order.seller.title
            data['status'] = ORDER_STATUS[order.order_status]
            data['mode_of_payment'] = order.mode_of_payment
            data['cost'] = order.total_cost
            report_data.append(data)
        else:
            data['order_date'] = order.order_date
            data['voucher_number'] = order.voucher_number
            data['buyer'] = order.buyer.title
            data['seller'] = order.seller.title
            data['status'] = ORDER_STATUS[order.order_status]
            data['mode_of_payment'] = order.mode_of_payment
            data['cost'] = order.total_cost
            report_data.append(data)
    return pdf(report_data, tenant)

@login_required
def generate_excel(request):
    min_value = request.GET['min_date']
    max_value = request.GET['max_date']
    tenant = request.tenant
    report_data = []
    orders = Order.objects.all()
    for order in orders:
        data = {}
        if order.order_date.strftime('%d-%m-%Y') >= min_value and order.order_date.strftime('%d-%m-%Y') <= max_value:
            data['order_date'] = order.order_date.strftime('%d-%m-%Y')
            data['voucher_number'] = str(order.voucher_number)
            data['buyer'] = order.buyer.title
            data['seller'] = order.seller.title
            data['status'] = str(ORDER_STATUS[order.order_status])
            data['mode_of_payment'] = order.mode_of_payment
            data['cost'] = str(order.total_cost)
            report_data.append(data)
        else:
            data['order_date'] = order.order_date.strftime('%d-%m-%Y')
            data['voucher_number'] = order.voucher_number
            data['buyer'] = order.buyer.title
            data['seller'] = order.seller.title
            data['status'] = str(ORDER_STATUS[order.order_status])
            data['mode_of_payment'] = order.mode_of_payment
            data['cost'] = str(order.total_cost)
            report_data.append(data)
    return excel(report_data, tenant)


def pdf(report_data, tenant):
    # Rendered
    html_string = render_to_string(
        'reports/orders_report.html', {
            'reports': report_data,
            'date': date.today(),
            'static': static_path,
            'logo': static_path+'images/logo.png',
            'tenant': tenant,
        }
    )
    html = HTML(string=html_string).write_pdf()
    # Creating http response
    response = HttpResponse(html, content_type='application/pdf')
    response['Content-Disposition'] = 'filename=distribution_report.pdf'
    return response


def excel(report_data, tenant):
    output = BytesIO()
    # Create a workbook and add a worksheet.
    workbook = xlsxwriter.Workbook(output, {'in_memory': True})
    worksheet = workbook.add_worksheet()

    # merge format for the cells
    merge_format = workbook.add_format(
        {
            'bold': 1,
            'align': 'center',
            'valign': 'vcenter'
        }
    )

    # merge logo cells
    # Merge 3 cells.
    worksheet.merge_range('C1:D4', '', merge_format)

    # setting the header with a logo
    image_width = 128.0
    image_height = 40.0

    cell_width = 64.0
    cell_height = 20.0

    x_scale = cell_width/image_width
    y_scale = cell_height/image_height

    logo = static_path+'images/logo.png'
    worksheet.insert_image('C1', logo,
                           {'x_scale': x_scale, 'y_scale': y_scale}
                           )
    # Add a bold format to use to highlight cells.
    bold = workbook.add_format({'bold': True})

    # adding the header under the logo
    worksheet.merge_range('C5:D5', '', merge_format)
    worksheet.write('C5', 'Orders Report', bold)
    worksheet.merge_range('A1:B4', '', merge_format)
    worksheet.write(
        'A1', 'Printed by: '+str(tenant)+'\nDate: '+str(date.today()))

    worksheet.merge_range('E1:G4', '', merge_format)
    worksheet.write(
        'E1', 'National Oil Corporation of Kenya \nKAWI house – South C Redcross Road, \nOff Popo Road \nTelephone: +254-20-695 2000')

    # Write some data headers.
    worksheet.write('A6', 'Date', bold)
    worksheet.write('B6', 'Voucher Number', bold)
    worksheet.write('C6', 'Buyer', bold)
    worksheet.write('D6', 'Seller', bold)
    worksheet.write('E6', 'Status', bold)
    worksheet.write('F6', 'Payment Method', bold)
    worksheet.write('G6', 'Total Cost', bold)

    # Start from the first cell below the headers.
    row = 6
    col = 0


    # Iterate over the data and write it out row by row.
    rows = list(report_data[0].keys())
    for i, row in enumerate(report_data):
        i += 6
        for j, col in enumerate(rows):
            worksheet.write(i, j, row[col])

    workbook.close()
    output.seek(0)
    # response
    filename = 'orders-'+str(date.today())+'.xls'
    response = HttpResponse(output.read(), content_type="application/ms-excel")
    response['Content-Disposition'] = "attachment; filename="+filename
    return response
