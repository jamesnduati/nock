import collections
import json
import os

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import IntegrityError, transaction
from django.db.models import Sum
from django.forms.formsets import formset_factory
from django.forms.models import inlineformset_factory
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import reverse
from django.utils import timezone
from django.views.generic import DetailView, ListView
from rolepermissions.decorators import has_permission_decorator
from rolepermissions.mixins import HasPermissionsMixin, HasRoleMixin

from apps.accounts.choices import PROFILE_TYPES
from apps.accounts.utils import Wallet, send_sms
from apps.consumers.models import Beneficiary
from apps.distribution.models import Distributor, GasImporter, Retailer
from apps.orders.models import Order, OrderItem
from apps.products.choices import SERVICE_CHOICES
from apps.products.models import Listing
from apps.tenants.choices import CONSUMER, DISTRIBUTOR, RETAILER
from apps.tenants.models import Tenant

from ..choices import MODE_OF_PAYMENT, ORDER_STATUS
from ..forms import (BaseSerialsFormSet, OrderForm, OrderFulfillForm,
                     OrderPreviewForm, SerialsForm)
from ..messages import (successful_gas_yetu_purchase,
                        successful_gas_yetu_refill,
                        successful_purchase_fulfill, successful_refill_fulfill)
from ..mixins import OrderRecipientDetailsMixin
from ..utils import get_voucher_number, process_order_express_input


class MyOrdersView(HasRoleMixin, LoginRequiredMixin, ListView):
    model = Order
    template_name = 'orders/my_orders.html'
    context_object_name = 'orders'
    allowed_roles = ['distributor_admin',
                     'retailer_admin', 'beneficiary', 'consumer']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'My Orders'
        return context

    def get_queryset(self):
        queryset = super(MyOrdersView, self).get_queryset()
        queryset = queryset.filter(buyer=self.request.tenant)
        return queryset


class NewOrdersView(HasRoleMixin, LoginRequiredMixin, ListView):
    model = Order
    template_name = 'orders/new_orders.html'
    context_object_name = 'orders'
    allowed_roles = ['distributor_admin',
                     'retailer_admin', 'system_admin', 'beneficiary', 'consumer']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'New Orders'
        return context

    def get_queryset(self):
        queryset = super(NewOrdersView, self).get_queryset()
        return queryset.filter(seller=self.request.tenant, order_status=ORDER_STATUS.New)


class OrderFulfillView(HasRoleMixin, LoginRequiredMixin, OrderRecipientDetailsMixin, DetailView):
    model = Order
    template_name = "orders/fulfill_order.html"
    allowed_roles = ['distributor_admin', 'retailer_admin',
                     'system_admin', 'finance_manager']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        order = self.object

        sum_items = order.order_items.aggregate(Sum('quantity'))[
            'quantity__sum']
        SerialsFormSet = formset_factory(
            SerialsForm, extra=sum_items, formset=BaseSerialsFormSet)
        formset = SerialsFormSet(form_kwargs={
                                 'service': order.get_service()})

        context['form'] = OrderFulfillForm(
            mode_of_payment=order.mode_of_payment)
        context['formset'] = formset

        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        order = self.object
        context = self.get_context_data(**kwargs)

        SerialsFormSet = formset_factory(
            SerialsForm, formset=BaseSerialsFormSet)

        form = OrderFulfillForm(
            request.POST, mode_of_payment=order.mode_of_payment)
        serials_formset = SerialsFormSet(request.POST, form_kwargs={
            'service': order.get_service()})

        context.update({
            'form': form,
            'formset': serials_formset,
        })

        if order.buyer.tenant_type == CONSUMER:
            self.buyer_phone = order.buyer.consumer_set.first().user.phone
        listings = list(order.order_items.values('listing', 'quantity'))
        serials = {'returned': [], 'issued': []}
        returned_serials_data = []
        issued_serials_data = []
        if form.is_valid() and serials_formset.is_valid():
            for i, serial_form in enumerate(serials_formset):
                issued_serial = serial_form.cleaned_data.get('issued_serials')
                returned_serial = serial_form.cleaned_data.get(
                    'received_serials')
                serials['issued'].append(issued_serial)
                serials['returned'].append(returned_serial)
            for listing in listings:
                for i in range(listing['quantity']):
                    if serials['returned'][i]:
                        returned = [
                            ('listing_id', listing['listing']),
                            ('serial_no', serials['returned'][i])
                        ]
                        returned_serials_data.append(
                            collections.OrderedDict(returned))
                    if serials['issued'][i]:
                        issued = [
                            ('serial_no', serials['issued'][i])
                        ]
                        issued_serials_data.append(
                            collections.OrderedDict(issued))
        else:
            return self.render_to_response(context=context)

        buyer = order.buyer
        seller = order.seller
        if buyer.tenant_type == CONSUMER:
            consumer_user = buyer.consumer_set.first().user
            self.buyer_phone = consumer_user.phone

            if seller.tenant_type == RETAILER:
                retailer = seller.retailer
                self.agent_email = retailer.jp_agent_email

            elif seller.tenant_type == DISTRIBUTOR:
                distributor = seller.distributor
                self.agent_email = distributor.jp_agent_email

            if order.mode_of_payment == MODE_OF_PAYMENT.Cash:
                wallet = Wallet(password=form.cleaned_data['password'])
                result = wallet.makeAgencyPayment(
                    agent_code=request.tenant_agent_code,
                    agent_key=request.tenant_agent_key,
                    order=order,
                    customer_phone=request.tenant_phone
                )
                if result['code'] != 0:
                    messages.add_message(
                        self.request, messages.ERROR, result['data'])
                    return self.render_to_response(context=context)

        elif buyer.tenant_type == RETAILER:
            retailer = buyer.retailer
            self.buyer_phone = retailer.contact_phone

        elif buyer.tenant_type == DISTRIBUTOR:
            distributor = buyer.distributor
            self.buyer_phone = distributor.contact_phone

        if issued_serials_data:
            order.complete_issued_cylinder(*issued_serials_data)
        else:
            order.order_status = ORDER_STATUS.Completed
            order.issued_cylinder_check = True
            order.save()

        try:
            beneficiary = Beneficiary.objects.get(tenant=order.buyer)
            if beneficiary.has_benefited is False:
                gas_yetu_items = [item.listing for item in order.order_items.all(
                ) if item.listing.product.gas_yetu is True]
                if gas_yetu_items:
                    beneficiary.has_benefited = True
                    beneficiary.save()
        except(Beneficiary.DoesNotExist):
            print('not exists')

        cylinder_serials = ",".join(serials['issued'])
        if order.get_service() == 'refill':
            if returned_serials_data:
                order.complete_order_returned_cylinder_serials(
                    *returned_serials_data)
            message = successful_refill_fulfill

        if order.get_service() == 'purchase':
            message = successful_purchase_fulfill

        send_sms(phone_number=self.buyer_phone,
                 sms_body=message % cylinder_serials)
        messages.add_message(
            self.request, messages.SUCCESS, "Order fulfilled successfully!")

        return self.render_to_response(context=context)


@login_required
@has_permission_decorator('can_make_order')
def create_order(request):
    seller_name = ""
    tenant = ""
    seller = ""
    seller_code = ""
    OrderItemFormSet = inlineformset_factory(
        Order, OrderItem, fields=('listing', 'quantity'))
    if request.user.profile_type == PROFILE_TYPES.Employee:
        if request.tenant.tenant_type == RETAILER:
            retailer = Retailer.objects.get(
                retailer_tenant=request.user.profile.tenant)
            seller_code = retailer.distributor.agent_number
            tenant = retailer.distributor.distributor_tenant.id
            seller_name = retailer.distributor.trading_name
        elif request.tenant.tenant_type == DISTRIBUTOR:
            distributor = request.tenant.distributor
            seller_code = distributor.gas_importer.agent_number
            tenant = distributor.gas_importer.gas_importer_tenant.id
            seller_name = distributor.gas_importer.trading_name
    if request.method == 'POST':
        order_form = OrderForm(request.POST, user=request.user)
        order_item_formset = OrderItemFormSet(request.POST)

        if order_form.is_valid() and order_item_formset.is_valid():
            mode_of_payment = order_form.cleaned_data.get('mode_of_payment')
            if request.user.profile_type in [PROFILE_TYPES.Consumer, PROFILE_TYPES.Beneficiary]:
                seller_code = order_form.cleaned_data.get('seller_code')
                if seller_code:
                    distributor = Distributor.objects.filter(enabled=True,
                                                             agent_number=seller_code).first()
                    if distributor:
                        seller = distributor.distributor_tenant
                    else:
                        retailer = Retailer.objects.filter(
                            enabled=True, agent_number=seller_code).first()
                        if retailer:
                            seller = retailer.retailer_tenant
            else:
                if request.tenant.tenant_type == RETAILER:
                    seller = request.tenant.retailer.distributor.distributor_tenant
                elif request.tenant.tenant_type == DISTRIBUTOR:
                    seller = request.tenant.distributor.gas_importer.gas_importer_tenant
                    distributor_number = distributor.distributor_number

            buyer = request.user.profile.tenant

            order_date = timezone.now()
            voucher_number = get_voucher_number()
            order_form.instance.buyer = buyer
            order_form.instance.seller = seller
            order_form.instance.order_status = ORDER_STATUS.Draft
            order_form.instance.order_date = order_date
            order_form.instance.seller_code = seller_code
            order_form.instance.voucher_number = voucher_number
            order_form.instance.mode_of_payment = mode_of_payment
            order = order_form.save()

            order_id = order.order_number
            order_number = order.voucher_number
            new_items = []
            for order_item in order_item_formset:
                listing = order_item.cleaned_data.get('listing')
                quantity = order_item.cleaned_data.get('quantity')
                if listing and quantity:
                    new_items.append(
                        OrderItem(order=order, listing=listing, quantity=quantity))

            try:
                with transaction.atomic():
                    OrderItem.objects.bulk_create(new_items)
                    order_oracle_items = []
                    for order_item in OrderItem.objects.filter(order_id=order_id):
                        data={}
                        data['INVENTORY_ITEM'] = order_item.listing.product
                        data['SHIP_FROM_ORG'] = str(
                            os.getenv('NOC_SHIPPING_ORG'))
                        data['ORDERED_QUANTITY'] = order_item.quantity
                        order_oracle_items.append(data)
                   
                    if request.tenant.tenant_type == DISTRIBUTOR:
                        r = process_order_express_input(order_oracle_items, distributor_number, order_number)
                        if r['OutputParameters']['X_RETURN_STATUS']!='S':
                            messages.error(request, r['OutputParameters']['X_RETURN_MESSAGE'])
                    return redirect(reverse('order_preview', kwargs={'pk': order.pk}))
            except IntegrityError:
                messages.error(
                    request, 'There was an error making your order.')
                return redirect(reverse('create_order'))
    else:
        order_form = OrderForm(user=request.user)
        agent_number = request.GET.get('agent_number')
        if agent_number:
            order_form = OrderForm(
                user=request.user, agent_number=agent_number)
        if seller_code:
            order_form = OrderForm(
                user=request.user, agent_number=seller_code, disabled=True)
        order_item_formset = OrderItemFormSet()

    context = {
        'order_form': order_form,
        'order_item_formset': order_item_formset,
        'display': 'Place Order',
        'seller_name': seller_name,
        'tenant': tenant
    }

    return render(request, 'orders/order.html', context)


@login_required
@has_permission_decorator('can_make_order')
def edit_order(request, pk):
    seller_name = ""
    tenant = ""
    seller = ""
    order = get_object_or_404(Order, pk=pk)
    OrderItemFormSet = inlineformset_factory(
        Order, OrderItem, fields=('listing', 'quantity'))
    if request.user.profile_type == PROFILE_TYPES.Employee:
        if request.tenant.tenant_type == RETAILER:
            retailer = Retailer.objects.get(
                retailer_tenant=request.user.profile.tenant)
            seller_code = retailer.agent_number
            tenant = retailer.distributor.distributor_tenant.id
            seller_name = retailer.distributor.trading_name
    if request.method == 'POST':
        order_form = OrderForm(request.POST, user=request.user, instance=order)
        if order_form.is_valid():
            mode_of_payment = order_form.cleaned_data['mode_of_payment']
            if request.user.profile_type in [PROFILE_TYPES.Consumer, PROFILE_TYPES.Beneficiary]:
                seller_code = order_form.cleaned_data['seller_code']
                if seller_code:
                    distributor = Distributor.objects.filter(enabled=True,
                                                             agent_number=seller_code).first()
                    if distributor:
                        seller = distributor.distributor_tenant
                    else:
                        retailer = Retailer.objects.filter(
                            enabled=True, agent_number=seller_code).first()
                        if retailer:
                            seller = retailer.retailer_tenant
            else:
                if request.tenant.tenant_type == RETAILER:
                    retailer = Retailer.objects.get(
                        retailer_tenant=request.user.profile.tenant)
                    seller = retailer.distributor.distributor_tenant

            buyer = request.user.profile.tenant

            order_date = timezone.now()
            voucher_number = get_voucher_number()
            order_form.instance.buyer = buyer
            order_form.instance.seller = seller
            order_form.instance.order_status = ORDER_STATUS.Draft
            order_form.instance.order_date = order_date
            order_form.instance.seller_code = seller_code
            order_form.instance.voucher_number = voucher_number
            order_form.instance.mode_of_payment = mode_of_payment
            order = order_form.save(commit=False)
            order_item_formset = OrderItemFormSet(request.POST, instance=order)
            if order_item_formset.is_valid():
                try:
                    with transaction.atomic():
                        order.save()
                        order_item_formset.save()
                        return redirect(reverse('order_preview', kwargs={'pk': order.pk}))
                except IntegrityError:
                    messages.error(
                        request, 'There was an error making your order.')
                    return redirect(reverse('edit_order'))
    else:
        order_form = OrderForm(user=request.user, instance=order)
        order_item_formset = OrderItemFormSet(instance=order)

    context = {
        'order_form': order_form,
        'order_item_formset': order_item_formset,
        'display': 'Edit Order',
        'seller_name': seller_name,
        'tenant': tenant
    }

    return render(request, 'orders/order.html', context)


class OrderPreviewView(HasPermissionsMixin, LoginRequiredMixin, OrderRecipientDetailsMixin, DetailView):
    model = Order
    template_name = "orders/order_preview.html"
    required_permission = 'can_make_order'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = OrderPreviewForm

        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        form = OrderPreviewForm(request.POST)
        order = self.object
        context = self.get_context_data(**kwargs)
        user = request.user

        listing = order.order_items.first().listing
        service = listing.service
        voucher_number = order.voucher_number
        if 'cancel_order' in request.POST:
            order.order_status = ORDER_STATUS.Cancelled
            order.save()
            messages.add_message(
                self.request, messages.SUCCESS, "Order cancelled!")

        else:
            if user.profile_type in [PROFILE_TYPES.Consumer, PROFILE_TYPES.Beneficiary]:
                self.buyer_phone = user.phone
                if order.mode_of_payment == MODE_OF_PAYMENT.Wallet:
                    if form.is_valid():
                        wallet = Wallet(
                            user=user, password=form.cleaned_data['password'])
                        result = wallet.makeWalletPayment(order=order)
                        if result['code'] != 0:
                            messages.add_message(
                                self.request, messages.ERROR, result['data'])
                            return self.render_to_response(context=context)

            elif user.profile_type == PROFILE_TYPES.Employee:
                self.buyer_phone = request.tenant_phone
                if form.is_valid():
                    wallet = Wallet(password=form.cleaned_data['password'])
                    result = wallet.makeAgencyPayment(
                        agent_code=request.tenant_agent_code,
                        agent_key=request.tenant_agent_key,
                        order=order,
                        customer_phone=request.tenant_phone
                    )
                    if result['code'] != 0:
                        messages.add_message(
                            self.request, messages.ERROR, result['data'])
                        return self.render_to_response(context=context)

            if service == 'purchase':
                message = successful_gas_yetu_purchase % (
                    voucher_number)
            else:
                message = successful_gas_yetu_refill % (
                    voucher_number)
            send_sms(phone_number=self.buyer_phone,
                     sms_body=message)
            messages.add_message(
                self.request, messages.SUCCESS, "Transaction completed successfully!")
            order.order_status = ORDER_STATUS.New
            order.save()

        return self.render_to_response(context=context)


@login_required
def load_listings(request):
    agent_number = request.GET.get('agent_number')
    service = str(request.GET.get('service'))
    tenant_id = str(request.GET.get('tenant_id'))
    seller_name = ""
    listings = Listing.objects.all()
    if agent_number:
        """
        This are orders by consumers/beneficiaries
        """

        distributor = Distributor.objects.filter(enabled=True,
                                                 agent_number=agent_number).first()
        if distributor:
            listings = listings.filter(
                tenant=distributor.distributor_tenant, service=service)
            seller_name = distributor.trading_name
        else:
            retailer = Retailer.objects.filter(
                enabled=True, agent_number=agent_number).first()
            if retailer:
                listings = listings.filter(
                    tenant=retailer.retailer_tenant, service=service)
                seller_name = retailer.trading_name
            else:
                gas_importer = GasImporter.objects.filter(
                    agent_number=agent_number).first()
                if gas_importer:
                    listings = listings.filter(
                        tenant=gas_importer.gas_importer_tenant, service=service)
                    seller_name = gas_importer.trading_name
                else:
                    listings = listings.none()
        if request.user.profile_type == PROFILE_TYPES.Consumer:
            listings = listings.filter(
                for_consumer=True, product__gas_yetu=False)
        elif request.user.profile_type == PROFILE_TYPES.Beneficiary:
            listings = listings.filter(
                for_consumer=True)
            if service == SERVICE_CHOICES.Purchase and request.user.profile.has_benefited:
                listings = listings.exclude(product__gas_yetu=True)

    elif tenant_id:
        tenant = Tenant.objects.get(id=tenant_id)
        listings = listings.filter(
            tenant=tenant, service=service)
    else:
        listings = listings.none()

    listings_res = []
    for listing in listings:
        json_obj = dict(pk=listing.pk, product=listing.product.title)
        listings_res.append(json_obj)

    response = {
        'listings': listings_res,
        'seller_name': seller_name
    }

    return HttpResponse(json.dumps(response), content_type='application/json')
