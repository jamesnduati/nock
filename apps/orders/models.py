from django.db import models
from django.db.models import Sum
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from apps.core.models import BaseModel
from apps.core.utils import get_uuid
from apps.finance.models import TenantProfit
from apps.inventory.choices import DIRECTION, MOVEMENT_TYPE
from apps.inventory.models import StockMovement
from apps.products.choices import BILLING_MODE, SERVICE_CHOICES
from apps.products.models import Listing, Product
from apps.tenants.choices import CONSUMER, DISTRIBUTOR, RETAILER
from apps.tenants.models import TENANT_TYPE, Tenant

from .choices import (DELIVERY_NOTE_STATUS, MODE_OF_PAYMENT, ORDER_STATUS,
                      ORDER_STATUS_COMPLETED, ORDER_STATUS_DISPATCHED)


class Order(BaseModel):
    """
    Order made
    """
    buyer = models.ForeignKey(
        Tenant, on_delete=models.CASCADE, related_name='order_buyer', null=True, blank=True)
    seller = models.ForeignKey(
        Tenant, on_delete=models.CASCADE, related_name='order_seller', null=True, blank=True)
    voucher_number = models.CharField(
        max_length=30, unique=True)
    order_number = models.UUIDField(
        primary_key=True, default=get_uuid, editable=False)
    order_status = models.PositiveSmallIntegerField(
        choices=ORDER_STATUS, default=ORDER_STATUS.Draft)
    order_date = models.DateTimeField(auto_now_add=True)
    seller_code = models.PositiveIntegerField(null=True,
                                              help_text='Agent number of Distributor/Retailer',
                                              verbose_name=_(
                                                  'Agent\'s Number'),
                                              blank=True)
    mode_of_payment = models.CharField(max_length=10,
                                       choices=MODE_OF_PAYMENT, default=MODE_OF_PAYMENT.Wallet,
                                       verbose_name=_('Mode of Payment'))
    returned_cylinder_check = models.BooleanField(default=False)
    issued_cylinder_check = models.BooleanField(default=False)

    class Meta:
        get_latest_by = 'order_date'

    def __str__(self):
        return f'{self.formatted_date(self.order_date)} ----- {self.buyer} [{self.get_order_status_display()}]'

    @property
    def returned_cylinders_serials(self):
        return ','.join(self.productmovement_set.filter(destination=self.seller).values_list(
            'product_item__serial_no', flat=True
        ))

    @property
    def issued_cylinders_serials(self):
        return ','.join(self.productmovement_set.filter(destination=self.buyer).values_list(
            'product_item__serial_no', flat=True
        ))

    @property
    def total_cost(self):
        "Returns the total order cost."
        return sum(float(order_item.cost) for order_item in self.order_items.all())

    @property
    def total_purchased_cylinders(self):
        return OrderItem.objects.filter(order=self).aggregate(Sum('quantity')).get('quantity__sum')

    def get_service(self):
        """
        Returns the service
        TODO:
         - move service to the order instead of order details
        """
        return self.order_items.all()[0].listing.service

    def get_buyer_name(self):
        """
        Returns the name of buyer.
        """

        if self.buyer.tenant_type == CONSUMER:
            if self.buyer.consumer_set.first():
                return self.buyer.consumer_set.first().user.get_full_name()
            else:
                return self.buyer.title
        else:
            return str(self.buyer)

    def complete_order_returned_cylinder_serials(self, *serial_numbers):
        """
        :param serial_numbers: Expects a list of serial numbers returned
        :return:
        """

        for serial in serial_numbers:

            listing = Listing.objects.get(pk=serial['listing_id'])

            product_item, __ = ProductItem.objects.get_or_create(
                serial_no=serial['serial_no']
            )

            movement, created = ProductMovement.objects.get_or_create(
                order=self,
                source=self.buyer,
                destination=self.seller,
                product_item=product_item
            )

            if created:
                StockMovement.objects.create(
                    tenant=self.seller,
                    product=product_item.product,
                    movement_type=MOVEMENT_TYPE.Orders,
                    direction=DIRECTION.In,
                    movement_date=timezone.now(),
                    no_of=1,
                    transaction_entity=self.buyer
                )
                if self.buyer.tenant_type == DISTRIBUTOR or self.buyer.tenant_type == RETAILER:
                    StockMovement.objects.create(
                        tenant=self.buyer,
                        product=product_item.product,
                        movement_type=MOVEMENT_TYPE.Orders,
                        direction=DIRECTION.Out,
                        movement_date=timezone.now(),
                        no_of=1,
                        transaction_entity=self.seller
                    )

        received_count = ProductMovement.objects.filter(
            order=self, source__isnull=False, destination=self.seller
        ).count()

        refill_count = OrderItem.objects.filter(
            order=self, listing__service=SERVICE_CHOICES.Refill).count()

        if received_count == refill_count:
            self.returned_cylinder_check = True
            self.save()

    def complete_issued_cylinder(self, *serial_numbers):
        """
        :param serial_numbers: Expects a list of serial numbers issued
        :return:
        """
        for serial in serial_numbers:
            product_item = ProductItem.objects.get(
                serial_no=serial['serial_no'])

            movement, created = ProductMovement.objects.get_or_create(
                order=self,
                source=self.seller,
                destination=self.buyer,
                product_item=product_item
            )
            if created:
                StockMovement.objects.create(
                    tenant=self.seller,
                    product=product_item.product,
                    movement_type=MOVEMENT_TYPE.Orders,
                    direction=DIRECTION.Out,
                    movement_date=timezone.now(),
                    no_of=1,
                    transaction_entity=self.buyer
                )
                if self.buyer.tenant_type == DISTRIBUTOR or self.buyer.tenant_type == RETAILER:
                    StockMovement.objects.create(
                        tenant=self.buyer,
                        product=product_item.product,
                        movement_type=MOVEMENT_TYPE.Orders,
                        direction=DIRECTION.In,
                        movement_date=timezone.now(),
                        no_of=1,
                        transaction_entity=self.seller
                    )

        self.order_status = ORDER_STATUS_COMPLETED
        self.issued_cylinder_check = True
        self.save()

    def compute_profit(self, *serial_numbers):
        """For each cylinder, get buying price and use that to
        compute overal total profit for the cylinders then update
        TenantProfit

        Note:
            Should be called only on fulfilling order.

        Args:
            serial_numbers: Cylinder serial numbers in order.

        Returns:
            Current tenant's profit.

        """
        buying_price = 0
        for serial in serial_numbers:
            product_item = ProductItem.objects.get(
                serial_no=serial['serial_no'])
            try:
                purchase_order = ProductMovement.objects.filter(
                    product_item=product_item, destination=self.seller).latest().order

                cost = purchase_order.order_items.all().filter(
                    listing__product=product_item.product).first().cost
                buying_price += cost
                profit = float(self.total_cost) - float(buying_price)
                tenant_profit = TenantProfit.objects.create_tenant_profit(
                   tenant=self.seller, profit=profit)
                return tenant_profit.profit

            except ProductMovement.DoesNotExist as e:
                print(e)
            else:# no trace of this product
                return 0

        

class OrderItem(BaseModel):
    """
    Order Item made
    """
    order = models.ForeignKey(
        Order, on_delete=models.CASCADE, related_name='order_items')
    listing = models.ForeignKey(
        Listing, on_delete=models.CASCADE, verbose_name=_('Product'))
    quantity = models.PositiveIntegerField(help_text='Number of items bought')

    @property
    def cost(self):
        return self.listing.list_price * self.quantity

    def __str__(self):
        return f'{self.listing} - {self.listing.product}({self.quantity}) == {self.cost}'


class DeliveryNote(BaseModel):
    """
    Delivery note created for an approved order
    """
    delivery_number = models.UUIDField(
        primary_key=True, default=get_uuid, editable=False)
    delivery_note_status = models.PositiveSmallIntegerField(
        choices=DELIVERY_NOTE_STATUS, default=DELIVERY_NOTE_STATUS.New)
    order = models.OneToOneField(
        Order, on_delete=models.CASCADE, related_name='delivery_note')
    comments = models.CharField(
        max_length=100, help_text='Comments about the order/delivery by supplier')
    delivered_by = models.ForeignKey(
        Tenant, on_delete=models.CASCADE, related_name='delivery_note_delivered_by')
    delivered_date = models.DateTimeField(auto_now=True)
    received_by = models.ForeignKey(
        Tenant, on_delete=models.CASCADE, related_name='delivery_note_received_by')
    received_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'#{self.delivery_number} - {self.order} - {self.delivery_note_status}'


class ProductItem(BaseModel):
    serial_no = models.CharField(max_length=128, unique=True)
    product = models.ForeignKey(Product,  on_delete=models.CASCADE)

    @property
    def current_location(self):
        return ProductMovement.objects.get(destination=self)

    def __str__(self):
        return '{} {}'.format(self.product, self.serial_no)


class ProductItemUser(BaseModel):
    product_item = models.OneToOneField(
        ProductItem, on_delete=models.CASCADE, related_name='product_item_user', unique=True)
    tenant = models.ForeignKey(
        Tenant, on_delete=models.CASCADE, related_name='product_item_user', null=True)

    def __str__(self):
        return '{} {}'.format(self.product_item, self.tenant)


class ProductMovement(BaseModel):
    order = models.ForeignKey('orders.Order', on_delete=models.CASCADE)
    source = models.ForeignKey(
        Tenant, related_name='sources', on_delete=models.CASCADE)
    destination = models.ForeignKey(
        Tenant, related_name='destinations', on_delete=models.CASCADE)
    product_item = models.ForeignKey(
        ProductItem, related_name='destinations', on_delete=models.CASCADE)

    class Meta:
        get_latest_by = 'created_on'

    def __str__(self):
        return '{} -> {}'.format(self.source, self.destination)
