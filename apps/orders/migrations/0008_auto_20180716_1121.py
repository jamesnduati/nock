# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-07-16 11:21
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('orders', '0007_auto_20180712_1351'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='deliverynote',
            name='buyer',
        ),
        migrations.RemoveField(
            model_name='deliverynote',
            name='checked_by',
        ),
        migrations.RemoveField(
            model_name='deliverynote',
            name='checked_date',
        ),
        migrations.RemoveField(
            model_name='deliverynote',
            name='packed_by',
        ),
        migrations.RemoveField(
            model_name='deliverynote',
            name='packed_date',
        ),
    ]
