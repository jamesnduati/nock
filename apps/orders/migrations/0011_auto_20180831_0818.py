# Generated by Django 2.0.8 on 2018-08-31 08:18

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tenants', '0001_initial'),
        ('orders', '0010_auto_20180802_0506'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductItemUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('product_item', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='product_item_user', to='orders.ProductItem')),
                ('tenant', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='product_item_user', to='tenants.Tenant')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AlterModelOptions(
            name='productmovement',
            options={'get_latest_by': 'created_on'},
        ),
    ]
