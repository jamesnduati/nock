from django.conf.urls import url

from ..views import default
from apps.orders.views import reports

urlpatterns = [
    url(r'^$', default.MyOrdersView.as_view(), name='orders'),
    url(r'^ajax/load-listings/$', default.load_listings, name='ajax_load_listings'),
    url(r'^new/$', default.create_order, name='create_order'),
    url(r'^(?P<pk>[0-9a-f-]+)/edit/$', default.edit_order, name='edit_order'),
    url(r'^(?P<pk>[0-9a-f-]+)/preview/$',
        default.OrderPreviewView.as_view(), name="order_preview"),
    url(r'^(?P<pk>[0-9a-f-]+)/fulfill/$',
        default.OrderFulfillView.as_view(), name="order_fulfill"),
    url(r'^fulfill/$', default.NewOrdersView.as_view(), name='new_orders_fulfill'),

    url(r'^reports/orders/$', reports.get_orders, name='report-orders'),
    url(r'^reports/orders/order_details/$', reports.get_order_details, name='report-orders-order-details'),
    # generate pdf and excel
    url(r'^reports/generate/pdf/$', reports.generate_pdf, name='orders_generate_pdf'),
    url(r'^reports/generate/excel/$', reports.generate_excel, name='orders_generate_excel'),
]
