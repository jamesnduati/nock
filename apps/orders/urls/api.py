from django.conf.urls import include, url

from ..views import api
from rest_framework_nested import routers

router = routers.SimpleRouter()
router.register(r'orders', api.OrdersViewSet, base_name='orders')
router.register(r'serials', api.SerialNumberViewSet, base_name='serials')


details_router = routers.NestedSimpleRouter(router, r'orders', lookup='order')
details_router.register(r'items', api.OrderItemViewSet, base_name='items')

details_router.register(r'delivery-note', api.DeliveryNoteViewSet, base_name='delivery_note')


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^', include(details_router.urls)),
]
