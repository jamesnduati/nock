from django.conf.urls import url

from apps.distribution.views.default import (DistributorDeleteView,
                                             DistributorsView,
                                             DistributorUpdateView,
                                             RetailerDeleteView,
                                             RetailerUpdateView,
                                             RetailersView,
                                             LocateDistributor,
                                             DistributorData,
                                             LocateRetailer,
                                             RetailerData,
                                             load_seller,
                                             DistributorRegisterWizard,
                                             RetailerRegisterWizard,
                                             FORMS)
from apps.distribution.views.reports import get_distributors, get_retailers, generate_pdf, generate_excel

urlpatterns = [
    url(r'^distributors/$', DistributorsView.as_view(), name='distributors'),
    url(r'^distributors/(?P<pk>[0-9]+)/$',
        DistributorUpdateView.as_view(),
        name='distributor-update'),
    url(r'^distributors/(?P<pk>[0-9]+)/deactivate$',
        DistributorDeleteView.as_view(),
        name='distributor-deactivate'),

    url(r'^distributors/new/$', DistributorRegisterWizard.as_view(FORMS),
        name='distributor-create'),

    url(r'^retailers/$', RetailersView.as_view(), name='retailers'),
    url(r'^retailers/(?P<pk>[0-9]+)/$', RetailerUpdateView.as_view(),
        name='retailer-update'),
    url(r'^retailers/(?P<pk>[0-9]+)/deactivate$',
        RetailerDeleteView.as_view(), name='retailer-deactivate'),
    url(r'^retailers/new/$', RetailerRegisterWizard.as_view(FORMS),
        name="retailer-create"),

    url(r'^locate/distributors/$', LocateDistributor.as_view(),
        name="locate-distributor"),
    url(r'^points.distributor/$', DistributorData.as_view(),
        name="distributor-points"),
    url(r'^locate/retailers/$', LocateRetailer.as_view(), name="locate-retailer"),
    url(r'^points.retailer/$', RetailerData.as_view(), name="retailer-points"),
    url(r'^ajax/load-seller/$', load_seller, name='ajax_load_seller'),

    url(r'^reports/retailers/$', get_retailers, name='reports-retailers'),    
    url(r'^reports/distributors/$', get_distributors, name='reports-distributors'),
    # generate pdf and excel
    url(r'^reports/generate/pdf/$', generate_pdf, name='distribution_generate_pdf'),
    url(r'^reports/distributors/generate/excel/$', generate_excel, name='distribution_generate_excel'),

]
