from django.conf.urls import include, url
from rest_framework_nested import routers
from apps.inventory.views.api import RetailerDistributorStockViewset
from apps.orders.views.api import RetailerDistributorOrdersViewset
from ..views import api

# Create a router and register our viewsets with it.
router = routers.SimpleRouter()
router.register(r'distributors', api.DistributorViewSet)
router.register(r'retailers', api.RetailerViewSet)

distributor_router = routers.NestedSimpleRouter(router, 'distributors', lookup='distributors')
distributor_router.register(r'stock', RetailerDistributorStockViewset, base_name='stock')
distributor_router.register(r'orders', RetailerDistributorOrdersViewset, base_name='orders')

retailer_router = routers.NestedSimpleRouter(router, 'retailers', lookup='retailers')
retailer_router.register(r'stock', RetailerDistributorStockViewset, base_name='stock')
retailer_router.register(r'orders', RetailerDistributorOrdersViewset, base_name='orders')

# The API URLs are now determined automatically by the router.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^', include(distributor_router.urls)),
    url(r'^', include(retailer_router.urls))
]
