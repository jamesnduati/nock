from django import forms
from django.conf import settings
from django.forms import ModelForm, SelectDateWidget
from django.forms.widgets import Select
from django.utils.translation import ugettext_lazy as _
from parsley.decorators import parsleyfy

from apps.accounts import messages
from apps.accounts.models import User
from apps.core.models import Region, SubRegion

from .models import Distributor, Retailer


class DistributorUpdateForm(ModelForm):
    class Meta:
        model = Distributor
        fields = (
            'trading_name',
            'agent_number',
            'kra_pin_number',
            'identity_number',
            'postal_address',
            'contact_email',
            'jp_agent_email',
            'contact_phone',
            'physical_address',
            'region',
            'start_date',
            'end_date'
        )
        widgets = {
            'end_date': forms.DateInput(format=('%d-%m-%Y'),
                                        attrs={'id': 'endDate',
                                               'placeholder': 'Select a date'}),
            'start_date': forms.DateInput(format=('%d-%m-%Y'),
                                          attrs={'id': 'startDate',
                                                 'placeholder': 'Select a date'})
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        if instance and instance.id:
            self.fields['agent_number'].widget.attrs['readonly'] = True
            self.fields['contact_email'].widget.attrs['readonly'] = True
            self.fields['jp_agent_email'].widget.attrs['readonly'] = True
            self.fields['contact_phone'].widget.attrs['readonly'] = True


class RetailerUpdateForm(ModelForm):

    class Meta:
        model = Retailer
        fields = (
            'trading_name',
            'agent_number',
            'kra_pin_number',
            'identity_number',
            'contact_phone',
            'contact_email',
            'jp_agent_email',
            'physical_address',
            'sub_region',
            'start_date',
            'end_date'
        )
        widgets = {
            'end_date': forms.DateInput(format=('%d-%m-%Y'),
                                        attrs={'id': 'endDate',
                                               'placeholder': 'Select a date'}),
            'start_date': forms.DateInput(format=('%d-%m-%Y'),
                                          attrs={'id': 'startDate',
                                                 'placeholder': 'Select a date'})
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        instance = getattr(self, 'instance', None)
        if instance and instance.id:
            self.fields['agent_number'].widget.attrs['readonly'] = True
            self.fields['contact_email'].widget.attrs['readonly'] = True
            self.fields['jp_agent_email'].widget.attrs['readonly'] = True
            self.fields['contact_phone'].widget.attrs['readonly'] = True


@parsleyfy
class MerchantForm(forms.Form):
    business_name = forms.CharField(label=_("Business Name"), max_length=100)
    phone = forms.RegexField(regex=r'^(07)[0-9]{8}$',
                             error_messages={'invalid': messages.phone_format},
                             required=True)
    kra_pin_number = forms.CharField(label=_("Kra Pin Number"), max_length=100)
    email = forms.EmailField(label=_("Contact Email Address"))
    identity_number = forms.CharField(
        label=_("Identity Number"), max_length=100)
    postal_address = forms.CharField(label=_(
        "Postal Address"), max_length=100, required=False)
    physical_address = forms.CharField(
        label=_("Postal Address"), max_length=100, required=False)
    region = forms.ModelChoiceField(
        queryset=Region.objects.all(), required=False)
    sub_region = forms.ModelChoiceField(
        queryset=SubRegion.objects.all(), required=False)
    password = forms.RegexField(regex=r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$',
                                error_messages={
                                    'invalid': messages.strong_password_format},
                                required=True, widget=forms.PasswordInput())

    def clean(self):
        cleaned_data = super().clean()

        email = cleaned_data.get('email', None)
        if email:
            email = email.casefold()
            if Distributor.objects.filter(enabled=True, contact_email=email).exists():
                raise forms.ValidationError(
                    _("User with this Email address already exists."))
            if Retailer.objects.filter(enabled=True, contact_email=email).exists():
                raise forms.ValidationError(
                    _("User with this Email address already exists."))

        phone = cleaned_data.get('phone', None)
        if phone:
            if Distributor.objects.filter(enabled=True,
                                          contact_phone=phone).exists():
                raise forms.ValidationError(
                    _("User with this Phone number already exists."))
            if Retailer.objects.filter(
                    enabled=True, contact_phone=phone).exists():
                raise forms.ValidationError(
                    _("User with this Phone number already exists."))
        return cleaned_data


@parsleyfy
class AgentForm(forms.Form):
    agent_email = forms.EmailField(label=_("Agent Email"), max_length=100)

    def clean(self):
        cleaned_data = super().clean()
        email = cleaned_data.get('agent_email', None)
        if email:
            email = email.casefold()
            if Distributor.objects.filter(enabled=True, jp_agent_email=email).exists():
                raise forms.ValidationError(
                    _("User with this Secondary Email address already exists."))
            if Retailer.objects.filter(enabled=True, jp_agent_email=email).exists():
                raise forms.ValidationError(
                    _("User with this Secondary Email address already exists."))
