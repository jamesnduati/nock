# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-06-17 23:15
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import djgeojson.fields


class Migration(migrations.Migration):

    dependencies = [
        ('distribution', '0010_auto_20180611_1455'),
    ]

    operations = [
        migrations.AddField(
            model_name='distributor',
            name='geom',
            field=djgeojson.fields.PointField(default={
                "type": "Point",
                "coordinates": [
                    -1.282668,
                    36.8140113
                ]
            }),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='distributor',
            name='gas_importer',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE,
                                    related_name='gas_importer_distributor', to='distribution.GasImporter'),
            preserve_default=False,
        ),
    ]
