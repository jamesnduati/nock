# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-06-20 15:17
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('distribution', '0011_auto_20180617_2315'),
    ]

    operations = [
        migrations.AlterField(
            model_name='retailer',
            name='physical_address',
            field=models.CharField(blank=True, max_length=50, null=True),
        ),
        migrations.AlterField(
            model_name='retailer',
            name='physical_location',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='retailer_location', to='core.Location'),
        ),
        migrations.AlterField(
            model_name='retailer',
            name='start_date',
            field=models.DateField(blank=True, null=True),
        ),
    ]
