# Generated by Django 2.0.8 on 2018-09-06 05:22

import apps.core.utils
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('distribution', '0014_auto_20180905_0749'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='distributor',
            options={'ordering': ('-modified_on',)},
        ),
        migrations.RemoveField(
            model_name='retailer',
            name='physical_location',
        ),
        migrations.AddField(
            model_name='retailer',
            name='jp_agent_key',
            field=models.CharField(blank=True, help_text='Jp Agent Key', max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='distributor',
            name='contact_email',
            field=apps.core.utils.NullableEmailField(default=None, help_text='Contact Email Address', max_length=254, unique=True, verbose_name='contact E-mail Address'),
        ),
        migrations.AlterField(
            model_name='distributor',
            name='contact_phone',
            field=models.CharField(blank=True, default=None, help_text='Contact Phone Number. Also represents JP Merchant Email', max_length=16, null=True, verbose_name='Phone Number'),
        ),
        migrations.AlterField(
            model_name='distributor',
            name='identity_number',
            field=models.CharField(blank=True, max_length=20, null=True),
        ),
    ]
