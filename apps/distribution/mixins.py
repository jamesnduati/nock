import logging

from django.conf import settings
from django.contrib import messages
from django.core.exceptions import ValidationError
from django.db import transaction
from django.utils.translation import ugettext_lazy as _
from formtools.wizard.forms import ManagementForm
from rolepermissions.roles import assign_role

from apps.accounts.choices import PROFILE_TYPES
from apps.accounts.models import User
from apps.accounts.utils import Wallet, send_sms
from apps.core.messages import first_time_password
from apps.employees.choices import EMPLOYMENT_STATUS, IDENTITY_TYPE
from apps.employees.models import Employee

from .models import Distributor, Retailer
from .utils import get_agent_number

logger = logging.getLogger('django')

# TODO: Combine these two classes for better code reuse


class CreateDistributorMerchantMixin(object):
    def post(self, *args, **kwargs):
        """
        This method handles POST requests.

        The wizard will render either the current step (if form validation
        wasn't successful), the next step (if the current step was stored
        successful) or the done view (if no more steps are available)
        """
        # Look for a wizard_goto_step element in the posted data which
        # contains a valid step name. If one was found, render the requested
        # form. (This makes stepping back a lot easier).
        wizard_goto_step = self.request.POST.get('wizard_goto_step', None)
        if wizard_goto_step and wizard_goto_step in self.get_form_list():
            return self.render_goto_step(wizard_goto_step)

        # Check if form was refreshed
        management_form = ManagementForm(self.request.POST, prefix=self.prefix)
        if not management_form.is_valid():
            raise ValidationError(
                _('ManagementForm data is missing or has been tampered.'),
                code='missing_management_form',
            )

        form_current_step = management_form.cleaned_data['current_step']
        if (form_current_step != self.steps.current and
                self.storage.current_step is not None):
            # form refreshed, change current step
            self.storage.current_step = form_current_step

        # get the form for the current step
        form = self.get_form(data=self.request.POST,
                             files=self.request.FILES)
        # and try to validate
        if form.is_valid():
            # if the form is valid, store the cleaned data and files.
            self.storage.set_step_data(
                self.steps.current, self.process_step(form))
            self.storage.set_step_files(
                self.steps.current, self.process_step_files(form))
            # check if the current step is the last step
            if self.steps.current == 'merchant_form':
                # return self.render_next_step(form)  # REMOVE

                # no more steps, render done view
                business_name = form.cleaned_data['business_name']
                phone = form.cleaned_data['phone']
                phone = '254' + phone[-9:]
                kra_pin_number = form.cleaned_data['kra_pin_number']
                email = form.cleaned_data['email']
                identity_number = form.cleaned_data['identity_number']
                password = form.cleaned_data['password']
                postal_address = form.cleaned_data.get('postal_address', None)
                physical_address = form.cleaned_data.get(
                    'physical_address', None)
                region = form.cleaned_data['region']
                merchant_code = get_agent_number()
                w = Wallet()
                result = w.create_merchant(
                    phone=phone,
                    merchant_code=merchant_code,
                    business_name=business_name,
                    id_number=identity_number,
                    email=email,
                    password=password,
                )
                if result['code'] != 0:
                    logger.error(f'Merchant creation failed==>{result}')
                    messages.add_message(
                        self.request, messages.ERROR, result['data'])
                    return self.render(form)

                with transaction.atomic():
                    try:
                        distributor = Distributor.objects.create_distributor_merchant(
                            business_name=business_name,
                            kra_pin_number=kra_pin_number,
                            postal_address=postal_address,
                            email=email,
                            phone=phone,
                            identity_number=identity_number,
                            physical_address=physical_address,
                            region=region,
                            agent_number=merchant_code
                        )
                        logger.info(f'Distributor created==>{distributor}')
                    except Exception as e:
                        if hasattr(e, 'message'):
                            message = e.message
                        else:
                            message = e
                        logger.error(
                            f'Distributor creation failed==>{message}')
                        messages.add_message(
                            self.request, messages.ERROR, message)
                        return self.render(form)
                return self.render_next_step(form)

            elif self.steps.current == 'agent_form':
                merchant_form = self.get_form(
                    step='merchant_form',
                    data=self.storage.get_step_data('merchant_form'),
                    files=self.storage.get_step_files('merchant_form'),
                )
                if data['merchant_form-email'] == form.cleaned_data['agent_email']:
                    messages.add_message(
                        self.request, messages.ERROR, 'Email entered should be different from the first form')
                data = merchant_form.data
                business_name = data['merchant_form-business_name']
                phone = data['merchant_form-phone']
                nat_id = data['merchant_form-identity_number']
                password = data['merchant_form-password']
                contact_email = data['merchant_form-email']
                wallet = Wallet()
                result = wallet.create_agent_jp(
                    business_name=business_name,
                    phone=phone,
                    pin=password,
                    email=form.cleaned_data['agent_email'],
                    last_name=business_name,
                    first_name=business_name,
                    nat_id=nat_id,
                    biz_reg_no=nat_id,
                )
                if result['code'] != 0:
                    logger.error(f'Agent creation failed==>{result}')
                    messages.add_message(
                        self.request, messages.ERROR, result['data'])
                    return self.render(form)

                result = wallet.get_agent_jp(
                    phone=phone, pin=password)
                if result['code'] != 0:
                    logger.error(f'Agent creation failed==>{result}')
                    messages.add_message(
                        self.request, messages.ERROR, result['data'])
                    return self.render(form)

                # Update Distributor with agent email
                distributor = Distributor.objects.get(
                    contact_email=contact_email)
                distributor.jp_agent_email = form.cleaned_data['agent_email']
                distributor.jp_agent_number = result['agent_code']
                distributor.jp_agent_key = result['agent_key']
                distributor.save()

                with transaction.atomic():
                    # Create User
                    distributor_user = User.objects.create_user(
                        username=distributor.contact_phone,
                        phone=distributor.contact_phone,
                        email=distributor.contact_email,
                        password=password,
                        # identity_number=distributor.identity_number,
                        profile_type=PROFILE_TYPES.Employee,
                        is_active=True,  # Check on better implementation of this
                        email_verified=True,
                        phone_verified=True
                    )

                    # Create Employee
                    Employee.objects.create(
                        tenant=distributor.distributor_tenant,
                        user=distributor_user,
                        identity_number=distributor.identity_number,
                        identity_type=IDENTITY_TYPE.NationalID,
                        employment_status=EMPLOYMENT_STATUS.Active
                    )

                sms_body = first_time_password.format(
                    username=distributor.contact_phone, password=password)
                assign_role(distributor_user, 'distributor_admin')
                send_sms(distributor.contact_phone, sms_body)
                self.storage.reset()
                msg = "Distributor created successfully!"
                messages.add_message(
                    self.request, messages.SUCCESS, msg)
                return self.done(form)

            else:
                # proceed to the next step
                return self.render_next_step(form)
        return self.render(form)


class CreateRetailerMerchantMixin(object):
    def post(self, *args, **kwargs):
        """
        This method handles POST requests.

        The wizard will render either the current step (if form validation
        wasn't successful), the next step (if the current step was stored
        successful) or the done view (if no more steps are available)
        """
        # Look for a wizard_goto_step element in the posted data which
        # contains a valid step name. If one was found, render the requested
        # form. (This makes stepping back a lot easier).
        wizard_goto_step = self.request.POST.get('wizard_goto_step', None)
        if wizard_goto_step and wizard_goto_step in self.get_form_list():
            return self.render_goto_step(wizard_goto_step)

        # Check if form was refreshed
        management_form = ManagementForm(self.request.POST, prefix=self.prefix)
        if not management_form.is_valid():
            raise ValidationError(
                _('ManagementForm data is missing or has been tampered.'),
                code='missing_management_form',
            )

        form_current_step = management_form.cleaned_data['current_step']
        if (form_current_step != self.steps.current and
                self.storage.current_step is not None):
            # form refreshed, change current step
            self.storage.current_step = form_current_step

        # get the form for the current step
        form = self.get_form(data=self.request.POST,
                             files=self.request.FILES)
        # and try to validate
        if form.is_valid():
            # if the form is valid, store the cleaned data and files.
            self.storage.set_step_data(
                self.steps.current, self.process_step(form))
            self.storage.set_step_files(
                self.steps.current, self.process_step_files(form))
            # check if the current step is the last step
            if self.steps.current == 'merchant_form':
                # return self.render_next_step(form)  # REMOVE

                # no more steps, render done view
                business_name = form.cleaned_data['business_name']
                phone = form.cleaned_data['phone']
                phone = '254' + phone[-9:]
                kra_pin_number = form.cleaned_data['kra_pin_number']
                email = form.cleaned_data['email']
                identity_number = form.cleaned_data['identity_number']
                password = form.cleaned_data['password']
                postal_address = form.cleaned_data.get('postal_address', None)
                physical_address = form.cleaned_data.get(
                    'physical_address', None)
                sub_region = form.cleaned_data['sub_region']
                merchant_code = get_agent_number()
                w = Wallet()
                result = w.create_merchant(
                    phone=phone,
                    merchant_code=merchant_code,
                    business_name=business_name,
                    id_number=identity_number,
                    email=email,
                    password=password,
                )
                if result['code'] != 0:
                    logger.error(
                        f'Retailer Merchant creation failed==>{result}')
                    messages.add_message(
                        self.request, messages.ERROR, result['data'])
                    return self.render(form)

                with transaction.atomic():
                    try:
                        retailer = Retailer.objects.create_retailer_merchant(
                            business_name=business_name,
                            distributor=self.request.tenant.distributor,
                            kra_pin_number=kra_pin_number,
                            email=email,
                            phone=phone,
                            identity_number=identity_number,
                            physical_address=physical_address,
                            sub_region=sub_region,
                            agent_number=merchant_code
                        )
                        logger.info(f'Retailer created==>{retailer}')
                    except Exception as e:
                        if hasattr(e, 'message'):
                            message = e.message
                        else:
                            message = e
                        logger.error(
                            f'Retailer creation failed==>{message}')
                        messages.add_message(
                            self.request, messages.ERROR, message)
                        return self.render(form)
                return self.render_next_step(form)

            elif self.steps.current == 'agent_form':
                merchant_form = self.get_form(
                    step='merchant_form',
                    data=self.storage.get_step_data('merchant_form'),
                    files=self.storage.get_step_files('merchant_form'),
                )
                if data['merchant_form-email'] == form.cleaned_data['agent_email']:
                    messages.add_message(
                        self.request, messages.ERROR, 'Email entered should be different from the first form')
                data = merchant_form.data
                business_name = data['merchant_form-business_name']
                phone = data['merchant_form-phone']
                nat_id = data['merchant_form-identity_number']
                password = data['merchant_form-password']
                contact_email = data['merchant_form-email']
                wallet = Wallet()
                result = wallet.create_agent_jp(
                    business_name=business_name,
                    phone=phone,
                    pin=password,
                    email=form.cleaned_data['agent_email'],
                    last_name=business_name,
                    first_name=business_name,
                    nat_id=nat_id,
                    biz_reg_no=nat_id,
                )
                if result['code'] != 0:
                    logger.error(f'Retailer Agent creation failed==>{result}')
                    messages.add_message(
                        self.request, messages.ERROR, result['data'])
                    return self.render(form)

                result = wallet.get_agent_jp(
                    phone=phone, pin=password)
                if result['code'] != 0:
                    logger.error(f'Retailer Agent fetching failed==>{result}')
                    messages.add_message(
                        self.request, messages.ERROR, result['data'])
                    return self.render(form)

                # Update Retailer with agent email
                retailer = Retailer.objects.get(
                    contact_email=contact_email)
                retailer.jp_agent_email = form.cleaned_data['agent_email']
                retailer.jp_agent_number = result['agent_code']
                retailer.jp_agent_key = result['agent_key']
                retailer.save()

                with transaction.atomic():
                    # Create User
                    retailer_user = User.objects.create_user(
                        username=retailer.contact_phone,
                        phone=retailer.contact_phone,
                        email=retailer.contact_email,
                        password=password,
                        # identity_number=distributor.identity_number,
                        profile_type=PROFILE_TYPES.Employee,
                        is_active=True,  # Check on better implementation of this
                        email_verified=True,
                        phone_verified=True
                    )

                    # Create Employee
                    Employee.objects.create(
                        tenant=retailer.retailer_tenant,
                        user=retailer_user,
                        identity_number=retailer.identity_number,
                        identity_type=IDENTITY_TYPE.NationalID,
                        employment_status=EMPLOYMENT_STATUS.Active
                    )

                sms_body = first_time_password.format(
                    username=retailer.contact_phone, password=password)
                assign_role(retailer_user, 'retailer_admin')
                send_sms(retailer.contact_phone, sms_body)
                self.storage.reset()
                msg = "Retailer created successfully!"
                messages.add_message(
                    self.request, messages.SUCCESS, msg)
                return self.done(form)

            else:
                # proceed to the next step
                return self.render_next_step(form)
        return self.render(form)
