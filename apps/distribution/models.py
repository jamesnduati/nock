import json

from django.contrib.gis.db import models as gis_models
from django.contrib.gis.geos import Point
from django.contrib.postgres.operations import CreateExtension
from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from djgeojson.fields import PointField

from apps.core.models import BaseModel, Location, Region, SubRegion
from apps.core.utils import NullableEmailField
from apps.tenants.models import Tenant
from apps.tenants.choices import TENANT_TYPE


class DistributorManager(models.Manager):
    def create_distributor_merchant(self, business_name, **kwargs):
        gas_importer = GasImporter.objects.first()
        distributor_tenant = Tenant.create_new(
            tenant_type=TENANT_TYPE.Distributor, title=business_name)
        distributor = Distributor.objects.create(
            gas_importer=gas_importer,
            distributor_tenant=distributor_tenant,
            trading_name=business_name,
            kra_pin_number=kwargs['kra_pin_number'],
            postal_address=kwargs.get('postal_address'),
            contact_email=kwargs['email'],
            contact_phone=kwargs['phone'],
            identity_number=kwargs['identity_number'],
            physical_address=kwargs.get('physical_address'),
            region=kwargs['region'],
            agent_number=kwargs['agent_number']
        )
        return distributor


class RetailerManager(models.Manager):
    def create_retailer_merchant(self, business_name, distributor, **kwargs):
        retailer_tenant = Tenant.create_new(
            tenant_type=TENANT_TYPE.Retailer, title=business_name)
        retailer = Retailer.objects.create(
            distributor=distributor,
            retailer_tenant=retailer_tenant,
            trading_name=business_name,
            kra_pin_number=kwargs['kra_pin_number'],
            contact_email=kwargs['email'],
            contact_phone=kwargs['phone'],
            identity_number=kwargs['identity_number'],
            physical_address=kwargs.get('physical_address'),
            sub_region=kwargs['sub_region'],
            agent_number=kwargs['agent_number']
        )
        return retailer


class Distributor(BaseModel):
    """
    Distributors registered in the system
    """
    gas_importer = models.ForeignKey(
        'GasImporter', on_delete=models.CASCADE, related_name='gas_importer_distributor')
    distributor_tenant = models.OneToOneField(
        Tenant, on_delete=models.CASCADE, related_name='distributor')
    distributor_number = models.CharField(
        max_length=100, null=True, blank=True, help_text='Noc Distributor Number')
    trading_name = models.CharField(
        max_length=50, help_text='Business Name')
    kra_pin_number = models.CharField(
        max_length=30, help_text='KRA pin number')
    postal_address = models.CharField(
        max_length=30, help_text='Business postal address', null=True, blank=True)
    contact_email = NullableEmailField(
        _('contact E-mail Address'), default=None, unique=True,
        help_text='Contact Email Address')
    contact_phone = models.CharField(
        _('Phone Number'), max_length=16, null=True,
        blank=True, default=None, help_text='Contact Phone Number. Also represents JP Merchant Email'
    )
    identity_number = models.CharField(max_length=20, null=True, blank=True)
    physical_address = models.CharField(
        max_length=30, help_text='Company physical address', null=True, blank=True)
    region = models.ForeignKey(Region, on_delete=models.CASCADE)
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    jp_agent_key = models.CharField(
        max_length=100, null=True, blank=True, help_text='Jp Agent Key')
    agent_number = models.IntegerField(
        unique=True, null=True, blank=True, help_text='Six digit agent number. Also represents JP Merchant Code')
    jp_agent_number = models.IntegerField(
        unique=True, null=True, blank=True, help_text='JP agent number')
    jp_agent_email = NullableEmailField(
        _('jambopay agent e-mail address'), blank=True, null=True, default=None, unique=True,
        help_text='JamboPay Agent Email Address')
    geom = PointField(null=True, blank=True)
    enabled = models.BooleanField(default=True)

    objects = DistributorManager()

    class Meta:
        ordering = ('-modified_on',)

    def __str__(self):
        return '{}'.format(self.distributor_tenant)

    def get_absolute_url(self):
        """
        Returns the URL of the detail page for that distributor.
        """
        return reverse(
            'distributor-update', kwargs={'pk': self.pk}
        )

    def get_position(self):
        return json.loads(self.geom)


class Retailer(BaseModel):
    """
    Retailers registered in the system

    """
    distributor = models.ForeignKey(
        Distributor, on_delete=models.CASCADE, related_name='distributor_retailer')
    retailer_tenant = models.OneToOneField(
        Tenant, on_delete=models.CASCADE, null=True, related_name='retailer')
    sub_region = models.ForeignKey(SubRegion, on_delete=models.CASCADE)
    trading_name = models.CharField(
        max_length=100, help_text='Name of the Retailer')
    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    agent_number = models.IntegerField(
        unique=True, null=True, blank=True, help_text='Six digit agent number')
    jp_agent_number = models.IntegerField(
        unique=True, null=True, blank=True, help_text='JP agent number')
    jp_agent_email = NullableEmailField(
        _('jambopay agent e-mail address'), blank=True, null=True, default=None, unique=True,
        help_text='JamboPay Agent Email Address')
    jp_agent_key = models.CharField(
        max_length=100, null=True, blank=True, help_text='Jp Agent Key')
    identity_number = models.CharField(max_length=20, blank=False)
    kra_pin_number = models.CharField(max_length=30)
    physical_address = models.CharField(max_length=50, null=True, blank=True)
    contact_phone = models.CharField(max_length=50)
    contact_email = models.CharField(max_length=50, unique=True)
    enabled = models.BooleanField(default=True)
    geom = PointField(null=True, blank=True)

    objects = RetailerManager()

    def __str__(self):
        return '{}'.format(self.retailer_tenant)

    def get_absolute_url(self):
        """
        Returns the URL of the detail page for that retailer.
        """
        return reverse(
            'retailer-update', kwargs={'pk': self.pk}
        )


class GasImporter(BaseModel):
    """
    GasImporters registered in the system
    """
    gas_importer_tenant = models.OneToOneField(
        Tenant, on_delete=models.CASCADE, related_name='gas_importer')
    trading_name = models.CharField(
        max_length=50, help_text='Business Name')
    contact_email = NullableEmailField(
        _('contact e-mail address'), blank=True, null=True, default=None, unique=True,
        help_text='Contact Email Address')
    contact_phone = models.CharField(
        _('phone number'), max_length=16, null=True,
        blank=True, default=None, help_text='Contact Phone Number'
    )
    identity_number = models.CharField(max_length=20, blank=False)
    agent_number = models.IntegerField(
        unique=True, null=True, blank=True, help_text='Six digit agent number')

    def __str__(self):
        return '{}'.format(self.gas_importer_tenant)
