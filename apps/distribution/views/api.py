import datetime

from rest_framework import serializers, status, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from apps.accounts.choices import EMPLOYEE
from apps.tenants.choices import TENANT_TYPE
from apps.tenants.models import Tenant

from ..models import Distributor, Retailer
from ..serializers import DistributorSerializer, RetailerSerializer


class DistributorViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Distributor.objects.filter(enabled=True)
    serializer_class = DistributorSerializer


class RetailerViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    queryset = Retailer.objects.filter(enabled=True)
    serializer_class = RetailerSerializer

    def get_queryset(self):
        queryset = super(RetailerViewSet, self).get_queryset()
        user = self.request.user
        if user.profile_type == EMPLOYEE:
            if user.profile.tenant.tenant_type == TENANT_TYPE.Distributor:
                queryset = queryset.filter(
                    distributor=self.request.user.profile.tenant.distributor)
        return queryset

    @action(methods=['patch'], url_path='deactivate', detail=True, url_name='retailer-deactivate')
    def deactivate(self, request, pk=None):
        """
        deactivating a retailer
        """
        try:
            retailer = Retailer.objects.get(id=pk)
        except Retailer.DoesNotExist:
            raise serializers.ValidationError({
                'error': "Retailer does not exist"
            })
        try:
            tenant = Tenant.objects.get(id=retailer.retailer_tenant_id)
        except Tenant.DoesNotExist:
            raise serializers.ValidationError({
                'error': "Tenant does not exist"
            })
        tenant = Tenant.objects.get(id=retailer.retailer_tenant_id)
        tenant.enabled = False
        tenant.save()
        retailer.end_date = datetime.date.today().strftime('%Y-%m-%d')
        retailer.save()
        serializer = RetailerSerializer
        headers = self.get_success_headers(serializer.data)
        data = {'message': f'Retailer {retailer.title} deactivated succesfully'}
        return Response(data, status=status.HTTP_200_OK, headers=headers)
