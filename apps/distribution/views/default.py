import datetime

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import transaction
from django.http import HttpResponseRedirect
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.generic import DeleteView, ListView, TemplateView, UpdateView
from djgeojson.views import GeoJSONLayerView
from formtools.wizard.views import SessionWizardView

from .. import forms
from ..mixins import (CreateDistributorMerchantMixin,
                      CreateRetailerMerchantMixin)
from ..models import Distributor, Retailer

FORMS = [("merchant_form", forms.MerchantForm),
         ("agent_form", forms.AgentForm)]

DISTRIBUTOR_TEMPLATES = {"merchant_form": "distribution/distributor/merchant_create.html",
                         "agent_form": "distribution/distributor/agent_create.html", }

RETAILER_TEMPLATES = {"merchant_form": "distribution/retailer/merchant_create.html",
                      "agent_form": "distribution/retailer/agent_create.html", }


class DistributorRegisterWizard(CreateDistributorMerchantMixin, SessionWizardView):
    """Wizard View for distributor's account registration.

    Note:
        Step 1: Create a merchant and the distributor.
        Step 2: Create the agent and the user for the distributor.

    """

    def get_template_names(self):
        return [DISTRIBUTOR_TEMPLATES[self.steps.current]]

    @transaction.atomic
    def done(self, form, **kwargs):
        return redirect('distributors')


class RetailerRegisterWizard(CreateRetailerMerchantMixin, SessionWizardView):
    """Wizard View for retailer's account registration.

    Note:
        Step 1: Create a merchant and the retailer.
        Step 2: Create the agent and the user for the retailer.

    """

    def get_template_names(self):
        return [RETAILER_TEMPLATES[self.steps.current]]

    @transaction.atomic
    def done(self, form, **kwargs):
        return redirect('retailers')


class DistributorsView(LoginRequiredMixin, ListView):
    model = Distributor
    template_name = 'distribution/distributors.html'
    context_object_name = 'distributors'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Distributors'
        return context

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(enabled=True)
        return queryset


class DistributorDeleteView(LoginRequiredMixin, DeleteView):
    model = Distributor
    template_name = 'distribution/distributor_delete.html'
    success_url = reverse_lazy('distributors')

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        tenant = self.object.distributor_tenant
        tenant.enabled = False
        tenant.save()
        self.object.end_date = datetime.date.today().strftime('%Y-%m-%d')
        self.object.enabled = False
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Confirm Delete'
        return context


class DistributorUpdateView(LoginRequiredMixin, UpdateView):
    model = Distributor
    template_name = 'distribution/distributor.html'
    form_class = forms.DistributorUpdateForm
    success_url = reverse_lazy('distributors')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Update Distributor'
        return context


class RetailersView(LoginRequiredMixin, ListView):
    model = Retailer
    template_name = 'distribution/retailers.html'
    context_object_name = 'retailers'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Retailers'
        return context

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(enabled=True)
        return queryset


class RetailerDeleteView(LoginRequiredMixin, DeleteView):
    model = Retailer
    template_name = 'distribution/retailer_delete.html'
    success_url = reverse_lazy('retailers')

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        tenant = self.object.retailer_tenant
        tenant.enabled = False
        tenant.save()
        self.object.end_date = datetime.date.today().strftime('%Y-%m-%d')
        self.object.enabled = False
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Confirm Delete'
        return context


class RetailerUpdateView(LoginRequiredMixin, UpdateView):
    model = Retailer
    template_name = 'distribution/retailer_update.html'
    form_class = forms.RetailerUpdateForm
    success_url = reverse_lazy('retailers')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Update Retailer'
        return context


class DistributorData(LoginRequiredMixin, GeoJSONLayerView):
    model = Distributor
    properties = ('trading_name', 'agent_number')
    precision = 4
    simplify = 0.5

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset


class RetailerData(LoginRequiredMixin, GeoJSONLayerView):
    model = Retailer
    properties = ('trading_name', 'agent_number')
    precision = 4
    simplify = 0.5

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset


class LocateDistributor(LoginRequiredMixin, TemplateView):
    model = Distributor
    template_name = 'location/location.html'

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Locate Nearest Distributor'
        context['seller_points'] = 'distributor-points'
        return context


class LocateRetailer(LoginRequiredMixin, TemplateView):
    model = Retailer
    template_name = 'location/location.html'

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Locate Nearest Retailer'
        context['seller_points'] = 'retailer-points'
        return context


@login_required
def load_seller(request):
    agent_number = request.GET.get('agent_number')
    if agent_number:
        """
        This are orders by consumers/beneficiaries
        """

        distributor = Distributor.objects.filter(
            agent_number=agent_number, enabled=True).first()

        if distributor:
            seller = distributor
        else:
            retailer = Retailer.objects.filter(
                agent_number=agent_number, enabled=True).first()
            if retailer:
                seller = retailer
            else:
                seller = None
    else:
        seller = None
    return render(request, 'location/location_seller_details.html', {'seller': seller})
