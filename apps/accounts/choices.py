from django.utils.translation import ugettext_lazy as _
from model_utils import Choices

NO_PROFILE = 1
EMPLOYEE = 2
CONSUMER = 3
BENEFICIARY = 4

PROFILE_TYPES = Choices(
    (NO_PROFILE, 'No_Profile', _('No_Profile')),
    (EMPLOYEE, 'Employee', _('Employee')),
    (CONSUMER, 'Consumer', _('Consumer')),
    (BENEFICIARY, 'Beneficiary', _('Beneficiary')),
)
