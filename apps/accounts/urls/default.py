import allauth.account.views as allauth_views
from django.conf.urls import url
from django.urls import path

from apps.accounts.views import default

app_name = 'accounts'

urlpatterns = [
    # url(r'^signup/$', default.AccountRegister.as_view(), name='register'),
    url(r'^signup/$', default.AccountRegisterWizard.as_view(default.FORMS),
        name='register'),
    url(r'^staff_register/$', default.StaffAccountRegister.as_view(),
        name='staff_register'),
    url(r'^confirm-email/(?P<key>[-:\w]+)/$', default.ConfirmEmail.as_view(),
        name='verify_email'),
    url(r'^invite/$', default.InviteUser.as_view(), name='invite_user'),
    path('accept_invite/<uidb64>/',
         default.AcceptInvite.as_view(), name='accept_invite'),
    url(r'^login/$', default.AccountLogin.as_view(), name='login'),
    url(r'^profile/$', default.AccountProfile.as_view(), name='profile'),
    url(r'^topup/$', default.AccountTopUp.as_view(), name='account_topup'),
    url(r'^password/change/$', default.PasswordChangeView.as_view(),
        name="account_change_password"),
    url(r'^password/reset/$', default.PasswordResetView.as_view(),
        name="account_reset_password"),
    url(r'^password/reset/done/$',
        default.PasswordResetDoneView.as_view(),
        name='account_reset_password_done'),
    url(r'^accountverification/$',
        default.ConfirmPhone.as_view(), name='verify_phone'),
    url(r'^resendtokenpage/$',
        default.ResendTokenView.as_view(), name='resend_token'),
    url(r'^password/reset/phone/$',
        default.PasswordResetFromPhoneView.as_view(),
        name='account_reset_password_from_phone'),
    url(r'^password/reset/key/done/$',
        allauth_views.PasswordResetFromKeyDoneView.as_view(),
        name='account_reset_password_from_key_done'),
    url(r'^statement/$', default.account_statement, name='statement'),
    url(r'^dashboard/$', default.dashboard, name='dashboard'),

    url(r'^manage_roles/$', default.ManageRoles.as_view(), name='manage_roles'),
    url(r'^ajax/load-roles/$', default.ManageRoles.as_view(action='get_roles_data'),
        name='ajax_load_roles_data'),
    url(r'^ajax/add-role/$', default.ManageRoles.as_view(action='add_user_role'),
        name='ajax_add_user_role'),
    url(r'^ajax/remove-role/$', default.ManageRoles.as_view(action='remove_user_role'),
        name='ajax_remove_user_role'),
    url(r'^ajax/remove-all-role/$', default.ManageRoles.as_view(
        action='remove_all_user_role'), name='ajax_remove_all_user_role'),
    url(r'^contactform/$', default.send_contact_email, name='contact_form'),

]
