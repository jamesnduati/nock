from rolepermissions.roles import AbstractUserRole


class SystemAdmin(AbstractUserRole):
    available_permissions = {
        'can_manage_regions': True,
        'can_manage_surveys': True,
        'can_manage_enumerators': True,
        'can_manage_products': True,
        'can_manage_users': True,
        'can_fulfill_distributor_orders': True,
        'can_view_orders_report': True,
        'can_manage_distributors': True,
        'can_view_orders_report': True,
        'can_manage_inventory': True,
        'can_dispatch_delivery': True,
        'edit_serials': True,
    }


class FinanceManager(AbstractUserRole):
    available_permissions = {
        'can_fulfill_distributor_orders': True,
        'can_view_orders_report': True,
    }


class DistributionManager(AbstractUserRole):
    available_permissions = {
        'can_manage_distributors': True,
        'can_view_orders_report': True,
    }


class InventoryManager(AbstractUserRole):
    available_permissions = {
        'can_manage_inventory': True,
        'can_manage_products': True,
        'can_dispatch_delivery': True,
        'edit_serials': True,
    }


class SurveyManager(AbstractUserRole):
    available_permissions = {
        'can_manage_surveys': True,
        'can_manage_enumerators': True,
    }


class Enumerator(AbstractUserRole):
    available_permissions = {
        'can_access_enumerator_view': True,
    }


class DistributorAdmin(AbstractUserRole):
    available_permissions = {
        'can_create_retailer': True,
        'can_settle_order': True,
        'can_make_order': True,
    }


class RetailerAdmin(AbstractUserRole):
    available_permissions = {
        'can_settle_order': True,
        'can_make_order': True,
    }


class Consumer(AbstractUserRole):
    available_permissions = {
        'can_make_order': True,
        'can_top_up_wallet': True,
        'can_view_distributors': True,
    }


class Beneficiary(AbstractUserRole):
    available_permissions = {
        'can_make_order': True,
        'can_self_register_account': True,
        'can_top_up_wallet': True,
        'can_view_distributors': True,
    }
