import hashlib
import inspect
import json
import os
import re
import time
import uuid
import xml.etree.ElementTree as ET
from datetime import datetime

import requests
from django.conf import settings
from django.core.mail import EmailMessage, send_mail
from django.template.loader import render_to_string
from django_otp.util import random_hex
from requests import Session
from rest_framework_jwt.compat import get_username, get_username_field
from rest_framework_jwt.settings import api_settings
from rolepermissions.permissions import available_perm_status
from rolepermissions.roles import get_user_roles
from zeep import Client
from zeep.transports import Transport

from apps.accounts.exceptions import SMSException
from apps.orders.models import Order
from apps.finance.choices import TRANSFER_TYPE

from . import roles


def jwt_payload_handler(user):
    username_field = get_username_field()
    username = get_username(user)
    permissions = available_perm_status(user)
    roles = [role.get_name() for role in get_user_roles(user)]
    payload = {
        'user_id': user.pk,
        'username': username,
        'first_name': user.first_name,
        'last_name': user.last_name,
        'exp': datetime.utcnow() + api_settings.JWT_EXPIRATION_DELTA,
        'permissions': permissions,
        'roles': roles
    }
    if hasattr(user, 'email'):
        payload['email'] = user.email
    if hasattr(user, 'phone'):
        payload['phone'] = user.phone
    if isinstance(user.pk, uuid.UUID):
        payload['user_id'] = str(user.pk)
    payload[username_field] = username

    # Include original issued at time for a brand new token,
    # to allow token refresh
    if api_settings.JWT_ALLOW_REFRESH:
        payload['orig_iat'] = timegm(
            datetime.utcnow().utctimetuple()
        )

    if api_settings.JWT_AUDIENCE is not None:
        payload['aud'] = api_settings.JWT_AUDIENCE

    if api_settings.JWT_ISSUER is not None:
        payload['iss'] = api_settings.JWT_ISSUER

    return payload


def default_otp_secret_key():
    return random_hex(20).decode()


def generate_jwt(user):
    jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
    jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
    payload = jwt_payload_handler(user)
    token = jwt_encode_handler(payload)
    return token


def send_sms(phone_number, sms_body):
    payload = {
        'Password': '1d95a4c6d681ede5b18c89b21ceb46bfea7b8e4d8f824107615a2ee297493710',
        'SenderName': 'GASYETU',
        'Mobile': phone_number,
        'Message': sms_body,
    }
    headers = {
        'DeveloperKey': '084049D0-AB72-4EE2-9EDE-0C25C1D1268C',
        'Password': '1d95a4c6d681ede5b18c89b21ceb46bfea7b8e4d8f824107615a2ee297493710',
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    r = requests.post(os.getenv('SMS_URL'), data=payload, headers=headers)
    status_code = r.status_code

    if status_code != 200:
        raise SMSException


def to_snake_case(name):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', name)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def send_email_update_notification(email):
    msg_body = render_to_string(
        'account/email/email_changed_notification.txt')
    send_mail(
        _("Change of email account at Gas Yetu"),
        msg_body,
        settings.DEFAULT_FROM_EMAIL,
        [email],
        fail_silently=False,
    )


def send_email_welcome(email, password, phone_number):
    context = {
        'mCode': password,
        'phone_number': phone_number
    }
    msg_body = render_to_string(
        'account/email/email_welcome.txt', context)
    send_mail(
        ("Welcome to Gas Yetu"),
        msg_body,
        settings.DEFAULT_FROM_EMAIL,
        [email],
        fail_silently=False,
    )


def get_roles_tuple():
    defined_roles = []
    for name, obj in inspect.getmembers(roles):
        if inspect.isclass(obj) and obj.__module__ == 'apps.accounts.roles':
            if name not in ["Consumer", "Beneficiary"]:
                defined_roles.append(name)
    roles_tuple = ()
    for role in defined_roles:
        human_readable = (" ").join(
            re.sub('([a-z])([A-Z])', r'\1 \2', role).split())
        r = (role, human_readable)
        roles_tuple += (r,)
    return roles_tuple


def send_email(subject, body, to):
    msg = EmailMessage(subject=subject, body=body,
                       from_email=settings.DEFAULT_FROM_EMAIL, to=[to, ])
    msg.content_subtype = "html"
    msg.send(fail_silently=True)


class Wallet(object):
    service_url = os.getenv('WSDL_URL')
    service_key = os.getenv('AGENT_KEY')
    service_username = os.getenv('AGENT_CODE')
    service_bank_code = os.getenv('WITHDRAWAL_MPESA_BANK_CODE')
    service_bank_swift_code = os.getenv('WITHDRAWAL_MPESA_BANK_SWIFTCODE')

    def __init__(self, *args, **kwargs):
        self.raw_password = kwargs.get('password', '')
        self.user = kwargs.get('user', None)
        session = Session()
        session.verify = False
        self.transport = Transport(session=session, timeout=60)
        if self.user:
            if '+' in self.user.phone:
                self.user.phone = self.user.phone.split('+')[1]

            if not self.user.email:
                self.user.email = self.user.phone + "@jambopay.com"

    def create_wallet(self):
        self.client = Client(self.service_url, transport=self.transport)
        if self.user:
            hash_string = self.service_username + self.user.phone + self.service_key
            hash_object = hashlib.sha1(hash_string.encode())
            self.service_password = hash_object.hexdigest()

        response = self.client.service.WalletRegisterNock(
            userName=self.service_username,
            jpwPhone=self.user.phone,
            jpwPIN=self.raw_password,
            email=self.user.email,
            lastName=self.user.last_name,
            firstName=self.user.first_name,
            nationalId=self.user.identity_number,
            servicePass=self.service_password
        )
        return {
            'code': response.Result.ResultCode,
            'data': response.Result.ResultText
        }

    def parse_statement(self, statement):
        statements = statement.split("|")
        entries = []
        for statement in statements[:10]:
            date = statement.split(',')[0]
            item = statement.split(',')[2]
            if 'NOCC' in item:
                voucher_number = item.split(" ")[1].strip()
                order = Order.objects.filter(
                    voucher_number=voucher_number).first()
                if order:
                    order_item = order.order_items.all()[0].listing
                    item = f'{order_item.product} {order_item.service}'
                else:
                    item = f'voucher: {voucher_number}'
            amount = statement.split(',')[3]
            entries.append({
                'date': date,
                'item': item,
                'amount': amount
            })
        return entries

    def get_wallet_statement(self):
        self.client = Client(self.service_url, transport=self.transport)
        if self.user:
            hash_string = self.service_username + self.user.phone + self.service_key
            hash_object = hashlib.sha1(hash_string.encode())
            self.service_password = hash_object.hexdigest()
        response = self.client.service.WalletStatementNock(
            userName=self.service_username,
            jpwPhone=self.user.phone,
            jpwPIN=self.raw_password,
            servicePass=self.service_password
        )
        if response.Result.ResultCode == 0:
            xml = ET.fromstring(response['Stat'])
            balance = xml[2].text
            name = xml[3].text
            email = xml[4].text
            statement = xml[5].text
            if statement:
                statement = self.parse_statement(statement)
            else:
                statement = []
            statement = {
                'balance': balance,
                'name': name,
                'email': email,
                'statement': statement
            }

            return {
                'code': response.Result.ResultCode,
                'data': statement
            }

        return {
            'code': response.Result.ResultCode,
            'data': response.Result.ResultText
        }

    def create_agent_jp(self, *args, **kwargs):
        self.client = Client(self.service_url, transport=self.transport)
        phone = '254' + kwargs['phone'][-9:]
        hash_string = self.service_username + \
            phone + self.service_key
        hash_object = hashlib.sha1(hash_string.encode())
        self.service_password = hash_object.hexdigest()
        response = self.client.service.CreateAgent(
            self.service_username,
            kwargs['business_name'],
            phone,
            kwargs['pin'],
            kwargs['email'],
            kwargs['last_name'],
            kwargs['first_name'],
            kwargs['nat_id'],
            kwargs['biz_reg_no'],
            290,
            self.service_password
        )
        print(response)

        return {
            'code': response.Result.ResultCode,
            'data': response.Result.ResultText
        }

    def get_agent_jp(self, *args, **kwargs):
        self.client = Client(self.service_url, transport=self.transport)
        timestamp = datetime.now()
        hash_string = self.service_username + \
            kwargs['phone'] + str(timestamp) + self.service_key
        hash_object = hashlib.sha1(hash_string.encode())
        self.service_password = hash_object.hexdigest()
        response = self.client.service.GA(
            self.service_username,
            str(timestamp),
            kwargs['phone'],
            kwargs['pin'],
            self.service_password
        )
        return {
            'code': response.Result.ResultCode,
            'data': response.Result.ResultText,
            'agent_code': response.AGC,
            'agent_key': response.AGK,
        }

    def get_agent_statement(self, *args, **kwargs):
        self.client = Client(self.service_url, transport=self.transport)
        hash_string = str(kwargs['agent_code']) + str(kwargs['agent_key'])
        hash_object = hashlib.sha1(hash_string.encode())
        self.service_password = hash_object.hexdigest()
        response = self.client.service.GetAgentStatement(
            str(kwargs['agent_code']),
            default_otp_secret_key(),
            kwargs['start_date'],
            kwargs['end_date'],
            self.service_password
        )
        print(response)
        if response.ResultCode == 0:
            if not response['AgentTransaction']:
                statement = {
                    'balance': 0.0,
                    'name': '',
                    'email': '',
                    'statement': []
                }
                return {
                    'code': response.ResultCode,
                    'data': statement
                }
            xml = ET.fromstring(response.AgentTransaction)

            balance = xml[2].text
            name = xml[3].text
            email = xml[4].text
            statement = xml[5].text
            if statement:
                statement = self.parse_statement(statement)
            else:
                statement = []
            statement = {
                'balance': balance,
                'name': name,
                'email': email,
                'statement': statement
            }

            return {
                'code': response.ResultCode,
                'data': statement
            }

        return {
            'code': response.ResultCode,
            'data': response.ResultText
        }

    def make_deposit(self, *args, **kwargs):
        self.client = Client(self.service_url, transport=self.transport)
        hash_string = self.service_username + \
            kwargs['ref_no'] + kwargs['amount'] + \
            kwargs['phone'] + self.service_key
        hash_object = hashlib.sha1(hash_string.encode())
        self.service_password = hash_object.hexdigest()

        response = self.client.service.WalletInitiateDepositNOCK(
            userName=self.service_username,
            agentReference=kwargs['ref_no'],
            amount=kwargs['amount'],
            phone=kwargs['phone'],
            servicePass=self.service_password
        )

        if response.Result.ResultCode != 0:
            return {
                'code': response.Result.ResultCode,
                'data': response.Result.ResultText
            }

        hash_string = self.service_username + \
            response.TransactionID + self.service_key
        hash_object = hashlib.sha1(hash_string.encode())
        self.service_password = hash_object.hexdigest()

        response = self.client.service.WalletCompleteDepositNOCK(
            userName=self.service_username,
            transactionID=response.TransactionID,
            currency='KES',
            servicePass=self.service_password
        )
        return {
            'code': response.Result.ResultCode,
            'data': response.Result.ResultText
        }

    def create_merchant(self, *args, **kwargs):
        self.client = Client(self.service_url, transport=self.transport)
        hash_string = self.service_username + \
            kwargs['phone'] + self.service_key
        hash_object = hashlib.sha1(hash_string.encode())
        self.service_password = hash_object.hexdigest()
        response = self.client.service.CreateMerchant(
            userName=self.service_username,
            jpwPhone=kwargs['phone'],
            password=kwargs['password'],
            servicepass=self.service_password,
            merchantCode=kwargs['merchant_code'],
            fixedCharge=0,
            businessName=kwargs['business_name'],
            businessRegNumber=kwargs['merchant_code'],
            email=kwargs['email'],
            lastName=kwargs['business_name'],
            firstName=kwargs['business_name'],
            nationalId=kwargs['id_number'],
        )

        return {
            'code': response.Result.ResultCode,
            'data': response.Result.ResultText
        }

    def pin_reset(self, *args, **kwargs):
        self.client = Client(self.service_url, transport=self.transport)
        timestamp = datetime.now()
        if self.user:
            hash_string = self.service_username + \
                str(timestamp) + self.user.phone + self.service_key
            hash_object = hashlib.sha1(hash_string.encode())
            self.service_password = hash_object.hexdigest()
        response = self.client.service.JPWalletPINResetNock(
            userName=self.service_username,
            jpwPhone=self.user.phone,
            jpwPin=self.raw_password,
            servicePass=self.service_password,
            timestamp=str(timestamp),
        )
        return {
            'code': response.Result.ResultCode,
            'data': response.Result.ResultText
        }

    def makeAgencyPayment(self, *args, **kwargs):
        self.client = Client(self.service_url, transport=self.transport)
        order = kwargs['order']
        agent_code = str(kwargs['agent_code'])
        agent_key = str(kwargs['agent_key'])
        timestamp = datetime.now()
        agentRef = str(round(time.mktime(timestamp.timetuple())))
        hash_string = agent_code + \
            agentRef + agent_key
        hash_object = hashlib.sha1(hash_string.encode())
        self.service_password = hash_object.hexdigest()
        response = self.client.service.PreparePaymentUniversalPaymentAgencyNOCK(
            username=agent_code,
            agentRef=agentRef,
            invoiceNumber=str(order.voucher_number),
            amount=order.total_cost,
            merchantCode=order.seller_code,
            customerPhone=kwargs['customer_phone'],
            description="NOCK",
            servicePass=self.service_password,
        )
        if response.Result.ResultCode != 0:
            if "low" in response.Result.ResultText.lower():
                response.Result.ResultText = "Low wallet balance,please topup and try again"

            return {
                'code': response.Result.ResultCode,
                'data': response.Result.ResultText
            }

        hash_string = agent_code + response.TransactionID + agent_key
        hash_object = hashlib.sha1(hash_string.encode())
        self.service_password = hash_object.hexdigest()

        response = self.client.service.CompletePaymentUniversalpaymentAgencyNock(
            userName=agent_code,
            transactionId=response.TransactionID,
            amount=order.total_cost,
            currency="KES",
            channelRef=str(order.voucher_number),
            servicePass=self.service_password
        )
        if "low" in response.Result.ResultText.lower():
            response.Result.ResultText = "Low wallet balance,please topup and try again"
        return {
            'code': response.Result.ResultCode,
            'data': response.Result.ResultText
        }

    def makeWalletPayment(self, *args, **kwargs):
        self.client = Client(self.service_url, transport=self.transport)
        order = kwargs['order']
        timestamp = datetime.now()
        agentRef = str(round(time.mktime(timestamp.timetuple())))
        if self.user:

            # self.user.phone = '0726981598'
            hash_string = self.service_username + self.user.phone + \
                agentRef + self.service_key
            hash_object = hashlib.sha1(hash_string.encode())
            self.service_password = hash_object.hexdigest()

        response = self.client.service.PreparePaymentNockWallet(
            username=self.service_username,
            agentRef=agentRef,
            invoiceNumber=str(order.voucher_number),
            amount=order.total_cost,
            currency="KES",  # constant
            itemName="NOCC",  # constant
            itemCode=543,  # constant
            merchantCode=order.seller_code,
            walletPhone=self.user.phone,
            servicePass=self.service_password,
        )
        if response.Result.ResultCode != 0:
            if "low" in response.Result.ResultText.lower():
                response.Result.ResultText = "Low wallet balance,please topup and try again"

            return {
                'code': response.Result.ResultCode,
                'data': response.Result.ResultText
            }
        hash_string = self.service_username + response.TransactionID + self.service_key
        hash_object = hashlib.sha1(hash_string.encode())
        self.service_password = hash_object.hexdigest()

        response = self.client.service.CompletePaymentNockWallet(
            userName=self.service_username,
            transactionId=response.TransactionID,
            pin=self.raw_password,
            amount=order.total_cost,
            currency="KES",
            channelRef=str(order.voucher_number),
            servicePass=self.service_password
        )
        if "low" in response.Result.ResultText.lower():
            response.Result.ResultText = "Low wallet balance,please topup and try again"

        return {
            'code': response.Result.ResultCode,
            'data': response.Result.ResultText
        }

    def agent_withdraw(self, *args, **kwargs):
        print('URL::',self.service_url)
        self.client = Client(self.service_url, transport=self.transport)

        hash_string = kwargs['agent_code'] + \
            kwargs['bank_code'] + kwargs['account'] + self.service_key
        hash_object = hashlib.sha1(hash_string.encode())
        self.service_password = hash_object.hexdigest()
        timestamp = datetime.now()
        reference = str(round(time.mktime(timestamp.timetuple())))
        response = self.client.service.NockFundsTransfer(
            agentCode=self.service_username,
            BankCode=kwargs['bank_code'],
            BankSwiftCode=kwargs['swift_code'],
            BranchCode=kwargs['branch_code'],
            Account=kwargs['account'],
            BeneficiaryAccountName=kwargs['beneficiary_account_name'],
            BeneficiaryName=kwargs['beneficiary_name'],
            Country=kwargs['country'],
            TranType=kwargs['tran_type'],
            Reference=reference,
            Currency='KES',  # Hardcoded for now
            Amount=kwargs['amount'],
            Narration=kwargs['narration'],
            dateFrom= timestamp.strftime("%Y%m%d%H%M%S"),
            dateTo= timestamp.strftime("%Y%m%d%H%M%S"),
            servicePass=self.service_password
        )
        return {
            'code': response.ResultCode,
            'data': response.ResultText,
            'ref':reference
        }
