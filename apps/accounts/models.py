import logging
import time
from binascii import unhexlify

import cloudinary
import django.contrib.auth.base_user as auth_base
import django.contrib.auth.models as auth
from allauth.account.signals import password_changed, password_reset
from cloudinary.models import CloudinaryField as BaseCloudinaryField
from django.conf import settings
from django.contrib.auth.models import AbstractUser, PermissionsMixin
from django.db import models
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from django.template.loader import render_to_string
from django.utils.translation import ugettext as _
from django_otp.models import Device
from django_otp.oath import TOTP
from django_otp.util import hex_validator, random_hex
from simple_history.models import HistoricalRecords

from apps.accounts import messages as account_message
from apps.accounts.choices import PROFILE_TYPES
from apps.accounts.manager import UserManager
from apps.accounts.utils import default_otp_secret_key, send_sms
from apps.consumers.choices import IPRS_STATUS 
from apps.core.models import Location, Region, SubRegion, Village
from apps.core.utils import NullableEmailField, NullableCharField

logger = logging.getLogger(__name__)


def abstract_user_field(name):
    for f in auth.AbstractUser._meta.fields:
        if f.name == name:
            return f


class CloudinaryField(BaseCloudinaryField):
    def upload_options(self, model_instance):
        return {
            'public_id': model_instance.username,
            'unique_filename': False,
            'overwrite': True,
            'resource_type': 'image',
            'tags': ['map', 'market-map'],
            'invalidate': True,
            'quality': 'auto:eco',
        }


class User(auth_base.AbstractBaseUser, PermissionsMixin):
    username = abstract_user_field('username')
    first_name = abstract_user_field('first_name')
    last_name = abstract_user_field('last_name')
    identity_number = models.CharField(
        _('identity'), max_length=15, unique=True
    )
    image = CloudinaryField(blank=True)
    profile_type = models.PositiveSmallIntegerField(
        choices=PROFILE_TYPES, default=PROFILE_TYPES.Consumer)
    email = NullableEmailField(
        _('e-mail address'), blank=True, null=True, default=None)
    phone = models.CharField(
        _('mobile number'), max_length=16, null=True,
        blank=True, default=None, unique=True
    )
    is_staff = abstract_user_field('is_staff')
    is_active = abstract_user_field('is_active')
    date_joined = abstract_user_field('date_joined')
    email_verified = models.BooleanField(default=False)
    phone_verified = models.BooleanField(default=False)
    update_profile = models.BooleanField(default=True)
    survey_requested = models.BooleanField(default=True)
    region = models.ForeignKey(
        Region, on_delete=models.CASCADE, null=True, blank=True)
    sub_region = models.ForeignKey(
        SubRegion, on_delete=models.CASCADE, blank=True, null=True)
    location = models.ForeignKey(
        Location, on_delete=models.CASCADE, null=True, blank=True)
    village = models.ForeignKey(
        Village, on_delete=models.CASCADE, null=True, blank=True)
    ussd = models.BooleanField(default=False)

    objects = UserManager()

    # Audit history
    created_date = models.DateTimeField(auto_now_add=True)
    last_updated = models.DateTimeField(auto_now=True)
    history = HistoricalRecords()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email']

    class Meta:
        ordering = ('username',)
        verbose_name = _('user')
        verbose_name_plural = _('users')

    objects = UserManager()

    def __repr__(self):
        repr_string = ('<User username={obj.username}'
                       ' email={obj.email}'
                       ' email_verified={obj.email_verified}'
                       ' phone={obj.phone}'
                       ' phone_verified={obj.phone_verified}>')
        return repr_string.format(obj=self)

    @property
    def profile(self):
        """:obj:`model`: Returns the user's entity.

        Note:
            There are 3 main entities: `employee`, `consumer`, `beneficiary`.
        """

        if self.profile_type == 1:
            return None
        elif self.profile_type == 2:
            return self.employee
        elif self.profile_type == 3:
            return self.consumer
        elif self.profile_type == 4:
            return self.consumer.beneficiary

    def get_image_url(self):
        "Returns the avatar url."
        if self.image.url:
            return self.image.url
        return ''

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_initials(self):
        """
        Returns the initials capitalized from full name.
        """
        if self.get_full_name():
            return ''.join([x[0].upper()
                            for x in self.get_full_name().split(' ')])
        return self.username[0]

    def get_short_name(self):
        "Returns the short name for the user."
        if self.first_name:
            return self.first_name
        return self.username

#@receiver(post_save, sender=User)
#def save_profile(sender, instance, **kwargs):
#    if instance.consumer.iprs_status == IPRS_STATUS.PendingChange:
#        instance.consumer.iprs_status = IPRS_STATUS.Default
#        instance.consumer.save()

@receiver(pre_delete, sender=User)
def photo_delete(sender, instance, **kwargs):
    if instance.image:
        cloudinary.uploader.destroy(instance.image.public_id)


@receiver(password_changed)
@receiver(password_reset)
def password_changed_reset(sender, request, user, **kwargs):
    if user.phone:
        send_sms(user.phone, account_message.password_change_or_reset)


class OTPVerificationDevice(Device):
    unverified_phone = models.CharField(max_length=16)
    secret_key = models.CharField(
        max_length=40,
        default=default_otp_secret_key,
        validators=[hex_validator],
        help_text="Hex-encoded secret key to generate totp tokens.",
        unique=True,
    )
    last_verified_counter = models.BigIntegerField(
        default=-1,
        help_text=("The counter value of the latest verified token."
                   "The next token must be at a higher counter value."
                   "It makes sure a token is used only once.")
    )
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    label = models.CharField(max_length=20,
                             choices=(('phone_verify', _("Verify Phone")),
                                      ('password_reset', _("Reset Password"))),
                             default='phone_verify')
    verified = models.BooleanField(default=False)

    step = settings.TOTP_TOKEN_VALIDITY
    digits = settings.TOTP_DIGITS

    class Meta(Device.Meta):
        verbose_name = "OTP Verification Devices"

    @property
    def bin_key(self):
        return unhexlify(self.secret_key.encode())

    def totp_obj(self):
        totp = TOTP(key=self.bin_key, step=self.step, digits=self.digits)
        totp.time = time.time()
        return totp

    def generate_challenge(self):
        totp = self.totp_obj()
        token = str(totp.token()).zfill(self.digits)

        message = _("Your One Time Password for Gas Yetu is {token_value}."
                    " It is valid for {time_validity} minutes.")
        message = message.format(
            token_value=token, time_validity=self.step // 60)

        send_sms(phone_number=self.unverified_phone, sms_body=message)
        return token

    def verify_token(self, token, tolerance=0):
        totp = self.totp_obj()
        if ((totp.t() > self.last_verified_counter) and
                (totp.verify(token, tolerance=tolerance))):
            self.last_verified_counter = totp.t()
            self.verified = True
            self.save()
        else:
            self.verified = False
        return self.verified


class Invitations(models.Model):
    email = models.EmailField(max_length=70)
    accepted = models.BooleanField(default=False)

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = _('invitations')
        verbose_name_plural = _('invitations')
