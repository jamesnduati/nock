from django.utils.translation import ugettext as _

phone_format = _(
    "Your phone number must start with 07 and contain 10 digits"
)

password_format = _(
    "Your password must be numeric and 4 digits long."
)

strong_password_format = _(
    "Your password must be alphanumeric and contain atleast one uppercase \
    letter, number and symbol.It must also be atleast 8 digits long."
)

# send an sms to the user's phone notifying the change in password
password_change_or_reset = _(
    "You are receiving this message because a user at Gas Yetu has"
    " changed or reset the password for your account linked to this phone"
    " number."
    " If it wasn't you, please contact us immediately at security@gasyetu.co.ke")

unverified_identifier = _(
    "You have not verified your phone or email. We request you to verify"
    " your registered phone or email in order to access your account.")

INVALID_CREDENTIALS_ERROR = _('Unable to login with provided credentials.')
INACTIVE_ACCOUNT_ERROR = _(
    'User account not verified. Verify first before login.')

account_inactive = _(
    "Your account has been set inactive. We request you to verify"
    " your registered phone or email in order to access your account.")

# send an sms to the user's old phone(if any) notifying the change in phone
phone_change = _(
    "You are receiving this message because a user at Gas Yetu changed"
    " the phone number for the account previously linked to this"
    " phone number."
    " If it wasn't you, please contact us immediately at security@gasyetu.co.ke")
