from allauth.account.models import EmailAddress
from django.conf import settings
from django.contrib.auth import authenticate, get_user_model
from django.contrib.auth.password_validation import validate_password
from django.db import IntegrityError, transaction
from django.utils.translation import ugettext as _
from djoser import serializers as djoser_serializers
from phonenumbers import NumberParseException
from phonenumbers import parse as parse_phone
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from apps.accounts import messages
from apps.accounts.choices import PROFILE_TYPES
from apps.accounts.exceptions import (AccountInactiveError,
                                      EmailNotVerifiedError,
                                      PhoneNotVerifiedError)
from apps.accounts.messages import phone_format
from apps.accounts.models import OTPVerificationDevice, User
from apps.accounts.utils import Wallet
from apps.accounts.validators import (check_username_case_insensitive,
                                      phone_validator)
from apps.core.serializers import SanitizeFieldSerializer
from apps.core.utils import get_random_digits

from .utils import send_sms

User = get_user_model()


class RegistrationSerializer(SanitizeFieldSerializer,
                             djoser_serializers.UserCreateSerializer):
    email = serializers.EmailField(
        allow_blank=True,
        allow_null=True,
        required=False
    )
    phone = serializers.RegexField(
        regex=r'^\+[0-9]{5,14}$',
        error_messages={'invalid': phone_format},
        validators=[UniqueValidator(
            queryset=User.objects.all(),
            message=_("User with this Phone number already exists.")
        )],
        allow_blank=True,
        allow_null=True,
        required=False
    )
    password = serializers.RegexField(regex=r'^[0-9]{4}$',
                                      error_messages={
                                          'invalid': messages.password_format},
                                      allow_blank=True,
                                      allow_null=True,
                                      required=False)
    from_enumerator = serializers.BooleanField(write_only=True)

    class Meta:
        model = User
        fields = (
            'username',
            'first_name',
            'last_name',
            'ussd',
            'survey_requested',
            'identity_number',
            'email',
            'phone',
            'password',
            'region',
            'sub_region',
            'location',
            'village',
            'from_enumerator'
        )
        read_only_fields = (
            'from_enumerator',
        )
        extra_kwargs = {
            'password': {'write_only': True},
            'email': {'required': False, 'unique': True,
                      'allow_null': True, 'allow_blank': True},
            'phone': {'required': True, 'unique': True},
        }

    def validate(self, data):
        from_enumerator = self.initial_data.get('from_enumerator')
        password = self.initial_data.get('password')
        if not password and not from_enumerator:
            raise serializers.ValidationError({
                'msg': _("Please supply a 4 digit password")
            })

        return data

    def create(self, validated_data):
        from_enumerator = validated_data.pop('from_enumerator', False)
        if from_enumerator:
            random_password = get_random_digits(4)
            validated_data['password'] = random_password
        validated_data['profile_type'] = PROFILE_TYPES.Consumer
        try:
            user = self.perform_create(validated_data)
        except IntegrityError:
            self.fail('cannot_create_user')

        """
        Create consumer wallet in JamboPay
        """
        raw_password = validated_data['password']
        wallet = Wallet(user=user, password=raw_password)
        result = wallet.create_wallet()
        if result['code'] != 0:
            if 'exists on JamboPay' in result['data']:
                result = wallet.pin_reset()
            else:
                raise serializers.ValidationError({
                    'msg': _(result['data'])
                })
        if from_enumerator:
            message = _(
                "Use %s as your first time login password to Gas Yetu. Thank You") % random_password
            send_sms(phone_number=user.phone, sms_body=message)

        return user

    def validate_email(self, email):
        if email:
            email = email.casefold()
            if User.objects.filter(email=email).exists():
                raise serializers.ValidationError({
                    'msg': _("User with this Email address already exists.")
                })
        else:
            email = None

        return email

    def validate_identity_number(self, identity_number):
        if not identity_number.isdigit():
            raise serializers.ValidationError("Invalid ID Number")
        if len(identity_number) < 7:
            raise serializers.ValidationError(
                "ID Number should be 7 digits or more")
        return identity_number

    def validate_phone(self, phone):
        if phone:
            if OTPVerificationDevice.objects.filter(
                    unverified_phone=phone).exists():
                raise serializers.ValidationError(
                    _("User with this Phone number already exists."))
            try:
                parse_phone(phone)
            except NumberParseException:
                raise serializers.ValidationError(
                    _("Please enter a valid country code."))
        else:
            phone = None

        return phone

    def validate_password(self, password):
        validate_password(password)

        errors = []
        email = self.initial_data.get('email')
        if email:
            email = email.split('@')
            if email[0].casefold() in password.casefold():
                errors.append(_("Passwords cannot contain your email."))

        phone = self.initial_data.get('phone')
        if phone:
            if phone_validator(phone):
                try:
                    phone = str(parse_phone(phone).national_number)
                    if phone in password:
                        errors.append(
                            _("Passwords cannot contain your phone."))
                except NumberParseException:
                    pass
        if errors:
            raise serializers.ValidationError(errors)

        return password


class AccountLoginSerializer(djoser_serializers.TokenCreateSerializer):

    default_error_messages = {
        'invalid_credentials': messages.INVALID_CREDENTIALS_ERROR,
        'inactive_account': messages.INACTIVE_ACCOUNT_ERROR,
    }

    def validate(self, attrs):
        attrs = super(AccountLoginSerializer, self).validate(attrs)

        if (attrs['username'] == self.user.phone and
                not self.user.phone_verified):
            raise PhoneNotVerifiedError

        if (attrs['username'] == self.user.email and
                not self.user.email_verified):
            raise EmailNotVerifiedError

        if (attrs['username'] == self.user.username and
            not self.user.email_verified and
                not self.user.phone_verified):
            raise AccountInactiveError

        return attrs


class PhoneVerificationSerializer(serializers.Serializer,
                                  SanitizeFieldSerializer):
    phone = serializers.RegexField(
        regex=r'^\+[0-9]{5,14}$',
        error_messages={'invalid': phone_format},
        required=True
    )
    token = serializers.CharField(label=_("Token"),
                                  max_length=settings.TOTP_DIGITS,
                                  required=True)

    def validate_token(self, token):
        phone = self.initial_data.get('phone')
        if '+' in phone:
            phone = phone.split('+')[1]
        try:
            token = int(token)
            device = OTPVerificationDevice.objects.get(
                unverified_phone=phone, verified=False)
        except ValueError:
            raise serializers.ValidationError(_("Token must be a number."))
        except OTPVerificationDevice.DoesNotExist:
            raise serializers.ValidationError(
                "Phone is already verified or not linked to any user account.")

        user = device.user
        if device.verify_token(token):
            if user.phone != phone:
                user.phone = phone
            user.phone_verified = True
            user.is_active = True
            user.save()
            device.delete()
            return token
        elif device.verify_token(token=token, tolerance=5):
            raise serializers.ValidationError(
                _("The token has expired."))
        else:
            raise serializers.ValidationError(
                _("Invalid Token. Enter a valid token."))


class UserSerializer(SanitizeFieldSerializer,
                     djoser_serializers.UserSerializer):
    email = serializers.EmailField(
        allow_blank=True,
        allow_null=True,
        required=False
    )
    phone = serializers.RegexField(
        regex=r'^\+[0-9]{5,14}$',
        error_messages={'invalid': phone_format},
        validators=[UniqueValidator(
            queryset=User.objects.all(),
            message=_("User with this Phone number already exists.")
        )],
        allow_blank=True,
        allow_null=True,
        required=False
    )

    class Meta:
        model = User
        fields = (
            'username',
            'email',
            'phone',
            'first_name',
            'last_name',
            'region',
            'sub_region',
            'location',
            'village',
            'email_verified',
            'phone_verified'
        )
        extra_kwargs = {
            'email': {'required': False, 'unique': True,
                      'allow_null': True, 'allow_blank': True},
            'phone': {'required': False, 'unique': True,
                      'allow_null': True, 'allow_blank': True},
            'email_verified': {'read_only': True},
            'phone_verified': {'read_only': True}
        }

    def validate(self, data):
        if self.instance and not self.instance.update_profile:
            raise serializers.ValidationError(
                _("The profile for this user can not be updated."))

        data = super(UserSerializer, self).validate(data)

        email = self.initial_data.get('email',
                                      getattr(self.instance, 'email', None))
        phone = self.initial_data.get('phone',
                                      getattr(self.instance, 'phone', None))

        if (not phone) and (not email):
            raise serializers.ValidationError(
                _("You cannot leave both phone and email empty."))
        return data

    def validate_email(self, email):
        instance = self.instance
        if (instance and email != instance.email and
                self.context['request'].user != instance):
            raise serializers.ValidationError(_("Cannot update email"))

        if instance and instance.email == email:
            return email

        if email:
            email = email.casefold()
            # make sure that the new email updated by a user is not a duplicate
            # of an unverified email already linked to a different user
            if (EmailAddress.objects.filter(email=email).exists() or
                    User.objects.filter(email=email).exists()):
                raise serializers.ValidationError(
                    _("User with this Email address already exists."))
        else:
            email = None

            if (instance and instance.email and
                    not instance.phone and self.initial_data.get('phone')):
                raise serializers.ValidationError(
                    _('It is not possible to change from email to phone for '
                      'your account identifier.'))
        return email

    def validate_phone(self, phone):
        instance = self.instance
        if (instance and phone != instance.phone and
                self.context['request'].user != instance):
            raise serializers.ValidationError(_("Cannot update phone"))

        if instance and instance.phone == phone:
            return phone

        if phone:
            # make sure that the new phone updated by a user is not a duplicate
            # of an unverified phone already linked to a different user
            if OTPVerificationDevice.objects.filter(
                    unverified_phone=phone).exists():
                raise serializers.ValidationError(
                    _("User with this Phone number already exists."))
            try:
                parse_phone(phone)
            except NumberParseException:
                raise serializers.ValidationError(
                    _("Please enter a valid country code."))
        else:
            phone = None

            if (instance and instance.phone and
                    not instance.email and self.initial_data.get('email')):
                raise serializers.ValidationError(
                    _('It is not possible to change from phone to email for '
                      'your account identifier.'))
        return phone


class AccountStatementSerializer(serializers.Serializer):
    password = serializers.CharField()


class MpesaSTKPushSerializer(serializers.Serializer):

    account = serializers.CharField(label=_("Account"),
                                    max_length=20,
                                    allow_blank=True,
                                    allow_null=True,
                                    required=False)
    amount = serializers.IntegerField(label=_("Amount"),
                                      required=True)


class MpesaDepositSerializer(serializers.Serializer):
    """MPESA Deposit Serializer. """
    amount = serializers.DecimalField(max_digits=10, decimal_places=2)
    account = serializers.CharField(max_length=200)
    phone = serializers.CharField(max_length=200)
    name = serializers.CharField(max_length=200)
    mpesareceipt = serializers.CharField(max_length=200)
