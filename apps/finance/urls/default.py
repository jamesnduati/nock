from django.conf.urls import url

from ..views import default

app_name = 'finance'

urlpatterns = [
    # url(r'^withdraw/$', default.withdrawal, name='withdraw'),
    url(r'^withdraw/$', default.WithdrawalCreateView.as_view(), name='withdraw'),
    url(r'^bank-autocomplete/$',
        default.BankAutocompleteView.as_view(), name='bank-autocomplete'),
    url(r'^branch-autocomplete/$',
        default.BranchAutocompleteView.as_view(), name='branch-autocomplete'),
]
