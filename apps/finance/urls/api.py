from django.conf.urls import include, url
from rest_framework_nested import routers

from ..views import api

router = routers.SimpleRouter()
router.register(r'bank', api.BankViewSet, base_name='bank')
router.register(r'branch', api.BankBranchViewSet, base_name='branch')

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^withdraw/profit/$', api.ProfitWithdrawViewSet.as_view(), name='withdrawprofit'),
    url(r'^withdraw/exit/$', api.ExitWithdrawViewSet.as_view(), name='withdrawexit'),
]
