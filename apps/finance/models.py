from django.db import models
from django.db.models import Sum

from apps.core.models import BaseModel, Country
from apps.tenants.models import Tenant, TenantBaseModel

from .choices import TRANSFER_TYPE, WITHDRAW_TYPE


class Bank(BaseModel):
    """
    Banks in a Country
    """
    title = models.CharField(max_length=100)
    bank_code = models.CharField(max_length=10)
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    swift_code = models.CharField(max_length=10)

    def __str__(self):
        return self.title


class Branch(BaseModel):
    """
    Branches Per Bank
    """
    title = models.CharField(max_length=50, help_text='Branch Name')
    bank = models.ForeignKey(Bank, on_delete=models.CASCADE)
    branch_code = models.CharField(max_length=10)

    def __str__(self):
        return self.title


class Withdrawal(TenantBaseModel):
    """
    Withdrawal amounts for each tenant
    i.e Retailer or Distributor
    """
    account_number = models.CharField(max_length=30)
    beneficiary_account_name = models.CharField(
        max_length=100, help_text='Beneficiary Account Name')
    beneficiary_name = models.CharField(
        max_length=100, help_text='Beneficiary Name', null=True, blank=True)
    transaction_date = models.DateTimeField(
        auto_now_add=True, help_text='Date the withdrawal was made')
    transaction_amount = models.DecimalField(
        max_digits=10, decimal_places=2, help_text='Amount withdrawn in the transaction')
    transaction_ref_number = models.CharField(
        max_length=50, help_text='Transaction Reference Number')
    transfer_type = models.PositiveSmallIntegerField(choices=TRANSFER_TYPE)
    withdraw_type = models.PositiveSmallIntegerField(choices=WITHDRAW_TYPE)
    country = models.ForeignKey(
        Country, on_delete=models.CASCADE)
    bank = models.ForeignKey(
        Bank, on_delete=models.CASCADE, blank=True, null=True)
    branch = models.ForeignKey(
        Branch, on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        get_latest_by = 'transaction_date'

    def __str__(self):
        return f'{self.transaction_date} ==>{self.transaction_amount}'


class TenantProfitManager(models.Manager):
    def create_tenant_profit(self, tenant, profit, **kwargs):
        tenant_profit = self.get_or_create(tenant=tenant)
        tenant_profit.profit = tenant_profit.profit + profit
        tenant_profit.save()
        return tenant_profit


class TenantProfit(BaseModel):
    """
    Profits per each tenant
    i.e Retailer and Distributor for fast calculation
    """
    tenant = models.OneToOneField(
        Tenant, on_delete=models.CASCADE, unique=True)
    profit = models.DecimalField(
        max_digits=10, decimal_places=2, help_text='Updated Profit on every order', default=0.0)

    objects = TenantProfitManager()

    def __str__(self):
        return f'{self.tenant} profit==>{self.profit}'

    @property
    def net_profit(self):
        total_withdrawal_amount = Withdrawal.objects.filter(tenant=self.tenant).aggregate(
            Sum('transaction_amount'))['transaction_amount__sum']
        return self.profit - total_withdrawal_amount
