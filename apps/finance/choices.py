from django.utils.translation import ugettext_lazy as _

from model_utils import Choices

TRANSFER_TYPE = Choices(
    (1, 'MPESA', _('M-PESA')),
    (2, 'INTERNAL', _('Transfer to CBA Account')),
    (3, 'EFT', _('(EFT) Electronic Funds Transfer')),
    (4, 'RTGS', _('(RTGS) Real Time Gross Settlement')),
)

WITHDRAW_TYPE = Choices(
    (1, 'PROFIT', _('ONLY PROFITS')),
    (2, 'PROFITEXIT', _('EVERYTHING (EXIT PROGRAM)')),
)
