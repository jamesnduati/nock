import os
from dal import autocomplete
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.db import IntegrityError, transaction
from django.shortcuts import render
from django.views.generic import CreateView

from apps.accounts.utils import Wallet

from ..forms import WithdrawalForm
from ..models import Bank, Branch, TenantProfit, Withdrawal
from apps.distribution.models import Distributor, Retailer
from apps.tenants.choices import TENANT_TYPE
from apps.finance.choices import TRANSFER_TYPE
from datetime import datetime


class WithdrawalCreateView(CreateView):
    model = Withdrawal
    template_name = 'finance/withdrawal.html'
    form_class = WithdrawalForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        try:
            tenant_profit = TenantProfit.objects.get(
                tenant=self.request.tenant)
            net_profit = tenant_profit.profit
        except TenantProfit.DoesNotExist:
            net_profit = 0

        latest_withdrawal = Withdrawal.objects.filter(
            tenant=self.request.tenant)
        if latest_withdrawal:
            latest_withdrawal = latest_withdrawal.latest()
        else:
            latest_withdrawal = None
        context['display'] = 'Merchant Withdrawal'
        context['net_profit'] = net_profit
        context['latest_withdrawal'] = latest_withdrawal
        return context

    def form_valid(self, form):
        tenant = self.request.tenant
        service_key = os.getenv('AGENT_KEY')
        service_username = os.getenv('AGENT_CODE')
        service_bank_code = os.getenv('WITHDRAWAL_MPESA_BANK_CODE')
        service_bank_swift_code = os.getenv('WITHDRAWAL_MPESA_BANK_SWIFTCODE')
        withdrawal = form.instance
        
        if withdrawal.get_transfer_type_display() == 'MPESA':
            bank_code = service_bank_code
            bank_swift_code = service_bank_swift_code
            branch_code = None
        else:
            bank_code = withdrawal.bank.bank_code
            bank_swift_code = withdrawal.bank.swift_code,
            branch_code = withdrawal.branch_code

        if withdrawal.transfer_type == 1:
            tran_type = 'Mpesa'
        elif withdrawal.transfer_type == 2:
            tran_type = 'INTERNAL'
        elif withdrawal.transfer_type == 3:
            tran_type = 'EFT'
        else:
            tran_type = 'RTGS'

        wallet = Wallet()
        timestamp = datetime.now()
        resp_data = wallet.get_agent_statement(
            agent_code=self.request.tenant_agent_code,
            start_date = timestamp,
            end_date = timestamp,
            agent_key=self.request.tenant_agent_key)
        
        if resp_data['code'] == 0:
            balance = resp_data['data']['balance']
            while balance < float(withdrawal.transaction_amount):
                messages.add_message(
                    self.request, messages.ERROR, 'You have insufficient funds in your account')
                return self.form_invalid(form)
        
        response = wallet.agent_withdraw(
            bank_code=bank_code,
            account=withdrawal.account_number,
            swift_code=bank_swift_code,
            branch_code=branch_code,
            country=withdrawal.country.title,
            tran_type=tran_type,
            amount=withdrawal.transaction_amount,
            beneficiary_account_name=withdrawal.beneficiary_account_name,
            beneficiary_name=withdrawal.beneficiary_name,
            narration=self.request.tenant.title,
            agent_code=service_username,
            service_key=service_key
        )
        if response['code'] == 0:
            try:
                withdrawal.tenant = tenant
                withdrawal.transaction_ref_number = response['ref']
                withdrawal.save()
                return super().form_valid(form)
            except IntegrityError as e:
                messages.add_message(
                    self.request, messages.ERROR, 'An error occured '+str(e.message))
                return self.form_invalid(form)
        messages.add_message(self.request, messages.ERROR, {response['data']})
        return self.form_invalid(form)


class BankAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Bank.objects.all()

        country = self.forwarded.get('country', None)

        if country:
            qs = qs.filter(country=country)
            if self.q:
                qs = qs.filter(title__istartswith=self.q)
        else:
            qs = None

        return qs


class BranchAutocompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = Branch.objects.all()

        bank = self.forwarded.get('bank', None)

        if bank:
            qs = qs.filter(bank=bank)
            if self.q:
                qs = qs.filter(title__istartswith=self.q)
        else:
            qs = None

        return qs
