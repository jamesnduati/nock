import logging
from datetime import datetime
from rest_framework import status
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from apps.core.viewsets import ReadOnlyViewSet
from apps.accounts.utils import Wallet, send_sms

from apps.core.models import Country
from ..models import (Bank, Branch, Withdrawal, TenantProfit)
from ..serializers import BankSerializer, WithdrawProfitSerializer, ExitWithdrawSerializer

from ..choices import TRANSFER_TYPE, WITHDRAW_TYPE


class BankViewSet(ReadOnlyViewSet):
    queryset = Bank.objects.all()
    serializer_class = BankSerializer


class BankBranchViewSet(ReadOnlyViewSet):
    queryset = Branch.objects.all()
    serializer_class = BankSerializer


class ProfitWithdrawViewSet(GenericAPIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = WithdrawProfitSerializer

    def post(self, request):
        serializer = self.serializer_class(
            data=request.data, context={'request': request})
        _status = status.HTTP_400_BAD_REQUEST
        response = {'msg': 'error'}
        if serializer.is_valid(raise_exception=True):
            wallet = Wallet()
            response = wallet.agent_withdraw(**serializer.validated_data)
            print(response)
            if 'code' in response:
                if response['code'] == 0:
                    _status = status.HTTP_200_OK
                    _transfer_type = serializer.validated_data['tran_type'].upper(
                    )
                    transfer_type = TRANSFER_TYPE.MPESA
                    if _transfer_type == 'MPESA':
                        transfer_type = TRANSFER_TYPE.MPESA
                    if _transfer_type == 'INTERNAL':
                        transfer_type = TRANSFER_TYPE.INTERNAL
                    if _transfer_type == 'EFT':
                        transfer_type = TRANSFER_TYPE.EFT
                    if _transfer_type == 'RTGS':
                        transfer_type = TRANSFER_TYPE.RTGS
                    withdrawal = Withdrawal(account_number=serializer.validated_data['account'],
                                            beneficiary_account_name=serializer.validated_data[
                                                'beneficiary_account_name'],
                                            beneficiary_name=serializer.validated_data['beneficiary_name'],
                                            transaction_date=datetime.now(),
                                            transaction_amount=serializer.validated_data['amount'],
                                            transaction_ref_number=response['ref'],
                                            transfer_type=transfer_type,
                                            withdraw_type=WITHDRAW_TYPE.PROFIT)
                    if not serializer.validated_data['tran_type'] == TRANSFER_TYPE.MPESA:
                        bank = Bank.objects.filter(
                            bank_code=serializer.validated_data['bank_code']).first()
                        if bank:
                            withdrawal.bank = bank
                            branch = Branch.objects.filter(
                                bank_code=serializer.validated_data['branch_code'], bank=bank).first()
                            if branch:
                                withdrawal.branch = branch
                    withdrawal.country = Country.objects.get(
                        code=serializer.validated_data['country'])
                    withdrawal.save()

        return Response(response, status=_status)


class ExitWithdrawViewSet(GenericAPIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)
    serializer_class = ExitWithdrawSerializer

    @classmethod
    def get_extra_actions(cls):
        return []

    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        _status = status.HTTP_400_BAD_REQUEST
        response = {'msg': 'error'}
        if serializer.is_valid(raise_exception=True):
            # TODO get profit amount
            # get wallet balance
            balance = self.get_balance(
                request.user, serializer.validated_data['password'])
            if balance:
                balance = int(float(balance.replace(',', '')))
            else:
                response['msg'] = 'Amount error'
                return Response(response, status=_status)
            serializer.validated_data['amount'] = balance
            response = {}
            try:
                wallet = Wallet()
                response = wallet.agent_withdraw(**serializer.validated_data)
            except Exception as e:
                logging.exception(e)
            print(response)
            if 'code' in response:
                if response['code'] == 0:
                    _status = status.HTTP_200_OK
                    _transfer_type = serializer.validated_data['tran_type'].upper(
                    )
                    transfer_type = TRANSFER_TYPE.MPESA
                    if _transfer_type == 'MPESA':
                        transfer_type = TRANSFER_TYPE.MPESA
                    if _transfer_type == 'INTERNAL':
                        transfer_type = TRANSFER_TYPE.INTERNAL
                    if _transfer_type == 'EFT':
                        transfer_type = TRANSFER_TYPE.EFT
                    if _transfer_type == 'RTGS':
                        transfer_type = TRANSFER_TYPE.RTGS
                    withdrawal = Withdrawal(account_number=serializer.validated_data['account'],
                                            beneficiary_account_name=serializer.validated_data[
                                                'beneficiary_account_name'],
                                            beneficiary_name=serializer.validated_data['beneficiary_name'],
                                            transaction_date=datetime.now(),
                                            transaction_amount=serializer.validated_data['amount'],
                                            transaction_ref_number=response['ref'],
                                            transfer_type=transfer_type,
                                            withdraw_type=WITHDRAW_TYPE.PROFITEXIT)
                    if not serializer.validated_data['tran_type'] == TRANSFER_TYPE.MPESA:
                        bank = Bank.objects.filter(
                            bank_code=serializer.validated_data['bank_code']).first()
                        if bank:
                            withdrawal.bank = bank
                            branch = Branch.objects.filter(
                                bank_code=serializer.validated_data['branch_code'], bank=bank).first()
                            if branch:
                                withdrawal.branch = branch
                    withdrawal.country = Country.objects.get(
                        code=serializer.validated_data['country'])
                    withdrawal.save()

        return Response(response, status=_status)

    def get_balance(self, user, password):
        balance = 0
        try:
            wallet = Wallet(user=user, password=password)
            if wallet:
                resp = wallet.get_wallet_statement()
                if 'code' in resp and resp['code'] == 0:
                    balance = resp['data']['balance']
        except Exception as e:
            logging.exception(e)
        return balance
