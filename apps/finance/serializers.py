from rest_framework import serializers
from rest_framework.fields import CurrentUserDefault

from apps.accounts import messages
from apps.core.models import Country
from .models import Bank, TenantProfit


class BankSerializer(serializers.ModelSerializer):
    """Bank Model Serializer. """

    class Meta:
        model = Bank
        fields = '__all__'

class WithdrawProfitSerializer(serializers.Serializer):
    """Profit withdrawal Serializer. """
    amount = serializers.IntegerField()
    tran_type = serializers.CharField(max_length=20)
    agent_code = serializers.CharField(max_length=50)
    swift_code = serializers.CharField(max_length=50, required=False, allow_null=True)
    bank_code = serializers.CharField(max_length=50, required=False, allow_null=True)
    branch_code = serializers.CharField(max_length=50, required=False, allow_null=True)
    account = serializers.CharField(max_length=50)
    narration = serializers.CharField(max_length=300, required=False, allow_null=True)
    beneficiary_name = serializers.CharField(max_length=200, required=False, allow_null=True)
    beneficiary_account_name = serializers.CharField(max_length=200, required=False, allow_null=True)
    country = serializers.CharField(max_length=200, required=False, allow_null=True)
    account = serializers.CharField(max_length=200, required=False, allow_null=True)
    '''
    def validate_country(self, country):
        if country:
            if not Country.objects.filter(code = country).exists():
                 raise serializers.ValidationError({
                     'msg': f'{country} does not exist.','code':400
                 })
        return country

    def validate_amount(self, amount):
        print(self.context)
        profit_amount = 0
        tenant_profit = TenantProfit.objects.filter(tenant = self.context['request'].user.profile.tenant).first()
        if tenant_profit:
            profit_amount = tenant_profit.profit
        if int(profit_amount) < amount:
             raise serializers.ValidationError({
                     'msg': f'{amount} is larger than your available profit: {profit_amount}.','code':400
                 })
        return amount 
    '''

class ExitWithdrawSerializer(serializers.Serializer):
    """Profit withdrawal Serializer. """    
    tran_type = serializers.CharField(max_length=20)
    agent_code = serializers.CharField(max_length=50)
    swift_code = serializers.CharField(max_length=50, required=False, allow_null=True)
    bank_code = serializers.CharField(max_length=50, required=False, allow_null=True)
    branch_code = serializers.CharField(max_length=50, required=False, allow_null=True)
    account = serializers.CharField(max_length=50)
    narration = serializers.CharField(max_length=300, required=False, allow_null=True)
    beneficiary_name = serializers.CharField(max_length=200, required=False, allow_null=True)
    beneficiary_account_name = serializers.CharField(max_length=200, required=False, allow_null=True)
    country = serializers.CharField(max_length=200, required=False, allow_null=True)
    password = serializers.RegexField(regex=r'^[0-9]{4}$',
                                      error_messages={
                                          'invalid': messages.password_format},
                                      required=True)
    '''
    def validate_country(self, country):
        if country:
            if not Country.objects.filter(code = country).exists():
                 raise serializers.ValidationError({
                     'msg': f'{country} does not exist.','code':400
                 })
        return country

    def validate_amount(self, amount):
        profit_amount = 0
        tenant_profit = TenantProfit.objects.filter(tenant = request.user.profile.tenant).first()
        if tenant_profit:
            profit_amount = tenant_profit.profit
        if int(profit_amount) < amount:
             raise serializers.ValidationError({
                     'msg': f'{amount} is larger than your available profit: {profit_amount}.','code':400
                 })
        return amount 
    '''
