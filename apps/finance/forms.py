from dal import autocomplete
from django import forms
from parsley.decorators import parsleyfy

from apps.core.models import Country

from .choices import TRANSFER_TYPE
from .models import Bank, Branch, Withdrawal


@parsleyfy
class WithdrawalForm(forms.ModelForm):
    class Meta:
        model = Withdrawal
        fields = ['transaction_amount', 'country',
                  'bank', 'branch', 'transfer_type', 'account_number', 'withdraw_type',
                  'beneficiary_name', 'beneficiary_account_name']
        widgets = {
            'country': autocomplete.ModelSelect2(url='country-autocomplete'),
            'bank': autocomplete.ModelSelect2(url='finance:bank-autocomplete', forward=['country']),
            'branch': autocomplete.ModelSelect2(url='finance:branch-autocomplete', forward=['bank']),
        }
