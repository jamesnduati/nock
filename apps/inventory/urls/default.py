from django.conf.urls import url

from ..views.default import CurrentStockListView, StockMovementListView, StockReconciliationCreateView

urlpatterns = [
    url(r'^current-stock/$', CurrentStockListView.as_view(), name='current_stock'),
    url(r'^stock-movement/$', StockMovementListView.as_view(), name='stock_movement'),
    url(r'^stock-reconciliation/$', StockReconciliationCreateView.as_view(), name='stock_reconciliation'),
]
