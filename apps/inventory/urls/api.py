from django.conf.urls import include, url
from rest_framework.routers import DefaultRouter
from ..views.api import StockViewSet, CurrentStockViewSet, ReconciliationViewSet

inventory_router = DefaultRouter()

inventory_router.register(r'stock-movement', StockViewSet)
inventory_router.register(
    r'stock', CurrentStockViewSet, base_name='current-stock')
inventory_router.register(r'reconciliation', ReconciliationViewSet)

urlpatterns = [
    url(r'^', include(inventory_router.urls))
]
