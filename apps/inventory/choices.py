from django.utils.translation import ugettext_lazy as _

from model_utils import Choices

MOVEMENT_TYPE = Choices(
    (1, 'Orders', _('Orders')),
    (2, 'Reconciliation', _('Reconciliation')),
)

DIRECTION = Choices(
    (1, 'In', _('Stock In')),
    (2, 'Out', _('Stock Leaving')),
)
