import datetime

from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import CreateView, ListView
from rolepermissions.mixins import HasRoleMixin

from ..forms import StockReconciliationForm
from ..models import CurrentStock, Reconciliation, StockMovement


class CurrentStockListView(HasRoleMixin, LoginRequiredMixin, ListView):
    model = CurrentStock
    template_name = 'inventory/current_stock.html'
    context_object_name = 'stocks'
    allowed_roles = ['system_admin', 'inventory_manager',
                     'distributor_admin', 'retailer_admin']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Current Stock'
        return context

    def get_queryset(self):
        tenant = self.request.user.employee.tenant
        return CurrentStock.objects.filter(tenant=tenant)


class StockMovementListView(HasRoleMixin, LoginRequiredMixin, ListView):
    model = StockMovement
    template_name = 'inventory/stock_movement.html'
    context_object_name = 'stocks'
    allowed_roles = ['system_admin', 'inventory_manager',
                     'distributor_admin', 'retailer_admin']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Stock Movement'
        return context

    def get_queryset(self):
        tenant = self.request.user.employee.tenant
        return StockMovement.objects.filter(tenant=tenant)


class StockReconciliationCreateView(HasRoleMixin, CreateView):
    model = Reconciliation
    template_name = 'inventory/reconciliation_create.html'
    form_class = StockReconciliationForm
    success_url = reverse_lazy('stock_movement')
    allowed_roles = ['system_admin', 'inventory_manager',
                     'distributor_admin', 'retailer_admin']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Reconcile stock by setting a value for current product (+ or -)'
        return context

    def form_valid(self, form):
        tenant = self.request.user.employee.tenant
        reconciliation = form.instance
        reconciliation.tenant = tenant
        reconciliation.reconciliation_date = datetime.date.today().strftime('%Y-%m-%d')
        reconciliation.save()
        return super(StockReconciliationCreateView, self).form_valid(form)
