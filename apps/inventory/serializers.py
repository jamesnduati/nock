from rest_framework import serializers

from apps.products.serializers import ProductSerializer
from .models import CurrentStock, Reconciliation, StockMovement


class ReconciliationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Reconciliation
        fields = ('id', 'tenant', 'product',
                  'reconciliation_date', 'item_count')


class StockMovementSerializer(serializers.ModelSerializer):

    class Meta:
        model = StockMovement
        fields = ('id', 'stock_level',)


class CurrentStockSerializer(serializers.ModelSerializer):

    product = ProductSerializer()

    class Meta:
        model = CurrentStock
        fields = ('id', 'product', 'no_of')
