from .models import Reconciliation
from django.forms import ModelForm


class StockReconciliationForm(ModelForm):
    class Meta:
        model = Reconciliation
        fields = (
            'product', 'item_count'
        )
