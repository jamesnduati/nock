class SuperUserCheckMixin:
    @property
    def is_superuser(self):
        return self.request.user.is_superuser

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['is_superuser'] = self.is_superuser
        return context
