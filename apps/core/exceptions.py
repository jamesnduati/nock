import json

from rest_framework.exceptions import APIException, ErrorDetail
from rest_framework.views import exception_handler
from rest_framework.utils.serializer_helpers import ReturnDict


def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    msg = None
    if hasattr(exc, 'detail') and isinstance(exc.detail, (list, dict)):
        if exc.detail:
            first_error = list(exc.detail.values())[0]
            if isinstance(first_error, ErrorDetail):
                msg = first_error
            else:
                msg = first_error[0]
    response = exception_handler(exc, context)

    # Change error key to errors.
    if response is not None:
        if isinstance(response.data, dict):
            if msg:
                response.data['msg'] = msg
            if 'detail' in response.data.keys():
                response.data['errors'] = response.data.pop('detail')
    return response


class OrderEditForbidden(APIException):
    status_code = 403
    default_detail = 'Order not in draft mode. Edit Forbidden'
    default_code = 'edit_forbidden'

class PermissionDenied(APIException):
    status_code = 403
    default_detail = 'Permission Denied'
    default_code = 'permission_denied'


class JsonValidationError(BaseException):
    def __init__(self, errors):
        self.errors = errors

    def __str__(self):
        return json.dumps(self.errors)
