from django_filters import rest_framework as filters
from rest_framework.permissions import IsAuthenticated

from .models import Country, Location, Region, SubRegion, Village
from .serializers import (CountrySerializer, LocationSerializer,
                          RegionSerializer, SubRegionSerializer,
                          VillageSerializer)
from .viewsets import ReadOnlyViewSet


class CountryViewSet(ReadOnlyViewSet):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer


class RegionViewSet(ReadOnlyViewSet):
    queryset = Region.objects.all()
    serializer_class = RegionSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('code', 'title')


class SubRegionViewSet(ReadOnlyViewSet):
    queryset = SubRegion.objects.all()
    serializer_class = SubRegionSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('code', 'title')

    def get_queryset(self):
        queryset = super(SubRegionViewSet, self).get_queryset()
        return queryset.filter(region=self.kwargs['region_pk'])


class LocationViewset(ReadOnlyViewSet):
    queryset = Location.objects.all()
    serializer_class = LocationSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('code', 'title')

    def get_queryset(self):
        queryset = super(LocationViewset, self).get_queryset()
        return queryset.filter(subregion=self.kwargs['subregion_pk'])


class VillageViewset(ReadOnlyViewSet):
    queryset = Village.objects.all()
    serializer_class = VillageSerializer
    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('code', 'title')

    def get_queryset(self):
        queryset = super(VillageViewset, self).get_queryset()
        return queryset.filter(location=self.kwargs['location_pk'])
