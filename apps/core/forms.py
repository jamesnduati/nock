from django import forms
from django.conf import settings
from django.forms import ModelForm

from apps.core.models import Country, Region, SubRegion, Location, Village


class CountryForm(ModelForm):
    class Meta:
        model = Country
        fields = (
            'code',
            'title',
            'tel_code'
        )
        


class RegionForm(ModelForm):

    class Meta:
        model = Region
        fields = (
            'country',
            'code',
            'title',
        )



class SubRegionForm(ModelForm):

    class Meta:
        model = SubRegion
        fields = (
            'region',
            'code',
            'title',
        )



class LocationForm(ModelForm):
    
    class Meta:
        model = Location
        fields = (
            'subregion',
            'code',
            'title',
        )



class VillageForm(ModelForm):

    class Meta:
        model = Village
        fields = (
            'location',
            'code',
            'title',
        )
