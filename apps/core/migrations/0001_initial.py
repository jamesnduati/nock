# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-05-19 16:35
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('code', models.CharField(max_length=3, unique=True)),
                ('title', models.CharField(max_length=100, unique=True)),
                ('tel_code', models.CharField(blank=True, max_length=3)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('code', models.CharField(max_length=3)),
                ('title', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='MobileMoneyOperator',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('title', models.CharField(max_length=50)),
                ('short_name', models.CharField(max_length=20)),
                ('is_active', models.BooleanField(default=True)),
                ('country', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='core_mobilemoneyoperator_country', to='core.Country')),
            ],
        ),
        migrations.CreateModel(
            name='MobileNetworkOperator',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('title', models.CharField(max_length=100)),
                ('short_name', models.CharField(max_length=20)),
                ('is_active', models.BooleanField(default=True)),
                ('country', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Country')),
            ],
        ),
        migrations.CreateModel(
            name='Region',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('code', models.CharField(max_length=3)),
                ('title', models.CharField(max_length=100)),
                ('country', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Country')),
            ],
        ),
        migrations.CreateModel(
            name='SubRegion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('code', models.CharField(max_length=3)),
                ('title', models.CharField(max_length=100)),
                ('region', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Region')),
            ],
        ),
        migrations.CreateModel(
            name='Village',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('code', models.CharField(max_length=3)),
                ('title', models.CharField(max_length=100)),
                ('location', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.Location')),
            ],
        ),
        migrations.AddField(
            model_name='mobilemoneyoperator',
            name='mobile_network_operator',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='core_mobilemoneyoperator_networkoperator', to='core.MobileNetworkOperator'),
        ),
        migrations.AddField(
            model_name='location',
            name='subregion',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.SubRegion'),
        ),
        migrations.AlterUniqueTogether(
            name='village',
            unique_together=set([('location', 'code')]),
        ),
        migrations.AlterUniqueTogether(
            name='subregion',
            unique_together=set([('region', 'code')]),
        ),
        migrations.AlterUniqueTogether(
            name='region',
            unique_together=set([('country', 'code')]),
        ),
        migrations.AlterUniqueTogether(
            name='mobilenetworkoperator',
            unique_together=set([('title', 'country')]),
        ),
        migrations.AlterUniqueTogether(
            name='mobilemoneyoperator',
            unique_together=set([('title', 'country')]),
        ),
        migrations.AlterUniqueTogether(
            name='location',
            unique_together=set([('subregion', 'code')]),
        ),
    ]
