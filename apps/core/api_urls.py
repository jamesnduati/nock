from django.conf.urls import include, url

from . import api
from rest_framework_nested import routers

router = routers.SimpleRouter()
router.register(r'region', api.RegionViewSet, base_name='region')

region_router = routers.NestedSimpleRouter(router, r'region', lookup='region')
region_router.register(
    r'subregion', api.SubRegionViewSet, base_name='subregion')

sub_region_router = routers.NestedSimpleRouter(
    region_router, r'subregion', lookup='subregion')
sub_region_router.register(
    r'location', api.LocationViewset, base_name='location')

location_router = routers.NestedSimpleRouter(
    sub_region_router, r'location', lookup='location')
location_router.register(r'village', api.VillageViewset, base_name='village')

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^', include(region_router.urls)),
    url(r'^', include(sub_region_router.urls)),
    url(r'^', include(location_router.urls)),
]
