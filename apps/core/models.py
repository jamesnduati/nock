from django.db import models
from django.urls import reverse


class BaseModel(models.Model):
    """
    An abstract base class model that provides self updating
    ``created`` and ``modified`` fields.
    """
    created_on = models.DateTimeField(auto_now_add=True, editable=False)
    modified_on = models.DateTimeField(auto_now=True, editable=False)

    class Meta:
        abstract = True

    def formatted_date(self, date):
        return '%s' % date.strftime('%m/%d/%Y')


class Country(BaseModel):
    """
    List of Countries
    """
    code = models.CharField(max_length=3, unique=True)
    title = models.CharField(max_length=100, unique=True)
    tel_code = models.CharField(max_length=3, blank=True)

    def __str__(self):
        return '{}'.format(self.title)

    def get_country(self, **kwargs):
        """
        Get Country detail
        Returns:
            Country
        """
        try:
            return Country.objects.get(**kwargs)
        except Country.DoesNotExist:
            return None

    def get_absolute_url(self):
        return reverse(
            'country-update', kwargs={'pk': self.pk}
        )


class Region(BaseModel):
    """
    List of regions in countries. This is equivalent to Counties in Kenya
    """
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    code = models.CharField(max_length=3)
    title = models.CharField(max_length=100)

    class Meta:
        unique_together = (('country', 'code'),)
        ordering = ('title',)

    def __str__(self):
        return '{}'.format(self.title)

    def get_absolute_url(self):
        return reverse(
            'region-update', kwargs={'pk': self.pk}
        )


class SubRegion(BaseModel):
    """
    List of sub regions. This is equivalent to Sub Counties in Kenya
    """
    region = models.ForeignKey(Region, on_delete=models.CASCADE)
    code = models.CharField(max_length=3)
    title = models.CharField(max_length=100)

    class Meta:
        unique_together = (('region', 'code'),)
        ordering = ('title',)

    def __str__(self):
        return '{}'.format(self.title)

    def get_absolute_url(self):
            return reverse(
            'sub-region-update', kwargs={'pk': self.pk}
        )


class Location(BaseModel):
    """
    List of locations
    """
    subregion = models.ForeignKey(SubRegion, on_delete=models.CASCADE)
    code = models.CharField(max_length=3)
    title = models.CharField(max_length=100)

    class Meta:
        unique_together = (('subregion', 'code'),)
        ordering = ('title',)

    def __str__(self):
        return '{}'.format(self.title)

    def get_absolute_url(self):
        return reverse(
        'location-update', kwargs={'pk': self.pk}
    )



class Village(BaseModel):
    """
    List of Villages
    """
    location = models.ForeignKey(Location, on_delete=models.CASCADE)
    code = models.CharField(max_length=3)
    title = models.CharField(max_length=100)

    class Meta:
        unique_together = (('location', 'code'),)
        ordering = ('title',)

    def __str__(self):
        return '{}'.format(self.title)

    def get_absolute_url(self):
        return reverse(
            'village-update', kwargs={'pk': self.pk}
        )


class MobileNetworkOperator(BaseModel):
    """
    List of mobile network operators registered in Scholar
    E.g. Safaricom, Airtel, Orange etc
    """
    title = models.CharField(max_length=100)
    short_name = models.CharField(max_length=20)
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    is_active = models.BooleanField(default=True)

    class Meta:
        unique_together = (('title', 'country'),)

    def __str__(self):
        return '{} - {}'.format(self.title, self.short_name)


class MobileMoneyOperator(BaseModel):
    """
    List of mobile money operators registered in Scholar
    E.g. Safaricom - Mpesa, Airtel Money, JamboPay etc
    """
    title = models.CharField(max_length=50)
    short_name = models.CharField(max_length=20)
    mobile_network_operator = models.ForeignKey(MobileNetworkOperator, on_delete=models.CASCADE, null=True,
                                                related_name='core_mobilemoneyoperator_networkoperator')
    is_active = models.BooleanField(default=True)
    country = models.ForeignKey(
        Country, on_delete=models.CASCADE, related_name='core_mobilemoneyoperator_country')

    class Meta:
        unique_together = (('title', 'country'),)

    def __str__(self):
        return '{} - {}'.format(self.title, self.short_name)
