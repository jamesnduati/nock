class ExtraActionMixin(object):
    """
    helper mixing to enable other https methods apart from get, post, put, delete
    """
    action = None

    def dispatch(self, request, *args, **kwargs):
        """
        Submitting form works only for "GET" and "POST".
        If `action` is defined use it dispatch request to the right method.
        """
        if not self.action:
            return super(ExtraActionMixin, self).dispatch(request, *args, **kwargs)

        if self.action in self.http_method_names:
            handler = getattr(self, self.action, self.http_method_not_allowed)
        else:
            handler = getattr(self, self.action, None)
        self.request = request
        self.args = args
        self.kwargs = kwargs
        return handler(request, *args, **kwargs)
