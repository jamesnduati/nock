from rest_framework import serializers

from .messages import SANITIZE_ERROR
from .models import Country, Region, SubRegion, Location, Village
from .validators import sanitize_string


class CountrySerializer(serializers.ModelSerializer):
    """Country Model Serializer. """

    class Meta:
        model = Country
        fields = '__all__'


class RegionSerializer(serializers.ModelSerializer):
    """Region Model Serializer. """

    class Meta:
        model = Region
        fields = '__all__'


class SubRegionSerializer(serializers.ModelSerializer):
    """SubRegion Model Serializer. """

    class Meta:
        model = SubRegion
        fields = '__all__'


class LocationSerializer(serializers.ModelSerializer):
    """Location Model Serializer. """

    class Meta:
        model = Location
        fields = '__all__'


class VillageSerializer(serializers.ModelSerializer):
    """Village Model Serializer. """

    class Meta:
        model = Village
        fields = '__all__'


class SanitizeFieldSerializer:
    def validate(self, data):
        data = super().validate(data)

        errors = {}

        for name, field in self.fields.items():
            if type(field) is serializers.CharField:
                value = data.get(name)
                if value:
                    value = value.strip()
                    data[name] = value
                valid = sanitize_string(value)
            elif type(field) is serializers.JSONField:
                value = data.get(name, {}) or {}
                valid = all(sanitize_string(value[k]) for k in value)

            if not valid:
                errors[name] = [SANITIZE_ERROR]

        if errors:
            raise serializers.ValidationError(errors)

        return data
