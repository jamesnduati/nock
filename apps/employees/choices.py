from django.utils.translation import ugettext_lazy as _

from model_utils import Choices

EMPLOYMENT_STATUS = Choices(
    (1, 'Active', _('Active')),
    (2, 'Resigned', _('Resigned')),
    (3, 'Fired', _('Fired')),
    (4, 'MIA', _('Missing in Action')),
)
IDENTITY_TYPE = Choices(
    (1, 'NationalID', _('National ID Card')),
    (2, 'Passport', _('Passport')),
)
