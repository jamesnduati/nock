from django.db import models

from apps.accounts.models import User
from apps.core.models import BaseModel, Country
from apps.tenants.models import Tenant

from .choices import EMPLOYMENT_STATUS, IDENTITY_TYPE


class Employee(BaseModel):
    """
    A employee of either NOCC, Distributor Company or Retailer Company
    """
    tenant = models.ForeignKey(Tenant, null=True, blank=True,
                               on_delete=models.CASCADE, help_text='Related Tenant')
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    employee_number = models.CharField(max_length=50, null=True, blank=True)
    identity_number = models.CharField(max_length=50, null=True, blank=True)
    identity_type = models.PositiveSmallIntegerField(
        choices=IDENTITY_TYPE, null=True, blank=True)
    country_of_issue = models.ForeignKey(Country, on_delete=models.CASCADE,
                                         related_name="%(app_label)s_%(class)s_country_of_issue", null=True, blank=True)
    employment_status = models.PositiveSmallIntegerField(
        choices=EMPLOYMENT_STATUS, default=EMPLOYMENT_STATUS.Active)
    join_date = models.DateField(null=True, blank=True)
    departure_date = models.DateField(null=True, blank=True)

    class Meta:
        unique_together = (('tenant', 'employee_number'),)

    def __str__(self):
        return f'{self.user.get_full_name()} - {self.tenant}'
