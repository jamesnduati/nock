from rest_framework.decorators import action
from rest_framework.response import Response
from apps.accounts.choices import EMPLOYEE
from apps.consumers.models import Beneficiary, Consumer
from apps.consumers.serializers import ConsumerSerializer, BeneficiarySerializer
from apps.core.viewsets import ReadOnlyViewSet
from apps.surveys.models import Enumerator
from apps.tenants.choices import ENUMERATOR


class BeneficiaryViewset(ReadOnlyViewSet):
    queryset = Beneficiary.objects.filter()
    serializer_class = BeneficiarySerializer

    def get_queryset(self):
        queryset = super(BeneficiaryViewset, self).get_queryset()
        if self.request.user.profile_type == EMPLOYEE:
            employee = self.request.user.employee
            if employee.tenant.tenant_type == ENUMERATOR:
                enumerator = Enumerator.objects.filter(
                    enumerator_tenant=employee.tenant).first()
                if enumerator:
                    queryset = queryset.filter(
                        survey_requested=True, user__sub_region=enumerator.sub_region)
                return queryset
        return queryset

    @action(methods=['get'], detail=False, url_path='me', url_name='my-surveys')
    def me(self, request, *args, **kwargs):
        """
        View of beneficiaries surveyed by the current enumerator
        """
        queryset = self.filter_queryset(
            super(BeneficiaryViewset, self).get_queryset())
        queryset = queryset.filter(
            survey__enumerator__enumerator_tenant=request.user.profile.tenant)

        page = self.paginate_queryset(queryset)

        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)

        return Response(serializer.data)


class ConsumerViewset(ReadOnlyViewSet):
    queryset = Consumer.objects.filter()
    serializer_class = ConsumerSerializer

    def get_queryset(self):
        queryset = super(ConsumerViewset, self).get_queryset()
        queryset = queryset.filter(survey_requested=True)
        if self.request.user.profile_type == EMPLOYEE:
            employee = self.request.user.employee
            if employee.tenant.tenant_type == ENUMERATOR:
                enumerator = Enumerator.objects.filter(
                    enumerator_tenant=employee.tenant).first()
                if enumerator:
                    queryset = queryset.filter(
                        survey_requested=True, user__sub_region=enumerator.sub_region)
                return queryset

        pending = self.request.query_params.get('pending')

        if pending:
            queryset = queryset.filter(survey__isnull=True)

        return queryset

    @action(methods=['get'], detail=False, url_path='me', url_name='my-surveys')
    def me(self, request, *args, **kwargs):
        """
        View of consumers surveyed by the current enumerator
        """
        queryset = self.filter_queryset(
            super(ConsumerViewset, self).get_queryset())
        queryset = queryset.filter(
            survey__enumerator__enumerator_tenant=request.user.profile.tenant)

        page = self.paginate_queryset(queryset)

        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)

        return Response(serializer.data)
