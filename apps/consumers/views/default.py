from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render, reverse
from django.urls import reverse_lazy
from django.views.generic import (CreateView, DeleteView, DetailView, ListView,
                                  RedirectView, UpdateView, View)

from apps.tenants.models import Tenant
from apps.consumers.models import Consumer, Beneficiary


@login_required
def get_consumers(request):
    consumers = Consumer.objects.all()
    beneficiaries = Beneficiary.objects.all()
    return render(request, 'consumer_list.html', {'consumers': consumers, 'beneficiaries': beneficiaries})
