from django.utils.translation import ugettext_lazy as _
from enum import IntEnum

from model_utils import Choices

ENROLLMENT_STATUS = Choices(
    (1, 'Requested', _('Requested for Enrollment')),
    (2, 'Surveyed', _('Survey Done')),
    (3, 'Approved', _('Approved as beneficiary')),
    (4, 'Rejected', _('Rejected from being a beneficiary')),
)
IDENTITY_TYPE = Choices(
    (1, 'NationalID.', _('National ID Card')),
    (2, 'Passport.', _('Passport')),
)
IPRS_STATUS = Choices(
    (0, 'Default', _('Default')),
    (1, 'Verified', _('Details verified')),
    (2, 'PendingChange', _('Pending Change')),
)
