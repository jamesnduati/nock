from django.db import models

from apps.consumers.choices import IDENTITY_TYPE, IPRS_STATUS
from apps.core.models import Country, BaseModel
from apps.tenants.models import Tenant
from apps.tenants.choices import TENANT_TYPE
from apps.accounts.models import User


class ConsumerManager(models.Manager):
    def create_consumer(self, user, title, **kwargs):
        tenant = Tenant.create_new(TENANT_TYPE.Consumer, title)
        consumer = self.create(user=user, tenant=tenant,
                               survey_requested=kwargs['survey_requested'],
                               iprs_first_name=user.first_name,
                               iprs_last_name=user.last_name)
        return consumer


class Consumer(BaseModel):
    """
    Consumers registered in the system
    """
    tenant = models.ForeignKey(Tenant, null=True, blank=True,
                               on_delete=models.CASCADE, help_text='Related Tenant')
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    iprs_verified = models.BooleanField(default=False)
    iprs_change_counter = models.IntegerField(default=0,blank=True)
    iprs_status = models.IntegerField(default=0, choices=IPRS_STATUS, help_text='IPRS status')
    iprs_verification_date = models.DateTimeField(null=True)
    iprs_first_name = models.CharField(
        max_length=50, blank=True, help_text='First Name as received from IPRS')
    iprs_last_name = models.CharField(
        max_length=50, blank=True, help_text='Last Name as received from IPRS')
    iprs_other_name = models.CharField(
        max_length=50, blank=True, help_text='Other Name as received from IPRS')
    iprs_date_of_birth = models.DateField(
        null=True, help_text='Date of birth as received from IPRS')
    similarity_score = models.PositiveIntegerField(help_text='Name Similarity to determine basic authenticity of '
                                                             'provided details', null=True)
    survey_requested = models.BooleanField(
        default=False, help_text='True = a person has requested for GasYetu. Thus should be surveyed')

    survey_conducted = models.BooleanField(
        default=False, help_text='True = the person has been surveyed')

    spouse = models.OneToOneField('Consumer', null=True, blank=True, on_delete=models.CASCADE)

    objects = ConsumerManager()

    def __str__(self):
        return f'{self.user.get_full_name()}'


class Beneficiary(Consumer):
    """
    Beneficiaries who have been approved to purchase Govt Discounted gas
    """
    date_approved = models.DateField()
    has_benefited = models.BooleanField(
        default=False, help_text='True = a person has already gotten the discount')

    def __str__(self):
        return f'{super(Beneficiary, self).__str__()}'
