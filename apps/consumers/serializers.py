from rest_framework import serializers

from apps.consumers.models import Consumer, Beneficiary
from apps.surveys.models import Survey


class SurveySerializer(serializers.ModelSerializer):
    questionnaire_title = serializers.ReadOnlyField(source='questionnaire.title')

    class Meta:
        model = Survey
        fields = (
            'created_on',
            'passed',
            'questionnaire_title'
        )


class BeneficiarySerializer(serializers.ModelSerializer):
    """Beneficiary Model Serializer. """

    first_name = serializers.ReadOnlyField(source='user.first_name')
    last_name = serializers.ReadOnlyField(source='user.last_name')
    region = serializers.ReadOnlyField(source='user.region.title')
    sub_region = serializers.ReadOnlyField(source='user.sub_region.title')
    location = serializers.ReadOnlyField(source='user.location.title')
    village = serializers.ReadOnlyField(source='user.village.title')
    identity_number = serializers.ReadOnlyField(source='user.identity_number')
    phone_number = serializers.ReadOnlyField(source='user.phone')
    survey = SurveySerializer(many=True, source='survey_set')

    class Meta:
        model = Beneficiary
        fields = (
            'created_on', 'tenant', 'user', 'iprs_verified', 'iprs_verification_date', 'iprs_first_name',
            'iprs_last_name',
            'iprs_other_name', 'iprs_date_of_birth', 'similarity_score', 'survey_requested',
            'date_approved', 'has_benefited', 'first_name', 'last_name', 'identity_number', 'phone_number', 'region',
            'sub_region', 'location', 'village', 'survey')


class ConsumerSerializer(serializers.ModelSerializer):
    """Consumer Model Serializer. """

    first_name = serializers.ReadOnlyField(source='user.first_name')
    last_name = serializers.ReadOnlyField(source='user.last_name')
    region = serializers.ReadOnlyField(source='user.region.title')
    sub_region = serializers.ReadOnlyField(source='user.sub_region.title')
    location = serializers.ReadOnlyField(source='user.location.title')
    village = serializers.ReadOnlyField(source='user.village.title')
    identity_number = serializers.ReadOnlyField(source='user.identity_number')
    phone_number = serializers.ReadOnlyField(source='user.phone')
    survey = SurveySerializer(many=True, source='survey_set')

    class Meta:
        model = Consumer
        fields = (
            'created_on', 'tenant', 'user', 'iprs_verified', 'iprs_verification_date', 'iprs_first_name',
            'iprs_last_name', 'iprs_other_name', 'iprs_date_of_birth', 'similarity_score', 'survey_conducted',
            'survey_requested', 'first_name', 'last_name', 'identity_number', 'phone_number', 'region',
            'sub_region', 'location', 'village', 'survey')
