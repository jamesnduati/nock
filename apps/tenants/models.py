from django.db import models
from django.utils.translation import ugettext as _
from apps.core.models import BaseModel
from apps.core.utils import get_uuid
from simple_history.models import HistoricalRecords

from .choices import TENANT_TYPE


class Tenant(BaseModel):
    """
    Central repository of all tenants in the system
    """
    id = models.UUIDField(primary_key=True, default=get_uuid, editable=False)
    tenant_type = models.PositiveSmallIntegerField(
        choices=TENANT_TYPE, help_text='Type of Tenant')
    title = models.CharField(max_length=100, help_text='Name of the Tenant')
    email = models.EmailField(
        _('email address'), blank=True, null=True, default=None, help_text='Contact Email Address'
    )
    phone = models.CharField(
        _('phone number'), max_length=16, null=True,
        blank=True, default=None, help_text='Contact Phone Number'
    )
    enabled = models.BooleanField(default=True)
    history = HistoricalRecords()

    def __str__(self):
        return f'{self.title}'

    @staticmethod
    def create_new(tenant_type, title, commit=True):
        """
        Args:
            tenant_type (int): type of tenant
            title (str): Name of the tenant
            bulk_create (bool): If True, return a created object
        Create a tenant
        """
        if commit:
            return Tenant.objects.create(tenant_type=tenant_type, title=title)
        else:
            return Tenant(tenant_type=tenant_type, title=title)


class TenantBaseModel(BaseModel):
    """
    Base model for all tables that require are have a OneToMany relation with Tenant
    """
    tenant = models.ForeignKey(
        Tenant, on_delete=models.CASCADE)

    class Meta:
        abstract = True
