from django.utils.translation import ugettext_lazy as _

from model_utils import Choices

JAMBO_PAY = 1
GAS_IMPORTER = 2
DISTRIBUTOR = 3
RETAILER = 4
CONSUMER = 5
ENUMERATOR = 6

TENANT_TYPE = Choices(
    (1, 'JamboPay', _('JamboPay')),
    (2, 'GasImporter', _('Gas Importer')),
    (3, 'Distributor', _('Distributor')),
    (4, 'Retailer', _('Retailer')),
    (5, 'Consumer', _('Consumer')),
    (6, 'Enumerator', _('Enumerator')),
)

CYLINDER_SERVICE_REFILL = 1
CYLINDER_SERVICE_PURCHASE = 2

CYLINDER_SERVICE_CHOICES = Choices(
    (CYLINDER_SERVICE_REFILL, 'Cylinder Refill', _('Cylinder Refill')),
    (CYLINDER_SERVICE_PURCHASE, 'Cylinder Purchase', _('Cylinder Refill')),
)
