from rest_framework import status, viewsets
from rest_framework.response import Response
from rest_framework.settings import api_settings
from rolepermissions.decorators import has_role_decorator
from rolepermissions.mixins import HasRoleMixin

from apps.core.viewsets import ReadOnlyViewSet

from .. import models, serializers


class QuestionnaireViewset(HasRoleMixin, ReadOnlyViewSet):
    queryset = models.Questionnaire.objects.all()
    serializer_class = serializers.QuestionnaireSerializer
    allowed_roles = ['enumerator']

    def get_queryset(self):
        queryset = super(QuestionnaireViewset, self).get_queryset()
        return queryset


class SurveyRespond(HasRoleMixin, viewsets.GenericViewSet):
    serializer_class = serializers.SurveyResponseSerializer
    allowed_roles = ['enumerator']

    def get_success_headers(self, data):
        try:
            return {'Location': str(data[api_settings.URL_FIELD_NAME])}
        except (TypeError, KeyError):
            return {}

    def create(self, request, *args, **kwargs):
        """
        Handles Questionnaire responses with the format
        {
            "questionnaire_id":  "1",
            "identity_number":  "123",
            "respondent_name": "John Doe",
            "spouse_identity_number": "789",
            "spouse_name": "Mary Doe",
            "option_responses":
                [
                    {"question_id" : "1", "option_id": "1"},
                    {"question_id" : "2", "option_id": "1"},
                    {"question_id": "2", "option_id": "4", "user_input": "10,1000,10000"},
                ]
        }
        """

        serializer = self.get_serializer(data=request.data, context={
                                         'request': request})
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        serializer.save()
