from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render, reverse
from django.urls import reverse_lazy
from django.views.generic import (CreateView, DeleteView, DetailView, ListView,
                                  RedirectView, UpdateView, View)

from apps.tenants.models import Tenant
from apps.surveys.models import Enumerator, Survey, Questionnaire
from apps.consumers.models import Consumer
from apps.surveys.pdf import PrintPdf


@login_required
def get_enumerators(request):
    if request.method == 'GET':
        report_data = []
        enumerators = Enumerator.objects.all()
        for enumerator in enumerators:
            data = {}
            data['title'] = enumerator.title
            data['identity_number'] = enumerator.identity_number
            data['phone_number'] = enumerator.phone_number
            data['region'] = enumerator.region
            data['sub_region'] = enumerator.sub_region
            data['status'] = enumerator.enumerator_tenant.enabled
            data['date_added'] = enumerator.created_on.strftime('%d-%m-%Y')
            report_data.append(data)
        return render(request, 'reports/enumerators.html', {'enumerators': report_data})
    else:
        min_value = request.POST['min-date']
        max_value = request.POST['max-date']
        enumerators = Enumerator.objects.all()
        report_data = []
        for enumerator in enumerators:
            data = {}
            if enumerator.created_on.strftime('%d-%m-%Y') >= min_value and enumerator.created_on.strftime('%d-%m-%Y') <= max_value:
                data['title'] = enumerator.title
                data['identity_number'] = enumerator.identity_number
                data['phone_number'] = enumerator.phone_number
                data['region'] = enumerator.region
                data['sub_region'] = enumerator.sub_region
                data['status'] = enumerator.enumerator_tenant.enabled
                data['date_added'] = enumerator.created_on.strftime('%d-%m-%Y')
                report_data.append(data)
        return render(request, 'reports/enumerators.html', {'enumerators': report_data})


@login_required
def get_vetted_consumers(request):
    surveys = Survey.objects.all()
    report_data = []
    if request.method == 'GET':
        for survey in surveys:
            data = {}
            consumer = Consumer.objects.get(id=survey.consumer_id)
            if consumer:
                data['name'] = consumer.user.first_name + \
                ' '+consumer.user.last_name
            enumerator = Enumerator.objects.get(id=survey.enumerator_id)
            if enumerator:
                data['enumerator'] = enumerator.title
            questionnaire = Questionnaire.objects.get(
                id=survey.questionnaire_id)
            if questionnaire:
                data['questionnaire'] = questionnaire.title
            data['passed'] = survey.passed
            data['date_administered'] = survey.created_on.strftime('%d-%m-%Y')
            report_data.append(data)
        return render(request,'reports/consumers_vetted.html', {'consumers': report_data})
    else:
        min_value = request.POST['min-date']
        max_value = request.POST['max-date']
        for survey in surveys:
            data = {}
            if survey.created_on.strftime('%d-%m-%Y') >= min_value and survey.created_on.strftime('%d-%m-%Y') <= max_value:
                consumer = Consumer.objects.get(id=survey.consumer_id)
                if consumer:
                    data['name'] = consumer.user.first_name + \
                        ' '+consumer.user.last_name
                enumerator = Enumerator.objects.get(id=survey.enumerator_id)
                if enumerator:
                    data['enumerator'] = enumerator.title
                questionnaire = Questionnaire.objects.get(
                    id=survey.questionnaire_id)
                if questionnaire:
                    data['questionnaire'] = questionnaire.title
                data['passed'] = survey.passed
                data['date_administered'] = survey.created_on.strftime('%d-%m-%Y')
                report_data.append(data)
            else:
                consumer = Consumer.objects.get(id=survey.consumer_id)
                if consumer:
                    data['name'] = consumer.user.first_name + \
                        ' '+consumer.user.last_name
                enumerator = Enumerator.objects.get(id=survey.enumerator_id)
                if enumerator:
                    data['enumerator'] = enumerator.title
                questionnaire = Questionnaire.objects.get(
                    id=survey.questionnaire_id)
                if questionnaire:
                    data['questionnaire'] = questionnaire.title
                data['passed'] = survey.passed
                data['date_administered'] = survey.created_on.strftime('%d-%m-%Y')
                report_data.append(data)
        return render(request, 'reports/consumers_vetted.html', {'consumers': report_data})


@login_required
def print_report(request):
    min_value = request.GET['min_date']
    max_value = request.GET['max_date']
    d_type = request.GET['type']
    tenant = request.tenant
    report_data = []
    try:
        if d_type == 'enumerator':
            enumerators = Enumerator.objects.all()
            for enumerator in enumerators:
                data = {}
                if enumerator.created_on.strftime('%d-%m-%Y') >= min_value and enumerator.created_on.strftime('%d-%m-%Y') <= max_value:
                    data['title'] = enumerator.title
                    data['identity_number'] = enumerator.identity_number
                    data['phone_number'] = enumerator.phone_number
                    data['region'] = enumerator.region
                    data['sub_region'] = enumerator.sub_region
                    data['status'] = enumerator.enumerator_tenant.enabled
                    data['date_added'] = enumerator.created_on.strftime('%d-%m-%Y')
                    report_data.append(data)
                else:
                    data['title'] = enumerator.title
                    data['identity_number'] = enumerator.identity_number
                    data['phone_number'] = enumerator.phone_number
                    data['region'] = enumerator.region
                    data['sub_region'] = enumerator.sub_region
                    data['status'] = enumerator.enumerator_tenant.enabled
                    data['date_added'] = enumerator.created_on.strftime('%d-%m-%Y')
                    report_data.append(data)
            pdf = PrintPdf()
        else:
            surveys = Survey.objects.all()
            for survey in surveys:
                data = {}
                if survey.created_on.strftime('%d-%m-%Y') >= min_value and survey.created_on.strftime('%d-%m-%Y') <= max_value:
                    consumer = Consumer.objects.get(id=survey.consumer_id)
                    if consumer:
                        data['name'] = consumer.user.first_name + \
                            ' '+consumer.user.last_name
                    enumerator = Enumerator.objects.get(
                        id=survey.enumerator_id)
                    if enumerator:
                        data['enumerator'] = enumerator.title
                    questionnaire = Questionnaire.objects.get(
                        id=survey.questionnaire_id)
                    if questionnaire:
                        data['questionnaire'] = questionnaire.title
                    data['passed'] = survey.passed
                    data['date_administered'] = survey.created_on.strftime('%d-%m-%Y')
                    report_data.append(data)
                else:
                    consumer = Consumer.objects.get(id=survey.consumer_id)
                    if consumer:
                        data['name'] = consumer.user.first_name + \
                            ' '+consumer.user.last_name
                    enumerator = Enumerator.objects.get(
                        id=survey.enumerator_id)
                    if enumerator:
                        data['enumerator'] = enumerator.title
                    questionnaire = Questionnaire.objects.get(
                        id=survey.questionnaire_id)
                    if questionnaire:
                        data['questionnaire'] = questionnaire.title
                    data['passed'] = survey.passed
                    data['date_administered'] = survey.created_on.strftime('%d-%m-%Y')
                    report_data.append(data)
            pdf = PrintPdf()
        return pdf.report(report_data, tenant, d_type)
    except Exception as e:
        print(str(e))
