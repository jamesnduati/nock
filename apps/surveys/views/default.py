import datetime
import json

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core import serializers
from django.db import transaction
from django.forms.models import inlineformset_factory
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect, render, reverse
from django.urls import reverse_lazy
from django.views.generic import (CreateView, DeleteView, ListView,
                                  TemplateView, UpdateView)
from djgeojson.views import GeoJSONLayerView
from rolepermissions.decorators import has_permission_decorator
from rolepermissions.mixins import HasRoleMixin
from rolepermissions.roles import assign_role

from apps.accounts.choices import EMPLOYEE
from apps.accounts.models import User
from apps.accounts.utils import send_sms
from apps.core.messages import first_time_password
from apps.core.utils import get_random_digits
from apps.employees.choices import EMPLOYMENT_STATUS, IDENTITY_TYPE
from apps.employees.models import Employee
from apps.surveys.forms import EnumeratorForm, QuestionForm, QuestionnaireForm
from apps.surveys.models import (Enumerator, Option, Question, Questionnaire,
                                 Survey)
from apps.tenants.choices import TENANT_TYPE
from apps.tenants.models import Tenant


class EnumeratorsListView(HasRoleMixin, LoginRequiredMixin, ListView):
    model = Enumerator
    template_name = 'surveys/enumerator_list.html'
    context_object_name = 'enumerators'
    allowed_roles = ['system_admin', 'survey_manager']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Enumerators'
        return context


class EnumeratorCreateView(HasRoleMixin, LoginRequiredMixin, CreateView):
    model = Enumerator
    template_name = 'surveys/enumerator.html'
    form_class = EnumeratorForm
    success_url = reverse_lazy('enumerators')
    allowed_roles = ['system_admin', 'survey_manager']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Create New Enumerator'
        return context

    def form_valid(self, form):
        with transaction.atomic():
            tenant = Tenant.create_new(
                TENANT_TYPE.Enumerator, form.cleaned_data['title'])

            enumerator = form.instance
            enumerator.phone_number = '254' + enumerator.phone_number[-9:]
            enumerator.enumerator_tenant = tenant
            enumerator.save()

            generated_password = get_random_digits(4)
            enumerator_user = User.objects.create_user(
                username=enumerator.phone_number,
                phone=enumerator.phone_number,
                email=enumerator.email_address,
                password=generated_password,
                identity_number=enumerator.identity_number,
                profile_type=EMPLOYEE,
                is_active=True,  # Check on better implementation of this
                email_verified=True,
                phone_verified=True
            )

            Employee.objects.create(
                tenant=tenant,
                user=enumerator_user,
                identity_number=enumerator.identity_number,
                identity_type=IDENTITY_TYPE.NationalID,
                employment_status=EMPLOYMENT_STATUS.Active
            )

        login_username = enumerator.phone_number
        login_password = generated_password
        sms_body = first_time_password.format(
            username=login_username, password=login_password)

        assign_role(enumerator_user, 'enumerator')
        send_sms(enumerator.phone_number, sms_body)
        return super(EnumeratorCreateView, self).form_valid(form)


class EnumeratorUpdateView(HasRoleMixin, LoginRequiredMixin, UpdateView):
    model = Enumerator
    template_name = 'surveys/enumerator.html'
    form_class = EnumeratorForm
    success_url = reverse_lazy('enumerators')
    allowed_roles = ['system_admin', 'survey_manager']

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Update Enumerator'
        return context


class EnumeratorDeactivateView(HasRoleMixin, LoginRequiredMixin, DeleteView):
    model = Enumerator
    template_name = 'surveys/enumerator_deactivate.html'
    success_url = reverse_lazy('enumerators')
    allowed_roles = ['system_admin', 'survey_manager']

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        tenant = self.object.enumerator_tenant
        tenant.enabled = False
        tenant.save()
        self.object.end_date = datetime.date.today().strftime('%Y-%m-%d')
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Confirm Delete'
        return context


class LocateSurveys(LoginRequiredMixin, TemplateView):
    model = Survey
    template_name = 'surveys/survey_location.html'

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['display'] = 'Surveyed Users Locations'
        context['survey_points'] = 'survey-points'
        return context


class SurveyLocationData(LoginRequiredMixin, GeoJSONLayerView):
    model = Survey
    properties = ('consumer', 'created_on', 'id',
                  'enumerator_name', 'consumer_name', 'consumer_region')
    precision = 4
    simplify = 0.5


@login_required
def load_survey(request):
    survey_id = request.GET.get('id')
    if survey_id:
        survey = Survey.objects.get(pk=survey_id)
    else:
        survey = None
    return render(request, 'surveys/location_survey_details.html', {'survey': survey})


@login_required
@has_permission_decorator('can_manage_surveys')
def view_questionnaire(request):
    template = 'surveys/questionnaires.html'
    questionnaires = Questionnaire.objects.all()
    questions = Question.objects.all()
    return render(request, template, {'questionnaires': questionnaires, 'questions': questions})


@login_required
@has_permission_decorator('can_manage_surveys')
def questions_view(request, **kwargs):
    template = 'surveys/questions.html'
    questions = Question.objects.filter(questionnaire=kwargs['pk'])
    return render(request, template, {'questions': questions,
                                      'questionnaire_id': kwargs['pk']})


@login_required
@has_permission_decorator('can_manage_surveys')
def create_questionnaire(request):
    if request.method == "POST":
        questionnaire = QuestionnaireForm(request.POST)
        try:
            questionnaire.save()
            return redirect('questionnaires')
        except Exception as e:
            messages.error(
                request, str(e))
            return redirect(reverse('create_questionnaire'))
    else:
        template = 'surveys/add_questionnaire.html'
        questionnaire_form = QuestionnaireForm
        return render(request, template, {
            'questionnaire_form': questionnaire_form,
        })


@login_required
@has_permission_decorator('can_manage_surveys')
def add_question(request, **kwargs):
    questionnaire_id = kwargs['pk']
    question_form = QuestionForm
    option_formset = inlineformset_factory(
        Question, Option, fk_name='nested_child_question', fields=('option', 'is_correct'))
    if request.method == 'POST':
        question_form = QuestionForm(request.POST)
        option_formset = option_formset(request.POST)
        try:
            if question_form.is_valid() and option_formset.is_valid():
                question = question_form.save()
                new_options = []
                for option_item in option_formset:
                    if option_item.cleaned_data.get('option') is not None:
                        option = option_item.cleaned_data.get('option')
                        is_correct = option_item.cleaned_data.get('is_correct')
                        question = question
                        new_options.append(
                            Option(option=option, is_correct=is_correct, question=question))
                with transaction.atomic():
                    Option.objects.bulk_create(new_options)
                return redirect(reverse('questions', kwargs={'pk': kwargs['pk']}))
        except Exception as e:
            messages.error(
                request, 'There was an error adding the question')
            return redirect(reverse('add_question', kwargs={'pk': kwargs['pk']}))
    else:
        context = {
            'question_form': question_form,
            'option_formset': option_formset,
            'questionnaire_id': questionnaire_id
        }
        return render(request, 'surveys/add_question.html', context)


@login_required
@has_permission_decorator('can_manage_surveys')
def edit_question(request):
    get_value = request.GET['question_id']
    question = Question.objects.get(id=get_value)
    data = {}
    data['question'] = question.question
    data['id'] = question.id
    return HttpResponse(json.dumps(data), content_type="application/json")


@login_required
@has_permission_decorator('can_manage_surveys')
def edit_options(request):
    get_value = request.GET['question_id']
    options = Option.objects.filter(question=get_value)
    data = {}
    data['options'] = serializers.serialize('json', options)
    return HttpResponse(json.dumps(data), content_type="application/json")


@login_required
@has_permission_decorator('can_manage_surveys')
def update_question(request):
    question_id = request.GET['question_id']
    question_value = request.GET['question']
    try:
        question = Question.objects.get(id=question_id)
        question.question = question_value
        question.save()
        data = {}
        data['success'] = 'Question has been updated successfully'
        return HttpResponse(json.dumps(data), content_type="application/json")
    except Exception as e:
        data = {}
        data['error'] = str(e)
        return HttpResponse(json.dumps(data), content_type="application/json")


@login_required
@has_permission_decorator('can_manage_surveys')
def update_option(request):
    option_id = request.GET['option_id']
    option_value = request.GET['option']
    is_correct = request.GET['is_correct']
    try:
        option = Option.objects.get(pk=option_id)
        option.option = option_value
        option.is_correct = (is_correct).title()
        option.save()
        data = {}
        data['success'] = 'Option has been updated successfully'
        return HttpResponse(json.dumps(data), content_type="application/json")
    except Exception as e:
        data = {}
        data['error'] = str(e)
        return HttpResponse(json.dumps(data), content_type="application/json")
