from django import forms
from django.forms import ModelForm, SelectDateWidget
from django.utils.translation import ugettext as _

from apps.accounts import messages
from apps.accounts.models import User

from .models import Enumerator, Option, Question, Questionnaire


class EnumeratorForm(ModelForm):
    phone_number = forms.RegexField(regex=r'^(07)[0-9]{8}$',
                                    error_messages={
                                        'invalid': messages.phone_format},
                                    required=True)

    class Meta:
        model = Enumerator
        fields = (
            'title',
            'identity_number',
            'phone_number',
            'email_address',
            'kra_pin_number',
            'postal_address',
            'region',
            'sub_region',
            'location',
            'village',
            'start_date',
            'end_date'
        )
        widgets = {
            'end_date': forms.DateInput(format=('%d-%m-%Y'),
                                        attrs={'id': 'endDate',
                                               'placeholder': 'Select a date'}),
            'start_date': forms.DateInput(format=('%d-%m-%Y'),
                                          attrs={'id': 'startDate',
                                                 'placeholder': 'Select a date'})
        }

    def clean_phone_number(self):
        instance = getattr(self, 'instance', None)
        phone_number = self.data.get('phone_number', None)
        if instance.pk is None:
            if phone_number:
                phone_number = '254' + phone_number[-9:]
                if Enumerator.objects.filter(phone_number=phone_number).exists():
                    raise forms.ValidationError(
                        _("Enumerator with this phone number already exists."))
                if User.objects.filter(phone=phone_number).exists():
                    raise forms.ValidationError(
                        _("User with this phone number already exists."))
        return phone_number


class QuestionnaireForm(ModelForm):

    class Meta:
        model = Questionnaire
        fields = (
            'title',
            'pass_mark'
        )


class QuestionForm(ModelForm):

    class Meta:
        model = Question
        fields = (
            'questionnaire',
            'question',
            'question_order'

        )


class OptionForm(ModelForm):

    class Meta:
        model = Option
        fields = (
            'option',
            'is_correct',
            'question'
        )
