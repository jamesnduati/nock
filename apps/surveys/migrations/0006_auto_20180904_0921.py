# Generated by Django 2.0.8 on 2018-09-04 09:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('surveys', '0005_auto_20180808_0842'),
    ]

    operations = [
        migrations.AlterField(
            model_name='survey',
            name='consumer',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='consumers.Consumer'),
        ),
        migrations.AlterField(
            model_name='survey',
            name='enumerator',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='surveys.Enumerator'),
        ),
    ]
