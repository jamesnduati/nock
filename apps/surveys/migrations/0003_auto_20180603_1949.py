# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-06-03 19:49
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('surveys', '0002_auto_20180528_1022'),
    ]

    operations = [
        migrations.AddField(
            model_name='survey',
            name='enumerator',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='surveys.Enumerator'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='enumerator',
            name='location',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='core.Location'),
        ),
        migrations.AlterField(
            model_name='enumerator',
            name='region',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='core.Region'),
        ),
        migrations.AlterField(
            model_name='enumerator',
            name='village',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='core.Village'),
        ),
    ]
