# Generated by Django 2.0.8 on 2018-10-12 11:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('surveys', '0007_survey_geom'),
    ]

    operations = [
        migrations.AlterField(
            model_name='survey',
            name='consumer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='consumers.Consumer'),
        ),
    ]
