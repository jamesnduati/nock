from django.conf.urls import url

from apps.surveys.views import reports
from apps.surveys.views.default import (EnumeratorCreateView,
                                        EnumeratorDeactivateView,
                                        EnumeratorsListView,
                                        EnumeratorUpdateView, add_question,
                                        create_questionnaire, edit_options,
                                        edit_question, questions_view,
                                        update_option, update_question,
                                        LocateSurveys, SurveyLocationData,
                                        view_questionnaire, load_survey)

urlpatterns = [
    url(r'^enumerators/$', EnumeratorsListView.as_view(), name='enumerators'),
    url(r'^enumerators/(?P<pk>[0-9]+)/$',
        EnumeratorUpdateView.as_view(),
        name='enumerator_update'),
    url(r'^enumerators/new/$', EnumeratorCreateView.as_view(),
        name='enumerator_create'),
    url(r'^enumerators/(?P<pk>[0-9]+)/deactivate$',
        EnumeratorDeactivateView.as_view(),
        name='enumerator_deactivate'),

    url(r'^questionnaires/$', view_questionnaire, name='questionnaires'),
    url(r'^questions/(?P<pk>[0-9]+)/$', questions_view, name='questions'),
    url(r'^question/$', edit_question, name='question'),
    url(r'^option/$', edit_options, name='option'),
    url(r'^update_option/$', update_option, name='update_option'),
    url(r'^update_question/$', update_question, name='update_question'),
    url(r'^create_questionnaire/$', create_questionnaire,
        name='create_questionnaire'),
    url(r'^add_question/(?P<pk>[0-9]+)/$', add_question, name='add_question'),

    url(r'^reports/enumerators/$', reports.get_enumerators,
        name='report_enumerators'),
    url(r'^reports/print/enumerators/$', reports.print_report,
        name='report_print_enumerators'),

    url(r'^reports/vetted/consumers/$', reports.get_vetted_consumers,
        name='report_vetted_consumers'),
    url(r'^reports/print/vetted/consumers/$', reports.print_report,
        name='report_print_vetted_consumers'),


    url(r'^locate/surveys/$', LocateSurveys.as_view(),
        name="locate-survey"),
    url(r'^points.surveys/$', SurveyLocationData.as_view(),
        name="survey-points"),
    url(r'^ajax/load-survey/$', load_survey, name='ajax_load_survey'),
]
