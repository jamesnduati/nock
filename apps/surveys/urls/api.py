from django.conf.urls import url, include
from rest_framework_nested import routers

from ..views import api

router = routers.SimpleRouter()
router.register(r'questionnaire', api.QuestionnaireViewset, base_name='questionnaire')
router.register(r'respond', api.SurveyRespond, base_name='survey')

urlpatterns = [
    url(r'^', include(router.urls)),
]
