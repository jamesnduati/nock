from django.utils.translation import ugettext_lazy as _

from model_utils import Choices

MAIN_SOURCE_OF_ENERGY = 1
MONTHLY_EXPENDITURE = 2
CLOSED_ENDED = 3

OPEN_ENDED_QUESTIONS = Choices(
    (MAIN_SOURCE_OF_ENERGY, 'Main Source of Energy', _('Main Source of Energy')),
    (MONTHLY_EXPENDITURE, 'Monthly Expenditure', _('Monthly Expenditure')),
    (CLOSED_ENDED, 'Closed Ended', _('Closed Ended')),
)
