from django.apps import apps
from django.contrib import admin
from . import models


class OptionInline(admin.StackedInline):
    extra = 1
    model = models.Option
    fk_name = "question"


class QuestionAdmin(admin.ModelAdmin):
    inlines = [OptionInline, ]


class SurveyAdmin(admin.ModelAdmin):
    list_display = ('questionnaire', 'consumer', 'question_count',
                    'attempted_count', 'correct_answer_count',)


@admin.register(models.Enumerator)
class EnumeratorAdmin(admin.ModelAdmin):
    pass


admin.site.register(models.Option)
admin.site.register(models.Questionnaire)
admin.site.register(models.Question, QuestionAdmin)
admin.site.register(models.SelectedOption)
admin.site.register(models.Survey, SurveyAdmin)
