import json

from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from djgeojson.fields import PointField
from rolepermissions.roles import assign_role

from apps.accounts.choices import PROFILE_TYPES
from apps.consumers.models import Beneficiary, Consumer
from apps.core.models import BaseModel, Location, Region, SubRegion, Village
from apps.core.utils import NullableEmailField
from apps.surveys.choices import (CLOSED_ENDED, MAIN_SOURCE_OF_ENERGY,
                                  MONTHLY_EXPENDITURE, OPEN_ENDED_QUESTIONS)
from apps.tenants.models import Tenant


class Questionnaire(BaseModel):
    title = models.CharField(max_length=100, unique=True)
    pass_mark = models.PositiveSmallIntegerField(default=10)

    def __str__(self):
        return f'{self.title}'


class Question(BaseModel):
    """

    Questions :

    Q1. Predominant wall, closed
    Q2. Predominant roof, closed
    Q3. Main source of energy, open
    Q4. Use of LPG, closed
    Q5. Monthly Expenditure, open
    Q6. Household average income, open


    """

    questionnaire = models.ForeignKey(Questionnaire, on_delete=models.CASCADE)
    question = models.CharField(max_length=512)
    question_type = models.IntegerField(
        choices=OPEN_ENDED_QUESTIONS, default=CLOSED_ENDED)
    question_order = models.PositiveSmallIntegerField(default=0)
    expected_answer_count = models.PositiveSmallIntegerField(default=1)

    def __str__(self):
        return f'{self.question}'

    @property
    def is_open_ended(self):
        return self.question_type in [MAIN_SOURCE_OF_ENERGY, MONTHLY_EXPENDITURE]

    class Meta:
        unique_together = ('questionnaire', 'question')


class Option(BaseModel):
    option = models.CharField(max_length=512)
    is_correct = models.BooleanField(default=False)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    nested_child_question = models.ForeignKey(
        Question, null=True, blank=True, related_name='nested_child_questions', on_delete=models.SET_NULL)

    def __str__(self):
        return f'{self.option}'

    class Meta:
        unique_together = ('option', 'question')


class Survey(BaseModel):
    questionnaire = models.ForeignKey(Questionnaire, on_delete=models.CASCADE)
    consumer = models.ForeignKey(
        Consumer, on_delete=models.CASCADE)
    enumerator = models.ForeignKey(
        'surveys.Enumerator', on_delete=models.SET_NULL, null=True, blank=True)
    geom = PointField(null=True, blank=True)

    @property
    def enumerator_name(self):
        return self.enumerator.title

    @property
    def consumer_name(self):
        return self.consumer.user.get_full_name()

    @property
    def consumer_region(self):
        return self.consumer.user.region.title

    def __str__(self):
        return f'{self.consumer} - {self.questionnaire}'

    def get_position(self):
        return json.loads(self.geom)

    def promote_consumer(self):
        beneficiary = Beneficiary(
            user=self.consumer.user, date_approved=timezone.now())
        beneficiary.__dict__.update(self.consumer.__dict__)
        beneficiary.save()
        user = self.consumer.user
        user.profile_type = PROFILE_TYPES.Beneficiary
        user.save()
        assign_role(self.consumer.user, 'beneficiary')

    @property
    def passed(self):
        return self.questionnaire.pass_mark >= self.attempted_count

    @property
    def question_count(self):
        return Question.objects.filter(questionnaire=self.questionnaire).count()

    @property
    def attempted_count(self):
        return Question.objects.filter(selectedoption__survey=self).distinct().count()

    @property
    def correct_answer_count(self):
        correct_close_ended_questions = 0
        correct_open_ended_questions = 0

        close_ended_questions = Question.objects.filter(
            question_type=CLOSED_ENDED, questionnaire=self.questionnaire
        )

        for question in close_ended_questions:
            selected = SelectedOption.objects.filter(
                question=question, survey=self
            ).first()

            if selected:
                if selected.option.is_correct:
                    correct_close_ended_questions += 1

        main_source_of_energy = Question.objects.filter(
            question_type=MAIN_SOURCE_OF_ENERGY, questionnaire=self.questionnaire
        ).first()

        main_source_of_energy_answers = SelectedOption.objects.filter(
            question=main_source_of_energy, survey=self
        )

        kerosene_present = False
        charcoal_present = False
        other_source_present = False

        for answer in main_source_of_energy_answers:

            option = answer.option.option

            if option == 'Kerosene':
                kerosene_present = True
            elif option == 'Charcoal':
                charcoal_present = True
            else:
                other_source_present = True

        # Disqualify the answer if the user uses another different form of
        # energy

        if other_source_present:
            pass
        elif kerosene_present or charcoal_present:
            correct_open_ended_questions += 1

        monthly_expenditure = Question.objects.filter(
            question_type=MONTHLY_EXPENDITURE, questionnaire=self.questionnaire
        )

        monthly_expenditure_answers = SelectedOption.objects.filter(
            question__in=monthly_expenditure, survey=self
        )

        expenditure = 0

        for answer in monthly_expenditure_answers:
            detail = answer.answer
            try:
                detail = int(detail)
            except ValueError:
                detail = 0
            expenditure += detail

        if 25000 < expenditure:
            correct_open_ended_questions += 1

        correct = correct_close_ended_questions + correct_open_ended_questions

        return correct

    class Meta:
        unique_together = ('questionnaire', 'consumer')


class SelectedOption(BaseModel):
    option = models.ForeignKey(Option, on_delete=models.CASCADE)
    answer = models.CharField(max_length=64, null=True, blank=True)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    survey = models.ForeignKey(Survey, on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.option.question} - {self.option}'

    class Meta:
        unique_together = ('option', 'question', 'survey')


class Enumerator(BaseModel):
    """
    Enumerators registered in the system
    """
    enumerator_tenant = models.OneToOneField(
        Tenant, on_delete=models.CASCADE, null=True, related_name='enumerator_tenant')
    title = models.CharField(
        max_length=100, help_text='Name of the Enumarator')
    start_date = models.DateField()
    end_date = models.DateField(null=True, blank=True)
    identity_number = models.CharField(max_length=20, blank=False)
    phone_number = models.CharField(
        _('phone number'), max_length=16, unique=True, default=None, help_text='Contact Phone Number'
    )
    email_address = NullableEmailField(
        _('e-mail address'), blank=True, null=True, default=None, unique=True,
        help_text='Email Address')
    kra_pin_number = models.CharField(
        max_length=30, help_text='KRA pin number', null=True)
    postal_address = models.CharField(
        max_length=30, help_text='Business postal address', null=True)
    region = models.ForeignKey(
        Region, null=True, blank=True, on_delete=models.SET_NULL)
    sub_region = models.ForeignKey(
        SubRegion, blank=True, null=True, on_delete=models.SET_NULL)
    location = models.ForeignKey(
        Location, null=True, blank=True, on_delete=models.SET_NULL)
    village = models.ForeignKey(
        Village, null=True, blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        return '{}'.format(self.enumerator_tenant)
