from rest_framework import serializers
from django.utils.translation import ugettext_lazy as _
from rest_framework.exceptions import ValidationError
from apps.accounts.utils import send_sms
from apps.consumers.models import Consumer
from apps.accounts.models import User
from apps.core.models import Location, Village
from .models import Questionnaire, Option, Question, SelectedOption, Survey, Enumerator


class OptionSerializer(serializers.ModelSerializer):
    nested_child_question = serializers.SerializerMethodField(
        'get_nested_question')

    def get_nested_question(self, obj):
        if obj.nested_child_question:
            return QuestionSerializer(obj.nested_child_question).data

    class Meta:
        model = Option
        fields = ('id', 'option', 'nested_child_question')


class QuestionSerializer(serializers.ModelSerializer):
    options = OptionSerializer(source='option_set', many=True)

    class Meta:
        model = Question
        fields = ('id', 'question', 'question_type',
                  'expected_answer_count', 'is_open_ended', 'options')


class QuestionnaireSerializer(serializers.ModelSerializer):
    questions = serializers.SerializerMethodField('get_question_set')

    def get_question_set(self, questionnaire):
        queryset = Question.objects.filter(questionnaire=questionnaire)
        nested_questions = queryset.filter(
            option__nested_child_question__isnull=False).values_list('option__nested_child_question', flat=True)
        queryset = queryset.exclude(
            pk__in=nested_questions).order_by('question_order')
        serializer = QuestionSerializer(instance=queryset, many=True)
        return serializer.data

    class Meta:
        model = Questionnaire
        fields = ('id', 'title', 'questions')


class SurveySerializer(serializers.ModelSerializer):
    class Meta:
        model = Survey
        fields = ('questionnaire', 'beneficiary')


class OptionResponse(serializers.Serializer):
    question_id = serializers.IntegerField()
    option_id = serializers.IntegerField()
    user_input = serializers.CharField(required=False)

    def validate(self, attrs):
        # question = Question.objects.get(pk=attrs['question_id'])
        # if not 'user_input' in attrs and question.question_type in ['MONTHLY_EXPENDITURE',
        #                                                             'MAIN_SOURCE_OF_ENERGY']:
        #     return serializers.ValidationError('The user_input containing numerical values is required')
        return attrs


class SurveyResponseSerializer(serializers.Serializer):
    questionnaire_id = serializers.IntegerField(required=True)
    identity_number = serializers.CharField(required=True)
    spouse_name = serializers.CharField(required=False)
    spouse_identity_number = serializers.CharField(required=False)
    option_responses = OptionResponse(many=True)
    passed = serializers.BooleanField(read_only=True)
    lat = serializers.DecimalField(
        max_digits=11, decimal_places=9, write_only=True, required=False)
    lon = serializers.DecimalField(
        max_digits=11, decimal_places=9, write_only=True, required=False)

    def create(self, validated_data):
        questionnaire_id = validated_data['questionnaire_id']
        identity_number = validated_data['identity_number']
        option_responses = validated_data['option_responses']

        user = User.objects.filter(identity_number=identity_number).first()

        try:
            consumer = Consumer.objects.get(user=user)
        except Consumer.DoesNotExist:
            raise serializers.ValidationError({
                'error': "Consumer does not exist"
            })

        longitude = validated_data.get('lon', None)
        latitude = validated_data.get('lat', None)
        if latitude and longitude:
            point = {'type': 'Point', 'coordinates': [longitude, latitude]}
        else:
            point = None

        questionnaire = Questionnaire.objects.get(pk=questionnaire_id)

        enumerator = Enumerator.objects.get(
            enumerator_tenant=self.context['request'].user.profile.tenant)

        survey, __ = Survey.objects.get_or_create(
            enumerator=enumerator,
            questionnaire=questionnaire,
            consumer=consumer,
            geom=point
        )

        # TODO: Ensure to check if a response for a nested question is also
        # required for one to be able
        # if len(option_responses) < questionnaire.question_set.count():
        #     raise ValidationError("All the responses for the questions are required"
        #                           " for the survey to be valid")

        option_ids = [q['option_id'] for q in option_responses]

        for option in option_responses:
            question_obj = Question.objects.get(pk=option['question_id'])
            option_obj = Option.objects.get(pk=option['option_id'])

            if option_obj.question != question_obj:
                raise serializers.ValidationError({
                    'error': 'Wrong option {} for question {}'.format(
                        option_obj.pk, question_obj.pk
                    )
                })

            if option_obj.nested_child_question:
                if not Option.objects.filter(pk__in=option_ids,
                                             question=option_obj.nested_child_question).exists():
                    raise serializers.ValidationError({
                        'error': 'Please also send responses for the nested questions '
                        'the user selected'
                    })

            if not question_obj.is_open_ended:

                SelectedOption.objects.get_or_create(
                    option=option_obj,
                    question=question_obj,
                    survey=survey
                )

            else:

                selected, __ = SelectedOption.objects.get_or_create(
                    option=option_obj,
                    question=question_obj,
                    survey=survey
                )

                selected.answer = option.get('user_input', '')
                selected.save()

        correct_answers = survey.correct_answer_count

        # Mark the consumer has been surveyed
        consumer.survey_conducted = True

        # Set the spouse if provided
        if 'spouse_identity_number' in validated_data:
            if validated_data['spouse_identity_number'] != '' or not validated_data['spouse_identity_number'] is None:
                try:
                    consumer_spouse = Consumer.objects.get(
                        user__identity_number=validated_data['spouse_identity_number'])
                    consumer.spouse = consumer_spouse

                    first_name, last_name = validated_data['spouse_name'].split(
                        ' ')
                    consumer_spouse.first_name = first_name if first_name else ''
                    consumer_spouse.last_name = last_name if last_name else ''
                    consumer_spouse.save()

                except Consumer.DoesNotExist:
                    pass

        consumer.user.save()
        consumer.save()

        if correct_answers >= survey.questionnaire.pass_mark:
            validated_data['passed'] = True
            beneficiary_message = "You have successfully been qualified for Gas Yetu. You can now proceed to make orders."
            send_sms(phone_number=consumer.user.phone,
                     sms_body=beneficiary_message)

            enumerator_message = "Customer %s has successfully been qualified for Gas Yetu. They can now proceed to make orders."
            if self.context['request'].user.phone:
                send_sms(phone_number=self.context['request'].user.phone,
                         sms_body=enumerator_message % consumer.user.phone)

            survey.promote_consumer()
        else:
            message = "Unfortunately you have not qualified for Gas Yetu. Proceed to make orders for Super Gas."
            send_sms(phone_number=consumer.user.phone, sms_body=message)

            enumerator_message = "Customer %s has not qualified for Gas Yetu. They can now proceed to make orders for Super Gas."
            if self.context['request'].user.phone:
                send_sms(phone_number=self.context['request'].user.phone,
                         sms_body=enumerator_message % consumer.user.phone)
            validated_data['passed'] = False
        validated_data['phone_number'] = consumer.user.phone
        return validated_data

    def validate_questionnaire(self, value):
        try:
            Questionnaire.objects.get(pk=value)
        except Questionnaire.DoesNotExist:
            raise serializers.ValidationError("Invalid Questionnaire")

        return value

    def validate_identity_number(self, value):
        if not User.objects.filter(identity_number=value).exists():
            raise serializers.ValidationError("Invalid Identity Number")
        return value

    def validate_spouse_identity_number(self, value):
        if value != '' or not value is None:
            try:
                consumer = Consumer.objects.get(user__identity_number=value)
                if consumer.spouse:
                    if consumer.spouse.survey_conducted:
                        raise serializers.ValidationError(
                            "This household has already been surveyed")
            except Consumer.DoesNotExist:
                pass

        return value
