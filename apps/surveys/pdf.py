from django.utils.translation import ugettext_lazy as _
from reportlab.graphics.charts.textlabels import Label
from reportlab.lib import colors
from reportlab.lib.enums import TA_CENTER, TA_JUSTIFY, TA_LEFT, TA_RIGHT
from reportlab.lib.pagesizes import letter, A4, landscape
from reportlab.lib.units import mm, cm, inch
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.platypus import SimpleDocTemplate, BaseDocTemplate, Paragraph, Spacer, Table,\
    TableStyle, Frame, NextPageTemplate, PageBreak, PageTemplate
from reportlab.platypus.flowables import Image
from reportlab.pdfgen import canvas

from config.settings.base import STATIC_ROOT, STATIC_URL, BASE_DIR
from io import StringIO, BytesIO
from django.http import HttpResponse
from django.shortcuts import render
from datetime import date
buffer = BytesIO()
logo_path = BASE_DIR + STATIC_URL + 'images/logo.png'


class PrintPdf:

    def __init__(self):
        self.buffer = buffer
        self.pageSize = A4
        self.printed_by = ""
        self.date = ""

    def pageNumber(self, canvas, doc):
        canv = canvas
        address = 'National Oil Corporation of Kenya'
        address_1 = 'KAWI house – South C Redcross Road, off Popo Road'
        address_2 = 'Telephone: +254-20-695 2000'
        canv.setFont('Times-Bold', 8)
        canv.drawRightString(780, 590, str(address))
        canv.drawRightString(780, 580, str(address_1))
        canv.drawRightString(780, 570, str(address_2))
        canv.drawRightString(780, 550, str(self.date))
        number = canvas.getPageNumber()
        canvas.drawCentredString(260 * mm, 15 * mm, str(number))
        footer_note = f'Printed by: {self.printed_by}'
        canvas.drawString(10 * mm, 15 * mm, str(footer_note))

    def title_draw(self, x, y, text):
        chart_title = Label()
        chart_title.x = x
        chart_title.y = y
        chart_title.fontName = 'FreeSansBold'
        chart_title.fontSize = 16
        chart_title.textAnchor = 'middle'
        chart_title.setText(text)
        return chart_title

    def report(self, report_data, tenant, d_type):
        self.printed_by = tenant
        today = date.today()
        self.date = today
        response = HttpResponse(content_type='application/pdf')
        if d_type == 'enumerator':
            filename = 'enumerator_report_' + today.strftime('%Y-%m-%d')
        if d_type == 'consumer_vetted':
            filename = 'consumer_vetted_report_' + today.strftime('%Y-%m-%d')
        response['Content-Disposition'] = 'attachment; filename={0}.pdf'.format(
            filename)
        doc = SimpleDocTemplate(
            self.buffer,
            rightMargin=20,
            leftMargin=20,
            topMargin=10,
            bottomMargin=10,
            title=filename
        )
        doc.pagesize = landscape(letter)
        styles = getSampleStyleSheet()
        styles.add(ParagraphStyle(name="report_title",
                                  fontSize=11, alignment=TA_CENTER))
        styles.add(ParagraphStyle(name="TableHeader",
                                  fontSize=11, alignment=TA_CENTER))
        styles.add(ParagraphStyle(name="TableData",
                                  fontSize=10, alignment=TA_JUSTIFY))
        data = []
        data.append(Image(logo_path, 75, 50))
        data.append(Paragraph("<b>Enumerators Report</b>",
                                  styles['report_title']))
        # insert a blank space
        data.append(Spacer(1, 5))
        table_data = []
        # table header
        if d_type == 'enumerator':
            table_data.append([
                Paragraph('No', styles['TableHeader']),
                Paragraph('Name', styles['TableHeader']),
                Paragraph('Identity Number', styles['TableHeader']),
                Paragraph('Phone Number', styles['TableHeader']),
                Paragraph('County', styles['TableHeader']),
                Paragraph('Sub County', styles['TableHeader']),
                Paragraph('Status', styles['TableHeader']),
                Paragraph('Date Added', styles['TableHeader'])
            ])
            for no, report in enumerate(report_data, start=1):
                table_data.append([
                    no,
                    report['title'],
                    report['identity_number'],
                    report['phone_number'],
                    report['region'],
                    report['sub_region'],
                    report['status'],
                    report['date_added']
                ])
            # create table
            report_table = Table(table_data, colWidths=[
                2*cm, 2*cm, 4*cm, 4*cm, 3*cm, 5*cm, 3*cm, 4*cm])
            report_table.hAlign = 'CENTER'
            report_table.setStyle(TableStyle(
                [
                    ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                    ('BOX', (0, 0), (-1, -1), 0.5, colors.black),
                    ('VALIGN', (0, 0), (-1, 0), 'MIDDLE'),
                    ('BACKGROUND', (0, 0), (-1, 0), colors.gray)
                ]
            )
            )
        if d_type == 'consumer_vetted':
            table_data.append([
                Paragraph('No', styles['TableHeader']),
                Paragraph('Name', styles['TableHeader']),
                Paragraph('Enumerator', styles['TableHeader']),
                Paragraph('Questionnaire', styles['TableHeader']),
                Paragraph('Passed', styles['TableHeader']),
                Paragraph('Date Administered', styles['TableHeader'])
            ])
            for no, report in enumerate(report_data, start=1):
                table_data.append([
                    no,
                    report['name'],
                    report['enumerator'],
                    report['questionnaire'],
                    report['passed'],
                    report['date_administered']
                ])
            # create table
            report_table = Table(table_data, colWidths=[
                2*cm, 5*cm, 5*cm, 5*cm, 5*cm, 5*cm])
            report_table.hAlign = 'CENTER'
            report_table.setStyle(TableStyle(
                [
                    ('INNERGRID', (0, 0), (-1, -1), 0.25, colors.black),
                    ('BOX', (0, 0), (-1, -1), 0.5, colors.black),
                    ('VALIGN', (0, 0), (-1, 0), 'MIDDLE'),
                    ('BACKGROUND', (0, 0), (-1, 0), colors.gray)
                ]
            )
            )
        data.append(report_table)
        data.append(Spacer(1, 5))
        # create document
        doc.build(data, onFirstPage=self.pageNumber,
                  onLaterPages=self.pageNumber)
        response.write(self.buffer.getvalue())
        # self.buffer.close()
        return response
