#!/usr/bin/env python
from twisted.application import internet, service
from twisted.web import server
from twisted.internet import reactor
from twisted.python.log import ILogObserver, FileLogObserver
from twisted.python.logfile import DailyLogFile

from iprs.iprs_handler import Factory

PORT=1201
factory = server.Site(Factory())
application = service.Application('mobility-service')
apiService = internet.TCPServer(PORT, factory)
apiService.setName('mobility')
apiService.setServiceParent(application)
#logging
logfile = DailyLogFile("iprs.log", "/tmp")
application.setComponent(ILogObserver, FileLogObserver(logfile).emit)
reactor.suggestThreadPoolSize(20)
