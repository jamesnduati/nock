#!/bin/bash

export PYTHONPATH=$PATH:/app/nock
export DJANGO_SETTINGS_MODULE=config.settings
echo "-----Starting app ---"
twistd --pidfile=iprs.pid -y iprs/iprs_site.py
