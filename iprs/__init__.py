"""
Settings package initialization.
"""

import os
from django_envie import load_vars

# load and set environment variables from '.env.yml' or '.env.py' files with django_envie
load_vars()

