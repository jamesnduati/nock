import logging
import os
from time import sleep

import django
import redis
import requests
from twisted.internet import reactor
from twisted.web import resource

django.setup()
from apps.accounts.utils import send_sms
from apps.consumers.models import Consumer
from apps.consumers.choices import IPRS_STATUS
from datetime import datetime
from fuzzywuzzy import fuzz
from iprs.redisqueue import QueueHandler

# REDIS settings
REDIS_HOST = '127.0.0.1'
REDIS_PORT = 6379
REDIS_DB = 0
REDIS_KEY_REQUEST = 'iprs:req'


class IPRS():
    def __init__(self):
        self.connected = True  # true if service is up or
        self.redis = redis.StrictRedis(host=REDIS_HOST,
                                       port=REDIS_PORT,
                                       db=REDIS_DB)
        self.queue = QueueHandler(REDIS_KEY_REQUEST,
                                  host=REDIS_HOST,
                                  port=REDIS_PORT,
                                  db=REDIS_DB)

    def fetch_tasks(self):
        consumers = Consumer.objects.filter(
            iprs_verified=False,
            iprs_status=IPRS_STATUS.Default,
            user__is_active=True)
        for consumer in consumers:
            id_number = consumer.user.identity_number
            # if len(id_number) < 7:
            #    consumer.user.is_active = False
            #    consumer.user.save()
            #    continue

            request = {'id_number': id_number,
                       'tenant': str(consumer.tenant.id)}
            print(request)
            self.queue.put(request)
        reactor.callLater(3600, self.fetch_tasks)

    def iprs_requests(self):
        while True:
            print('waiting for tasks...')
            task = self.queue.get(block=True)
            if task:
                print(task)
                try:
                    self.handle_response(self.get_iprs(task))
                except Exception as e:
                    logging.exception(e)
            sleep(6)

    def handle_response(self, task):
        '''
          successful response:
           {
             "DOB": null,
             "DOD": null,
             "FistName": "ERASTUS",
             "Gender": "M",
             "IDNumber": null,
             "IDType": 0,
             "LastName": "KIBOI",
             "MiddleName": "GICHUHI",
             "PIN": null,
             "SerialNumber": null
           }
         failed response:
           {
              "ErrorType": "Service Exception",
              "ErrorCode": 2400,
             "Message": "ISW-105 - There is no information for requested search parameters"
           }
        '''
        if task:
            print('task')
            if 'FistName' in task['response']:
                print(task)
                consumer = Consumer.objects.get(
                    tenant__id=task['tenant'])
                consumer.iprs_first_name = task['response']['FistName']
                consumer.iprs_last_name = task['response']['LastName']
                consumer.iprs_other_name = task['response']['MiddleName']
                consumer.iprs_verification_date = datetime.now()
                #_last_name_score = fuzz.ratio(consumer.iprs_last_name, consumer.user.last_name)
                user_data = f'{consumer.user.first_name} {consumer.user.last_name}'
                iprs_data = f'{consumer.iprs_first_name} {consumer.iprs_last_name} {consumer.iprs_other_name}'
                print(f'user::{user_data} iprs:{iprs_data}')
                consumer.similarity_score = fuzz.token_sort_ratio(iprs_data, user_data)
                consumer.iprs_status = IPRS_STATUS.Verified
                consumer.iprs_verified = True

                if consumer.similarity_score < 60:
                    consumer.iprs_verified = False
                    consumer.iprs_status = IPRS_STATUS.PendingChange
                    sms_msg= 'Your details could not be verified. Kindly go to '\
                             'profile and edit to enjoy the service'
                    print(f'{consumer.user.phone}: {sms_msg}')
                    send_sms(consumer.user.phone, sms_msg)
                consumer.iprs_change_counter +=1
                consumer.save()
            if 'ErrorCode' in task['response']:
                if task['response']['ErrorCode'] == 2400:
                    consumer = Consumer.objects.get(
                        tenant__id=task['tenant'])
                    consumer.iprs_status = IPRS_STATUS.PendingChange
                    #consumer.iprs_change_counter +=1
                    consumer.save()
                    message = 'Your profile verification was unsuccessful, '\
                            'please provide the correct identity number'
                    print(f'{consumer.user.phone}: {message}')
                    send_sms(consumer.user.phone, message)

    def get_iprs(self, task):
        try:
            access_token = self.get_access_token()
            if not access_token:
                print('ERROR: NO access_token found')
                return None
            id_number = task['id_number']
            headers = {'Authorization': f"Bearer {access_token}",
                       'app_key': os.getenv('JAMBOPAY_APP_KEY')}
            url = f"{os.getenv('JAMBOPAY_URL')}/api/administration/GetCitizenDetails?role=user&IDNumber={id_number}"
            response = requests.get(url, headers=headers)
            print('STATUS:',response.status_code,'|',response.text)
            if response.status_code == 200 or response.status_code == 400:
                task['response'] = response.json()
                self.connected = True
                return task
            # assume IPRS is down
            self.connected = False
            # requeue
            #self.queue.put(task)
        except Exception as e:
            logging.exception(e)
        return None

    def get_access_token(self):
        access_token = self.redis.get('iprs:access_token')
        if access_token:
            return access_token.decode('utf8')
        headers = {'Content-Type': 'application/x-www-form-urlencoded'}
        request = {'grant_type': 'agency',
                   "password": os.getenv('JAMBOPAY_PASSWORD'),
                   "username": os.getenv('JAMBOPAY_USERNAME')}
        response = requests.post(f"{os.getenv('JAMBOPAY_URL')}/token",
                                 headers=headers,
                                 data=request)
        if response.status_code == 200:
            response = response.json()
            self.redis.setex('iprs:access_token',response['expires_in'],
                             response['access_token'])
            return response['access_token']

        return None


class Factory(resource.Resource):
    def __init__(self):
        resource.Resource.__init__(self)
        reactor.callInThread(IPRS().fetch_tasks)
        reactor.callInThread(IPRS().iprs_requests)
