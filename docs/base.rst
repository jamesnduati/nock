GasYetu System Road Map
#######################


2 week MVP
==========

Target to have a system that allows:

    * A consumer to express interest in joining Gas Yetu
    * Have an Enumerator conduct a survey/questionnaire (This might be on Google Forms for now)
    * Have a person approve the consumer to become a beneficiary via Django Admin
    * Have NOCK load up initial inventory, setup pricing and discounts via Django Admin
    * Have NOCK setup distributor via Django Admin
    * Have Distributor setup retailer via Django Admin
    * Have Distributor make an Order to NOCK and fulfil a sale to a beneficiary via a mobile app
    * Have Retailer make an Order to a Distributor and fulfil a refil to a beneficiary via a mobile app
    * Have a Distributor, Retailer and Beneficiary top up their Wallet
    * Have a Beneficiary place an order via USSD



**Current App Logic:**

All "Entities" are tenants meaning NOCK, Distributor, Retailer and even a Consumer are Tenants

I don't need to describe a modules at this point as I'd explained earlier else we will update the docs later