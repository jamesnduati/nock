FROM nikolaik/python-nodejs:latest
ENV PYTHONUNBUFFERED 1

RUN mkdir /app
RUN mkdir /logs
WORKDIR /app

COPY .prodenv.yml /app/.env.yml


ADD requirements.txt /app/
ADD package.json /app/

RUN pip install -r requirements.txt
RUN npm install

ADD . /app/
# RUN ["chmod", "+x", "/app/scripts/restart.sh"]

RUN pip install -r ussd/requirements.txt
